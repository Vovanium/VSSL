with AUnit.Assertions;
use  AUnit.Assertions;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;
with VSSL.Test_Cases;
use  VSSL.Test_Cases;
with VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
with VSSL.Reals.Approximate;

package body VSSL.Floating_Point.Generic_Quaternions.Test is

	Prefix : constant String := "VSSL.Floating_Point.Generic_Quaternions.";

	Quaternion_Name : constant String := "Quaternion (" & Real_Name & ")";

	function Approx is new Reals.Approximate.Generic_Equal (Real, Epsilon, Epsilon);

	function Image (Q : Quaternion) return String is ("("
		& Real'Image (Q.W) & ","
		& Real'Image (Q.X) & ","
		& Real'Image (Q.Y) & ","
		& Real'Image (Q.Z) & ")");

	function Eq (P, Q : Quaternion) return Boolean is (Approx (P.W, Q.W)
		and then Approx (P.X, Q.X)
		and then Approx (P.Y, Q.Y)
		and then Approx (P.Z, Q.Z));

	package Quaternion_Traits is new Type_Traits (Quaternion, Quaternion_Name, Image, Eq);

	function Image (X : Real) return String renames Real'Image;

	package Real_Traits is new Type_Traits (Real, Real_Name, Image, Approx);

	O    : Quaternion := To_Quaternion (0.0);
	E    : Quaternion := To_Quaternion (1.0);
	Ones : Quaternion := (1.0, 1.0, 1.0, 1.0);

	--

	package Plus_Case is new Self_Inverse_Unary_Function_Case (
		Quaternion_Traits,
		"+",
		"""+""",
		Prefix);

	type Plus_Test is new Plus_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_Test);

	procedure Run_Test (T : in out Plus_Test) is
	begin
		T.Assert_Identity (O);
		T.Assert_Identity (E);
		T.Assert_Identity ((1.0, 1.0, 1.0, 1.0));
	end Run_Test;

	--

	package Minus_Case is new Self_Inverse_Unary_Function_Case (
		Quaternion_Traits,
		"-",
		"""-""",
		Prefix);

	type Minus_Test is new Minus_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_Test);

	procedure Run_Test (T : in out Minus_Test) is
	begin
		T.Assert_Identity (O);
		T.Assert (E,                    (-1.0,  0.0,  0.0,  0.0));
		T.Assert (i,                     (0.0, -1.0,  0.0,  0.0));
		T.Assert (j,                     (0.0,  0.0, -1.0,  0.0));
		T.Assert (k,                     (0.0,  0.0,  0.0, -1.0));
		T.Assert ((1.0, 2.0, 3.0, 4.0), (-1.0, -2.0, -3.0, -4.0));
	end Run_Test;

	--

	package Conjugate_Case is new Self_Inverse_Unary_Function_Case (
		Quaternion_Traits,
		Conjugate,
		"Conjugate",
		Prefix);

	type Conjugate_Test is new Conjugate_Case.Test with null record;
	overriding procedure Run_Test (T : in out Conjugate_Test);

	procedure Run_Test (T : in out Conjugate_Test) is
	begin
		T.Assert_Identity (O);
		T.Assert_Identity (E);
		T.Assert (i,                    (0.0, -1.0,  0.0,  0.0));
		T.Assert (j,                    (0.0,  0.0, -1.0,  0.0));
		T.Assert (k,                    (0.0,  0.0,  0.0, -1.0));
		T.Assert ((1.0, 1.0, 1.0, 1.0), (1.0, -1.0, -1.0, -1.0));
	end Run_Test;

	--

	package Add_QR_Case is new Binary_Function_Case (
		Quaternion_Traits,
		Real_Traits,
		Quaternion_Traits,
		"+",
		"""+""", Prefix);
	type Add_QR_Test is new Add_QR_Case.Test with null record;
	overriding procedure Run_Test (T : in out Add_QR_Test);

	procedure Run_Test (T : in out Add_QR_Test) is
	begin
		T.Assert ((1.0, 1.0, 1.0, 1.0), 1.0, (2.0, 1.0, 1.0, 1.0));
	end Run_Test;

	--

	package Add_RQ_Case is new Binary_Function_Case (
		Real_Traits,
		Quaternion_Traits,
		Quaternion_Traits,
		"+",
		"""+""",
		Prefix);
	type Add_RQ_Test is new Add_RQ_Case.Test with null record;
	overriding procedure Run_Test (T : in out Add_RQ_Test);

	procedure Run_Test (T : in out Add_RQ_Test) is
	begin
		T.Assert (1.0, (1.0, 1.0, 1.0, 1.0), (2.0, 1.0, 1.0, 1.0));
	end Run_Test;

	--

	package Add_QQ_Case is new Commutative_Binary_Function_Case (
		Quaternion_Traits,
		Quaternion_Traits,
		"+",
		"""+""",
		Prefix);
	type Add_QQ_Test is new Add_QQ_Case.Test with null record;
	overriding procedure Run_Test (T : in out Add_QQ_Test);

	procedure Run_Test (T : in out Add_QQ_Test) is
	begin
		T.Assert ((1.0, 0.0, 0.0, 0.0), (1.0, 1.0, 1.0, 1.0), (2.0, 1.0, 1.0, 1.0));
	end Run_Test;

	--

	package Sub_QR_Case is new Binary_Function_Case (
		Quaternion_Traits,
		Real_Traits,
		Quaternion_Traits,
		"-",
		"""-""",
		Prefix);
	type Sub_QR_Test is new Sub_QR_Case.Test with null record;
	overriding procedure Run_Test (T : in out Sub_QR_Test);

	overriding procedure Run_Test (T : in out Sub_QR_Test) is
	begin
		T.Assert ((1.0, 1.0, 1.0, 1.0), 1.0, (0.0, 1.0, 1.0, 1.0));
	end Run_Test;

	--

	package Sub_RQ_Case is new Binary_Function_Case (
		Real_Traits,
		Quaternion_Traits,
		Quaternion_Traits,
		"-",
		"""-""",
		Prefix);
	type Sub_RQ_Test is new Sub_RQ_Case.Test with null record;
	overriding procedure Run_Test (T : in out Sub_RQ_Test);

	overriding procedure Run_Test (T : in out Sub_RQ_Test) is
	begin
		T.Assert (1.0, (1.0, 1.0, 1.0, 1.0), (0.0, -1.0, -1.0, -1.0));
	end Run_Test;

	--

	package Sub_QQ_Case is new Binary_Function_Case (
		Quaternion_Traits,
		Quaternion_Traits,
		Quaternion_Traits,
		"-",
		"""-""",
		Prefix);
	type Sub_QQ_Test is new Sub_QQ_Case.Test with null record;
	overriding procedure Run_Test (T : in out Sub_QQ_Test);

	procedure Run_Test (T : in out Sub_QQ_Test) is
	begin
		T.Assert ((1.0, 0.0, 0.0, 0.0), (1.0, 1.0, 1.0, 1.0), (0.0, -1.0, -1.0, -1.0));
	end Run_Test;

	--

	package Multiply_QR_Case is new Binary_Function_Case (
		Quaternion_Traits,
		Real_Traits,
		Quaternion_Traits,
		"*",
		"""*""",
		Prefix);
	type Multiply_QR_Test is new Multiply_QR_Case.Test with null record;
	overriding procedure Run_Test (T : in out Multiply_QR_Test);

	overriding procedure Run_Test (T : in out Multiply_QR_Test) is
	begin
		T.Assert (O,                    0.0, O);
		T.Assert ((1.0, 2.0, 3.0, 4.0), 0.0, O);
		T.Assert (O,                    1.0, O);
		T.Assert (E,                    1.0, E);
		T.Assert (i,                    1.0, i);
		T.Assert (j,                    1.0, j);
		T.Assert (k,                    1.0, k);
		T.Assert ((1.0, 2.0, 3.0, 4.0), 1.0, (1.0, 2.0, 3.0, 4.0));
		T.Assert ((1.0, 2.0, 3.0, 4.0), 2.0, (2.0, 4.0, 6.0, 8.0));
	end Run_Test;

	--

	package Multiply_RQ_Case is new Binary_Function_Case (
		Real_Traits,
		Quaternion_Traits,
		Quaternion_Traits,
		"*",
		"""*""",
		Prefix);
	type Multiply_RQ_Test is new Multiply_RQ_Case.Test with null record;
	overriding procedure Run_Test (T : in out Multiply_RQ_Test);

	overriding procedure Run_Test (T : in out Multiply_RQ_Test) is
	begin
		T.Assert (0.0, O,                    O);
		T.Assert (0.0, (1.0, 2.0, 3.0, 4.0), O);
		T.Assert (1.0, O,                    O);
		T.Assert (1.0, E,                    E);
		T.Assert (1.0, i,                    i);
		T.Assert (1.0, j,                    j);
		T.Assert (1.0, k,                    k);
		T.Assert (1.0, (1.0, 2.0, 3.0, 4.0), (1.0, 2.0, 3.0, 4.0));
		T.Assert (2.0, (1.0, 2.0, 3.0, 4.0), (2.0, 4.0, 6.0, 8.0));
	end Run_Test;

	--
	package Multiply_QQ_Case is new Binary_Function_Case (
		Quaternion_Traits,
		Quaternion_Traits,
		Quaternion_Traits,
		"*",
		"""*""",
		Prefix);
	type Multiply_QQ_Test is new Multiply_QQ_Case.Test with null record;
	overriding procedure Run_Test (T : in out Multiply_QQ_Test);
	-- Note: quaternion multiplication is not commutative

	overriding procedure Run_Test (T : in out Multiply_QQ_Test) is
	begin
		T.Assert (O,                    O,                    O);
		T.Assert ((1.0, 2.0, 3.0, 4.0), O,                    O);
		T.Assert (O,                    (1.0, 2.0, 3.0, 4.0), O);
		T.Assert (E, E, E); T.Assert (E, i,  i); T.Assert (E, j,  j); T.Assert (E, k,  k);
		T.Assert (i, E, i); T.Assert (i, i, -E); T.Assert (i, j,  k); T.Assert (i, k, -j);
		T.Assert (j, E, j); T.Assert (j, i, -k); T.Assert (j, j, -E); T.Assert (j, k,  i);
		T.Assert (k, E, k); T.Assert (k, i,  j); T.Assert (k, j, -i); T.Assert (k, k, -E);
		T.Assert ((1.0, 2.0, 3.0, 4.0), E,                    (1.0, 2.0, 3.0, 4.0));
		T.Assert (E,                    (1.0, 2.0, 3.0, 4.0), (1.0, 2.0, 3.0, 4.0));
		T.Assert ((2.0, 0.0, 0.0, 0.0), (1.0, 1.0, 1.0, 1.0), (2.0, 2.0, 2.0, 2.0));
	end Run_Test;

	--

	package Divide_QR_Case is new Binary_Function_Case (
		Quaternion_Traits,
		Real_Traits,
		Quaternion_Traits,
		"/",
		"""/""",
		Prefix);
	type Divide_QR_Test is new Divide_QR_Case.Test with null record;
	overriding procedure Run_Test (T : in out Divide_QR_Test);

	overriding procedure Run_Test (T : in out Divide_QR_Test) is
	begin
		T.Assert ((1.0, 1.0, 1.0, 1.0), 2.0, (0.5, 0.5, 0.5, 0.5));
	end Run_Test;

	--

	package Divide_RQ_Case is new Binary_Function_Case (
		Real_Traits,
		Quaternion_Traits,
		Quaternion_Traits,
		"/",
		"""/""",
		Prefix);
	type Divide_RQ_Test is new Divide_RQ_Case.Test with null record;
	overriding procedure Run_Test (T : in out Divide_RQ_Test);

	overriding procedure Run_Test (T : in out Divide_RQ_Test) is
	begin
		T.Assert (2.0, (1.0, 1.0, 1.0, 1.0), (0.5, -0.5, -0.5, -0.5));
	end Run_Test;

	--
	package Divide_QQ_Case is new Binary_Function_Case (
		Quaternion_Traits,
		Quaternion_Traits,
		Quaternion_Traits,
		"/",
		"""/""",
		Prefix);
	type Divide_QQ_Test is new Divide_QQ_Case.Test with null record;
	overriding procedure Run_Test (T : in out Divide_QQ_Test);

	overriding procedure Run_Test (T : in out Divide_QQ_Test) is
	begin
		T.Assert ((2.0, 0.0, 0.0, 0.0), (1.0, 1.0, 1.0, 1.0), (0.5, -0.5, -0.5, -0.5));
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Plus_Test);
		R.Add_Test (new Minus_Test);
		R.Add_Test (new Conjugate_Test);
		R.Add_Test (new Add_QR_Test);
		R.Add_Test (new Add_RQ_Test);
		R.Add_Test (new Add_QQ_Test);
		R.Add_Test (new Sub_QR_Test);
		R.Add_Test (new Sub_RQ_Test);
		R.Add_Test (new Sub_QQ_Test);
		R.Add_Test (new Multiply_QR_Test);
		R.Add_Test (new Multiply_RQ_Test);
		R.Add_Test (new Multiply_QQ_Test);
		R.Add_Test (new Divide_QR_Test);
		R.Add_Test (new Divide_RQ_Test);
		R.Add_Test (new Divide_QQ_Test);
		return R;
	end Suite;

end VSSL.Floating_Point.Generic_Quaternions.Test;
