with AUnit.Simple_Test_Cases;
with VSSL.Test_Cases.Traits;
use  VSSL.Test_Cases.Traits;

package body VSSL.Reals.String_Conversions.Test is

	Prefix : constant String := "VSSL.Reals.String_Conversions";

	type Parsing_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Parsing_Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Parsing_Test);

	function Name (T : Parsing_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format (Prefix & " Parsing test");
	end Name;

	-- Without exponential part

	procedure Assert (
		Prefix      : String;
		Int_Part    : String;
		Separator   : String     := "";
		Frac_Part   : String     := "";
		Postfix     : String     := "";
		Minus       : Boolean    := False;
		Radix       : Radix_Type := 10;
		Int_Digits  : Natural;
		Frac_Digits : Natural    := 0)
	is
		SM : String := Int_Part & Separator & Frac_Part;
		S  : String := Prefix & SM & Postfix;
		R  : Radix_Type;
		M  : Boolean;
		MF : Positive;
		MS : Positive;
		ML : Natural;
		DI : Natural;
		DF : Natural;
		EB : Radix_Type;
		EM : Boolean;
		EF : Positive;
		EL : Natural;
	begin
		Parse_Number_Format (S, R, M, MF, ML, EB, EM, EF, EL);
		Integer_Traits.Assert (R, Radix, "Radix of " & S);
		Boolean_Traits.Assert (M, Minus, "Minus of " & S);
		Integer_Traits.Assert (MF, Prefix'Last + 1, "M_First of " & S);
		Integer_Traits.Assert (ML, Prefix'Last + SM'Length, "M_Last of " & S);
		Split_Mantissa (S (MF .. ML), MS, DI, DF);
		Integer_Traits.Assert (MS, Prefix'Last + Int_Part'Length + 1, "Separator of " & S);
		Integer_Traits.Assert (DI, Int_Digits, "Int_Digits of " & S);
		Integer_Traits.Assert (DF, Frac_Digits, "Frac_Digits of " & S);

	end Assert;

	procedure Assert (
		Prefix      : String;
		Int_Part    : String;
		Separator   : String     := "";
		Frac_Part   : String     := "";
		Postfix     : String     := "";
		Minus       : Boolean    := False;
		Radix       : Radix_Type := 10)
	is
	begin
		Assert (Prefix, Int_Part, Separator, Frac_Part, Postfix,
		        Minus, Radix, Int_Part'Length, Frac_Part'Length);
	end Assert;

	-- With Exponential part

	procedure Assert (
		Prefix      : String;
		Int_Part    : String;
		Separator   : String;
		Frac_Part   : String;
		Infix       : String;
		Exponent    : String;
		Postfix     : String     := "";
		Minus       : Boolean    := False;
		Radix       : Radix_Type := 10;
		Int_Digits  : Natural;
		Frac_Digits : Natural    := 0;
		E_Base      : Radix_Type := 10;
		E_Minus     : Boolean    := False)
	is
		SM : String := Prefix & Int_Part & Separator & Frac_Part;
		S  : String := SM & Infix & Exponent & Postfix;
		R  : Radix_Type;
		M  : Boolean;
		MF : Positive;
		ML : Natural;
		EB : Radix_Type;
		EM : Boolean;
		EF : Positive;
		EL : Natural;

		MS : Positive;
		DI : Natural;
		DF : Natural;
	begin
		Parse_Number_Format (S, R, M, MF, ML, EB, EM, EF, EL);
		Integer_Traits.Assert (R, Radix, "Radix of " & S);
		Boolean_Traits.Assert (M, Minus, "Minus of " & S);
		Integer_Traits.Assert (MF, Prefix'Last + 1, "M_First of " & S);
		Integer_Traits.Assert (ML, SM'Last, "M_Last of " & S);
		Integer_Traits.Assert (EF, SM'Last + Infix'Length + 1, "E_First of " & S);
		Integer_Traits.Assert (EL, SM'Last + Infix'Length + Exponent'Length, "E_Last of " & S);
		Integer_Traits.Assert (EB, E_Base, "E_Base of " & S);
		Boolean_Traits.Assert (EM, E_Minus, "E_Minus of " & S);

		Split_Mantissa (S (MF .. ML), MS, DI, DF);
		Integer_Traits.Assert (MS, Prefix'Last + Int_Part'Length + 1, "Separator of " & S);
		Integer_Traits.Assert (DI, Int_Digits, "Int_Digits of " & S);
		Integer_Traits.Assert (DF, Frac_Digits, "Frac_Digits of " & S);

	end Assert;

	procedure Assert (
		Prefix      : String;
		Int_Part    : String;
		Separator   : String;
		Frac_Part   : String;
		Infix       : String;
		Exponent    : String;
		Postfix     : String     := "";
		Minus       : Boolean    := False;
		Radix       : Radix_Type := 10;
		E_Base      : Radix_Type := 10;
		E_Minus     : Boolean    := False)
	is
	begin
		Assert (Prefix, Int_Part, Separator, Frac_Part, Infix, Exponent, Postfix,
		        Minus, Radix, Int_Part'Length, Frac_Part'Length, E_Base, E_Minus);
	end Assert;

	procedure Run_Test (T : in out Parsing_Test) is
	begin
		-- Common formats (non exponential)
		Assert   ("", "0");
		Assert   (" ", "0");
		Assert   ("", "1", "", "", "   ");
		Assert   ("", "2");
		Assert   ("", "34");
		Assert   (" ", "3_5", "", "", " ",   Int_Digits => 2);
		Assert   ("", "01 2",                Int_Digits => 3);
		Assert   ("", "1", ".", "5");
		Assert   ("", "1", ".", "01");
		Assert   ("", "0", ".", "01");
		Assert   ("", "15", ".", "24");
		Assert   ("", "3", ".", "456789012");
		Assert   ("", "44534098345098275230952", ".", "5678901234567890123456789012345678901234567890");
		Assert   ("", "", ".", "5");
		Assert   ("-", "0",                  Minus => True);
		Assert   ("-", "1",                  Minus => True);
		Assert   ("-", "7",                  Minus => True);

		-- C- and Fortran-style exponential formats
		Assert   ("", "1", "", "", "E", "3");
		Assert   ("", "1_", "", "", "E", "3",    Int_Digits => 1);
		Assert   ("", "5", "", "", "E+", "3");
		Assert   ("", "5", "", "", "E-", "3",    E_Minus => True);
		Assert   ("", "1", ".", "9", "E+", "2");
		Assert   ("", "1", ".", "9", "E-", "2",  E_Minus => True);
		Assert   ("", "4", "", "", "E", "31");
		Assert   ("", "4", "", "", "E-", "31",   E_Minus => True);
		Assert   ("", "5", ".", "2", "D", "16");
		Assert   ("", "5", ".", "2", "D+", "16");
		Assert   ("", "5", ".", "2", "D-", "16", E_Minus => True);

		-- Ada-style based formats
		Assert   ("16#", "F", "", "", "#",          Radix => 16);
		Assert   ("16#", "F",                       Radix => 16);
		Assert   ("16#", "Ed", "", "", "#",         Radix => 16);
		Assert   ("2#", "11", "", "", "#",          Radix =>  2);
		Assert   ("16:", "cd", "", "", ":",         Radix => 16);
		Assert   ("-16#", "cd", "", "", "#",        Radix => 16, Minus => True);
		Assert   ("2#", "1", "", "", "#E", "0",     Radix =>  2, E_Base => 2);
		Assert   ("2#", "1", "", "", "#E", "1",     Radix =>  2, E_Base => 2);
		Assert   ("8#", "1", "", "", "#E", "1",     Radix =>  8, E_Base => 8);
		Assert   ("8#", "1", ".", "1", "#E", "1",   Radix => 8, E_Base => 8);
		Assert   ("8#", "3", ".", "77", "#E", "2",  Radix => 8, E_Base => 8);
		Assert   ("8#", "3", ".", "77", "#e", "2",  Radix => 8, E_Base => 8);
		Assert   ("8:", "3", ".", "77", ":e", "2",  Radix => 8, E_Base => 8);

		-- Algol-style based formats
		Assert   ("16r", "7FFD", ".", "8",  Radix => 16);
		Assert   ("8r", "377",              Radix => 8);

		-- Fortan BOZ formats
		Assert   ("B'", "1011_0101", "", "", "'", Radix => 2, Int_Digits => 8);
		Assert   ("O'", "376", "", "", "'",       Radix => 8);
		Assert   ("Z'", "CAFE", "", "", "'",      Radix => 16);
		Assert   ("Z""", "DEAD", "", "", """",    Radix => 16);

		-- C-style hexadecimal and binary (NO ambiguous octals!)
		Assert   ("0b", "1011_0101",      Radix => 2, Int_Digits => 8);
		Assert   ("0B", "1111_0101",      Radix => 2, Int_Digits => 8);
		Assert   ("0x", "BABE",           Radix => 16);
		Assert   ("0x", "c0de",           Radix => 16);
		Assert   ("0X", "FACE8D",         Radix => 16);
		Assert   ("0X", "FACE", ".", "8D",   Radix => 16);

		-- C-style hexadecimal floats (the only case where Radix and E_Base differ)
		Assert   ("0x", "1", "", "", "p", "0",                     Radix => 16, E_Base => 2);
		Assert   ("-0x", "1EF", "", "", "p", "0",   Minus => True, Radix => 16, E_Base => 2);
		Assert   ("0x", "F", ".", "", "p-", "1",                   Radix => 16, E_Base => 2, E_Minus => True);
		Assert   ("0X", "0", ".", "123", "P-", "4",                Radix => 16, E_Base => 2, E_Minus => True);
		Assert   ("0x", "1", ".", "8", "p", "1",                   Radix => 16, E_Base => 2);

		-- Postfix (Asm-style) hexadecimal and binary formats
		Assert ("", "1001_1011", "", "", "B",   Radix => 2, Int_Digits => 8);
		Assert ("", "0101_1010", "", "", "b",   Radix => 2, Int_Digits => 8);
		Assert ("", "0AF15", "", "", "H",       Radix => 16);
		Assert ("", "c0ac", "", "", "h",        Radix => 16);
	end Run_Test;

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Parsing_Test);
		return R;
	end Suite;

end VSSL.Reals.String_Conversions.Test;
