with AUnit.Simple_Test_Cases;
with AUnit.Assertions;
use  AUnit.Assertions;

package body VSSL.Integers.Modular_Binary.Generic_Test is

	type Is_Power_of_2_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Is_Power_of_2_Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Is_Power_of_2_Test);

	function Name (T : Is_Power_of_2_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Integers.Modular_Binary.Is_Power_of_2 test with " & Value_Name);
	end Name;

	procedure Run_Test (T : in out Is_Power_of_2_Test) is
		X : Value;
	begin
		for I in 0 .. Bits - 1 loop
			Assert (Is_Power_of_2 (2**I), Value'Image (2**I) & " is power of 2");
			for J in 0 .. I - 1 loop
				X := 2**I + 2**J;
				Assert (not Is_Power_of_2 (X), Value'Image (X) & " is not power of 2");
			end loop;
		end loop;
		for I in 3 .. Bits loop
			Assert (not Is_Power_of_2 (2**I - 1), Value'Image (2**I - 1) & " is not power of 2");
		end loop;
	end Run_Test;

	---

	type Floor_Log_2_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Floor_Log_2_Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Floor_Log_2_Test);

	function Name (T : Floor_Log_2_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Integers.Modular_Binary.Floor_Log_2 test with " & Value_Name);
	end Name;

	procedure Run_Test (T : in out Floor_Log_2_Test) is
		X : Value;
	begin
		for I in 0 .. Bits - 1 loop
			Assert (Floor_Log_2 (2**I) = I, "Floor Log2 " & Value'Image (2**I) & " = " & Natural'Image (I));
			for J in 0 .. I - 1 loop
				X := 2**I + 2**J;
				Assert (Floor_Log_2 (X) = I, "Floor Log2 b");
			end loop;
		end loop;
		for I in 3 .. Bits loop
			Assert (Floor_Log_2 (2**I - 1) = I - 1, "Floor Log2 c");
		end loop;
	end Run_Test;

	---

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Floor_Log_2_Test);
		R.Add_Test (new Is_Power_of_2_Test);
		return R;
	end Suite;
end VSSL.Integers.Modular_Binary.Generic_Test;