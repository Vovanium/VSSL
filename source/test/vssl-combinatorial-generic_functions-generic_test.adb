with AUnit.Simple_Test_Cases;
with AUnit.Assertions;
use  AUnit.Assertions;

package body VSSL.Combinatorial.Generic_Functions.Generic_Test is

	type Factorial_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;
	function Name (T : Factorial_Test) return AUnit.Message_String;
	procedure Run_Test (T : in out Factorial_Test);

	function Name (T : Factorial_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Combinatorial.Generic_Functions.Factorial test with " & Value_Name);
	end Name;

	procedure Run_Test (T : in out Factorial_Test) is
		pragma Unreferenced (T);
		F, NF : Value;
		I : Value;
	begin
		Assert (Factorial (0) = 1, "0! = 1");
		Assert (Factorial (1) = 1, "1! = 1");
		F := Factorial (1);
		I := 1;
		while F <= Value'Last / (I + 1) loop
			I := I + 1;
			NF := Factorial (I);
			Assert (NF = F * I, "n! = (n-1)! * n");
			F := NF;
		end loop;
	end Run_Test;

	--

	type Factorial_A_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;
	function Name (T : Factorial_A_Test) return AUnit.Message_String;
	procedure Run_Test (T : in out Factorial_A_Test);

	function Name (T : Factorial_A_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Combinatorial.Generic_Functions.Factorial (2 argument) test with "
		& Value_Name);
	end Name;

	procedure Run_Test (T : in out Factorial_A_test) is
		pragma Unreferenced (T);
	begin
		Assert (Factorial (0, 2) = 1, "0!! = 1");
		Assert (Factorial (1, 2) = 1, "1!! = 1");
		Assert (Factorial (2, 2) = 2, "2!! = 2");
		Assert (Factorial (3, 2) = 3, "3!! = 3");
		Assert (Factorial (4, 2) = 8, "4!! = 8");
		Assert (Factorial (5, 2) = 15, "5!! = 15");
	end Run_Test;

	--

	type Subfactorial_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;
	function Name (T : Subfactorial_Test) return AUnit.Message_String;
	procedure Run_Test (T : in out Subfactorial_Test);

	function Name (T : Subfactorial_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Combinatorial.Generic_Functions.Subfactorial test with " & Value_Name);
	end Name;

	procedure Run_Test (T : in out Subfactorial_Test) is
		pragma Unreferenced (T);
	begin
		Assert (Subfactorial (0) = 1, "!0 = 1");
		Assert (Subfactorial (1) = 0, "!1 = 0");
		Assert (Subfactorial (2) = 1, "!2 = 1");
		Assert (Subfactorial (3) = 2, "!3 = 2");
		Assert (Subfactorial (4) = 9, "!4 = 9");
		Assert (Subfactorial (5) = 44, "!5 = 44");
		Assert (Subfactorial (6) = 265, "!6 = 265");
		Assert (Subfactorial (10) = 1_334_961, "!10 = 1 334 691");
		Assert (Subfactorial (12) = 176_214_841, "!12 = 176 214 841");
	end Run_Test;

	--

	type Permutations_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;
	function Name (T : Permutations_Test) return AUnit.Message_String;
	procedure Run_Test (T : in out Permutations_Test);

	function Name (T : Permutations_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Combinatorial.Generic_Functions.Permutations test with "
		& Value_Name);
	end Name;

	procedure Run_Test (T : in out Permutations_Test) is
		pragma Unreferenced (T);
	begin
		Assert (Permutations (1, 1) = 1, "P (1, 1) = 1");
		Assert (Permutations (5, 3) = 60, "P (5, 3) = 60");
	end Run_Test;

	--

	type Combinations_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;
	function Name (T : Combinations_Test) return AUnit.Message_String;
	procedure Run_Test (T : in out Combinations_Test);

	function Name (T : Combinations_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Combinatorial.Generic_Functions.Combinations test with "
		& Value_Name);
	end Name;

	procedure Run_Test (T : in out Combinations_Test) is
		pragma Unreferenced (T);
	begin
		Assert (Combinations (1, 1) = 1, "C (1, 1) = 1");
		Assert (Combinations (5, 3) = 10, "C (5, 3) = 10");
		for N in Value range 1 .. 20 loop
			for R in 0 .. (N - 1) / 2 loop
				Assert (Combinations (N, R) = Combinations (N, N - R),
				"C(N, R) = C(N, N - R); N = " & Value'Image (N)
				& "; R = " & Value'Image (R));
			end loop;
		end loop;
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Factorial_Test);
		R.Add_Test (new Factorial_A_Test);
		R.Add_Test (new Subfactorial_Test);
		R.Add_Test (new Permutations_Test);
		R.Add_Test (new Combinations_Test);
		return R;
	end Suite;

end VSSL.Combinatorial.Generic_Functions.Generic_Test;
