with AUnit.Assertions;
use  AUnit.Assertions;
with AUnit.Simple_Test_Cases;
with Interfaces;
use  Interfaces;

package body VSSL.Integers.Modular_Binary.Generic_Reversed.Generic_Test is

	type Bit_Reverse_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Bit_Reverse_Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Bit_Reverse_Test);

	function Name (T : Bit_Reverse_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Integers.Modular_Binary.Reversed.Bit_Reverse test with "
		& Value_Name & ", "
		& Positive'Image (Default_Bits));
	end Name;

	procedure Run_Test (T : in out Bit_Reverse_Test) is
	begin
		for I in 0 .. Default_Bits - 1 loop
			Assert (Bit_Reverse (2**I) = 2**(Default_Bits - 1 - I), "Not written yet");
		end loop;
	end Run_Test;

	---

	type Increment_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Increment_Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Increment_Test);

	function Name (T : Increment_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Integers.Modular_Binary.Reversed.Increment test with "
		& Value_Name & ", "
		& Positive'Image (Default_Bits));
	end Name;

	procedure Run_Test (T : in out Increment_Test) is
	begin
		for I in 0 .. Default_Bits - 1 loop
			Assert (Increment (0, 2**I) = 2**I, "Increment");
			for J in 0 .. I - 1 loop
				Assert (Increment (2**J, 2**I) = 2**J + 2**I, "Increment no carry");
			end loop;
			for J in 0 .. I - 2 loop
				Assert (Increment (2**I + 2**J, 2**I) = 2**(I - 1) + 2**J, "Increment one carry");

				Assert (Increment (2**(I + 1) - 2**(J + 1), 2**I) = 2**J, "Increment long carry");
			end loop;
			Assert (Increment (2**(I + 1) - 1, 2**I) = 0, "Increment wrap");
		end loop;
	end Run_Test;

	---

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Bit_Reverse_Test);
		return R;
	end Suite;

end VSSL.Integers.Modular_Binary.Generic_Reversed.Generic_Test;
