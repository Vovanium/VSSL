with VSSL.Integers.Big.Test;
with VSSL.Integers.Binary_16.Test;
with VSSL.Integers.Binary_16.Reversed.Test;
with VSSL.Integers.Generic_Radix.Test;
with VSSL.Integers.Generic_Digit_Arrays.Test;
with VSSL.Integers.Eisenstein;
with VSSL.Integers.Generic_Eisenstein.Test;
with VSSL.Integers.Gaussian;
with VSSL.Integers.Generic_Gaussian.Test;

package body VSSL.Integers.Test is

	package Radix is new VSSL.Integers.Generic_Radix (
		Number        => Integer,
		Default_Radix => 10);

	package Radix_Test is new Radix.Test (
		Number_Name => "Integer",
		Image => Integer'Image,
		Zero => 0,
		One => 1,
		Last => Integer'Last);

	type Decimal is mod 10;
	type Decimal_Array is array (Integer range <>) of Decimal;

	package Digit_Arrays is new VSSL.Integers.Generic_Digit_Arrays (
		Digit_Type => Decimal,
		Index => Integer,
		Digit_Array => Decimal_Array);

	package Digit_Arrays_Test is new Digit_Arrays.Test (
		Digit_Name => "Decimal");

	package Eisenstein_Test is new VSSL.Integers.Eisenstein.Test (
		Integer_Name => "Integer");

	package Gaussian_Test is new VSSL.Integers.Gaussian.Test (
		Integer_Name => "Integer");

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (Big.Test.Suite);
		R.Add_Test (Binary_16.Test.Suite);
		R.Add_Test (Binary_16.Reversed.Test.Suite);
		R.Add_Test (Radix_Test.Suite);
		R.Add_Test (Digit_Arrays_Test.Suite);
		R.Add_Test (Eisenstein_Test.Suite);
		R.Add_Test (Gaussian_Test.Suite);
		return R;
	end Suite;

end VSSL.Integers.Test;
