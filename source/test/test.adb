with AUnit.Reporter.Text;
with AUnit.Run;

with VSSL.Test;

procedure Test is
	procedure Runner is new AUnit.Run.Test_Runner (VSSL.Test.Suite);
	Reporter : AUnit.Reporter.Text.Text_Reporter;
begin
	Runner (Reporter);
end Test;
