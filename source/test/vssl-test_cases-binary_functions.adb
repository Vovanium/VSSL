package body VSSL.Test_Cases.Binary_Functions is

	package body Binary_Function_Case is

		overriding function Name (T : Test) return AUnit.Message_String is
		begin
			return AUnit.Format (Prefix & Function_Name & "(" & Arg_1.Value_Name & ", "
				& Arg_2.Value_Name & ")" & " test" & Suffix);
		end Name;

		procedure Assert (
			T        : in out Test;
			A        : in     Arg_1.Value;
			B        : in     Arg_2.Value;
			Expected : in     Result.Value;
			Message  : in     String := "")
		is
		begin
			Result.Assert (Call (A, B), Expected, Function_Name & " (" & Arg_1.Image (A) & ", "
				& Arg_2.Image (B) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

	end Binary_Function_Case;

	package body Commutative_Binary_Function_Case is

		overriding function Name (T : Test) return AUnit.Message_String is
		begin
			return AUnit.Format (Prefix & Function_Name & "(" & Args.Value_Name & ", "
				& Args.Value_Name & ")" & " test" & Suffix);
		end Name;

		procedure Assert (
			T        : in out Test;
			A, B     : in     Args.Value;
			Expected : in     Result.Value;
			Message  : in     String := "")
		is
		begin
			Result.Assert (Call (A, B), Expected, Function_Name & " (" & Args.Image (A) & ", "
				& Args.Image (B) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
			Result.Assert (Call (B, A), Expected, Function_Name & " (" & Args.Image (B) & ", "
				& Args.Image (A) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

	end Commutative_Binary_Function_Case;

	package body Anticommutative_Binary_Function_Case is

		overriding function Name (T : Test) return AUnit.Message_String is
		begin
			return AUnit.Format (Prefix & Function_Name & "(" & Args.Value_Name & ", "
				& Args.Value_Name & ")" & " test" & Suffix);
		end Name;

		procedure Assert (
			T        : in out Test;
			A, B     : in     Args.Value;
			Expected : in     Result.Value;
			Message  : in     String := "")
		is
		begin
			Result.Assert (Call (A, B), Expected, Function_Name & " (" & Args.Image (A) & ", "
				& Args.Image (B) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
			Result.Assert (Call (B, A), -Expected, Function_Name & " (" & Args.Image (B) & ", "
				& Args.Image (A) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

	end Anticommutative_Binary_Function_Case;

end VSSL.Test_Cases.Binary_Functions;
