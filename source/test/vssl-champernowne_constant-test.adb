with AUnit.Simple_Test_Cases;
with AUnit.Assertions;
use  AUnit.Assertions;

package body VSSL.Champernowne_Constant.Test is

	type Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Test);

	function Name (T : Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format ("VSSL.Champernowne_Constant test");
	end Name;

	procedure Run_Test (T : in out Test) is
	begin
		Assert (Champernowne_Constant_Digit (1) = 1, "First digit");
		Assert (Champernowne_Constant_Digit (2) = 2, "Second digit");
		Assert (Champernowne_Constant_Digit (10) = 1, "First digit of second order");
	end Run_Test;

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Test);
		return R;
	end Suite;
end VSSL.Champernowne_Constant.Test;
