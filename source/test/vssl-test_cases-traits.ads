package VSSL.Test_Cases.Traits is

	package Boolean_Traits is new Type_Traits (Boolean, "Boolean", Boolean'Image, "=");

	package Integer_Traits is new Type_Traits (Integer, "Integer", Integer'Image, "=");

	function String_Image (S : String) return String is ("""" & S & """");

	package String_Traits is new Type_Traits (String, "String", String_Image, "=");

end VSSL.Test_Cases.Traits;
