with VSSL.Combinatorial.Generic_Functions.Generic_Test;
with VSSL.Combinatorial.Functions;
with VSSL.Combinatorial.Generic_Lexicographic_Permutations.Generic_Test;
with VSSL.Combinatorial.Permutations.Test;

package body VSSL.Combinatorial.Test is

	package Functions_Test is new VSSL.Combinatorial.Functions.Generic_Test (
		Value_Name => "Integer");

	package String_Lexicographic_Permutations
	is new VSSL.Combinatorial.Generic_Lexicographic_Permutations (
		Element => Character,
		Index   => Positive,
		Set     => String);

	package String_Lexicographic_Permutations_Test
	is new String_Lexicographic_Permutations.Generic_Test (
		Value_Name => "String",
		Initial    => "abcdefgh");

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (Functions_Test.Suite);
		R.Add_Test (VSSL.Combinatorial.Permutations.Test.Suite);
		R.Add_Test (String_Lexicographic_Permutations_Test.Suite);
		return R;
	end Suite;

end VSSL.Combinatorial.Test;
