--
-- Unary function (that is Y = f(X)) testing framework
--
package VSSL.Test_Cases.Unary_Functions is

	generic
		with package Arg_1  is new Type_Traits (<>);
		with package Result is new Type_Traits (<>);
		with function Call (A : Arg_1.Value) return Result.Value;
		Function_Name : String;
		Prefix        : String := "";
		Suffix        : String := "";
	package Unary_Function_Case is

		type Test is abstract new Test_Case with null record;
		overriding function Name (T : Test) return AUnit.Message_String;

		procedure Assert (
			T        : in out Test;
			A        : in     Arg_1.Value;
			Expected : in     Result.Value;
			Message  : in     String := "");
	end Unary_Function_Case;

	--
	-- Specific case for an unary function that is even
	-- For even function following rule is met: f(-x) = -f(x)
	--
	generic
		with package Arg_1  is new Type_Traits (<>);
		with package Result is new Type_Traits (<>);
		with function Call (A : Arg_1.Value) return Result.Value;
		Function_Name : String;
		Prefix        : String := "";
		Suffix        : String := "";
		with function "-" (X : Arg_1.Value) return Arg_1.Value is <>;
		with function "-" (X : Result.Value) return Result.Value is <>;
	package Odd_Unary_Function_Case is

		type Test is abstract new Test_Case with null record;
		overriding function Name (T : Test) return AUnit.Message_String;

		procedure Assert (
			T        : in out Test;
			A        : in     Arg_1.Value;
			Expected : in     Result.Value;
			Message  : in     String := "");
	end Odd_Unary_Function_Case;

	--
	-- Specific case for an unary function that is self-inverse
	-- For self-inverse function following rule is met: y = f(x) <=> x = f(y)
	-- Argument and result should be of the same type
	--
	generic
		with package Traits is new Type_Traits (<>);
		with function Call (A : Traits.Value) return Traits.Value;
		Function_Name : String;
		Prefix        : String := "";
		Suffix        : String := "";
	package Self_Inverse_Unary_Function_Case is

		type Test is abstract new Test_Case with null record;
		overriding function Name (T : Test) return AUnit.Message_String;

		procedure Assert (
			T        : in out Test;
			A, B     : in     Traits.Value;
			Message  : in     String := "");

		procedure Assert_Identity (
			T        : in out Test;
			A        : in     Traits.Value;
			Message  : in     String := "");
		-- Test for corner case: A = f(A)
	end Self_Inverse_Unary_Function_Case;

end VSSL.Test_Cases.Unary_Functions;