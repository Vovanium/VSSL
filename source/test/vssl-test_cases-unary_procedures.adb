package body VSSL.Test_Cases.Unary_Procedures is

	package body Unary_Procedure_Case is

		overriding function Name (T : Test) return AUnit.Message_String is
		begin
			return AUnit.Format (Prefix & Procedure_Name
			& " (A) with A = " & Arg_1.Value_Name & " test" & Suffix);
		end Name;

		procedure Assert (
			T        : in out Test;
			A_Pre    : in     Arg_1.Value;
			A_Post   : in     Arg_1.Value;
			Message  : in     String := "")
		is
			A_Var : Arg_1.Value := A_Pre;
		begin
			Call (A_Var);
			Arg_1.Assert (A_Var, A_Post, "A of " & Procedure_Name
				& " (A) with A = " & Arg_1.Image (A_Pre)
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

		procedure Assert_Identity (
			T        : in out Test;
			A        : in     Arg_1.Value;
			Message  : in     String := "")
		is
			A_Var : Arg_1.Value := A;
		begin
			Call (A_Var);
			Arg_1.Assert (A_Var, A, "A of " & Procedure_Name
				& " (A) with A = " & Arg_1.Image (A) & " (Identity)"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert_Identity;

	end Unary_Procedure_Case;

	package body Self_Inverse_Unary_Procedure_Case is

		overriding function Name (T : Test) return AUnit.Message_String is
		begin
			return AUnit.Format (Prefix & Procedure_Name
			& " (A) with A = " & Arg_1.Value_Name & " test" & Suffix);
		end Name;

		procedure Assert (
			T        : in out Test;
			A_Pre    : in     Arg_1.Value;
			A_Post   : in     Arg_1.Value;
			Message  : in     String := "")
		is
			A_Var : Arg_1.Value := A_Pre;
		begin
			Call (A_Var);
			Arg_1.Assert (A_Var, A_Post, "A of " & Procedure_Name
				& " (A) with A = " & Arg_1.Image (A_Pre)
				& (if Message /= "" then " : " & Message
				else ""));
			Call (A_Var);
			Arg_1.Assert (A_Var, A_Pre, "A of " & Procedure_Name
				& " (A) with A = " & Arg_1.Image (A_Post)
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

		procedure Assert_Identity (
			T        : in out Test;
			A        : in     Arg_1.Value;
			Message  : in     String := "")
		is
			A_Var : Arg_1.Value := A;
		begin
			Call (A_Var);
			Arg_1.Assert (A_Var, A, "A of " & Procedure_Name
				& " (A) with A = " & Arg_1.Image (A) & " (Identity)"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert_Identity;

	end Self_Inverse_Unary_Procedure_Case;

end VSSL.Test_Cases.Unary_Procedures;