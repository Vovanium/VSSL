with AUnit.Assertions, AUnit.Simple_Test_Cases;
use  AUnit.Assertions, AUnit.Simple_Test_Cases;
with VSSL.Test_Cases;
use  VSSL.Test_Cases;
with VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;

package body VSSL.Integers.Generic_Eisenstein.Test is

	Prefix : constant String := "VSSL.Integers.Generic_Eisenstein.";

	Eisenstein_Name : constant String := "Eisenstein_" & Integer_Name;

	package Integer_Traits is new Type_Traits (Integer_Type, Integer_Name, Integer_Type'Image, "=");

	function Image (X : Eisenstein_Integer) return String is ("("
		& Integer_Type'Image (X (1)) & " +" & Integer_Type'Image (X (2)) & " w)");

	package Eisenstein_Traits is new Type_Traits (Eisenstein_Integer, Eisenstein_Name, Image, "=");

	--

	package Plus_E_Case is new Self_Inverse_Unary_Function_Case (
		Eisenstein_Traits,
		"+",
		"""+""",
		Prefix);

	type Plus_E_Test is new Plus_E_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_E_Test);

	procedure Run_Test (T : in out Plus_E_Test) is
	begin
		T.Assert_Identity ((0, 0));
		T.Assert_Identity ((1, 0));
		T.Assert_Identity ((0, 1));
		T.Assert_Identity ((2, 1));
	end Run_Test;

	--

	package Plus_EI_Case is new Binary_Function_Case (
		Eisenstein_Traits,
		Integer_Traits,
		Eisenstein_Traits,
		"+",
		"""+""",
		Prefix);
	type Plus_EI_Test is new Plus_EI_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_EI_Test);

	procedure Run_Test (T : in out Plus_EI_Test) is
	begin
		T.Assert  ((0, 0),  0,  (0,  0));
		T.Assert  ((0, 0),  1,  (1,  0));
		T.Assert  ((1, 0),  0,  (1,  0));
		T.Assert  ((0, 0), -1, (-1,  0));
		T.Assert ((-1, 0),  0, (-1,  0));
		T.Assert  ((1, 0), -1,  (0,  0));
		T.Assert ((-1, 0),  1,  (0,  0));
		T.Assert  ((0, 1),  2,  (2,  1));
	end Run_Test;

	--

	package Plus_EE_Case is new Commutative_Binary_Function_Case (
		Eisenstein_Traits,
		Eisenstein_Traits,
		"+",
		"""+""",
		Prefix);
	type Plus_EE_Test is new Plus_EE_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_EE_Test);

	procedure Run_Test (T : in out Plus_EE_Test) is
	begin
		T.Assert ((0, 0),  (0,  0),  (0,  0));
		T.Assert ((0, 0),  (1,  0),  (1,  0));
		T.Assert ((0, 0), (-1,  0), (-1,  0));
		T.Assert ((1, 0), (-1,  0),  (0,  0));
		T.Assert ((0, 0),  (0,  1),  (0,  1));
		T.Assert ((0, 0),  (0, -1),  (0, -1));
		T.Assert ((0, 1),  (0, -1),  (0,  0));
		T.Assert ((1, 0),  (0,  2),  (1,  2));
	end Run_Test;

	--

	package Minus_E_Case is new Self_Inverse_Unary_Function_Case (
		Eisenstein_Traits,
		"-",
		"""-""",
		Prefix);

	type Minus_E_Test is new Minus_E_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_E_Test);

	procedure Run_Test (T : in out Minus_E_Test) is
	begin
		T.Assert_Identity ((0,  0));
		T.Assert ((1, 0), (-1,  0));
		T.Assert ((0, 1),  (0, -1));
		T.Assert ((2, 1), (-2, -1));
	end Run_Test;

	--

	package Minus_EE_Case is new Binary_Function_Case (
		Eisenstein_Traits,
		Eisenstein_Traits,
		Eisenstein_Traits,
		"-",
		"""-""",
		Prefix);
	type Minus_EE_Test is new Minus_EE_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_EE_Test);

	procedure Run_Test (T : in out Minus_EE_Test) is
	begin
		T.Assert  ((0,  0),  (0,  0),  (0,  0));
		T.Assert  ((1,  0),  (0,  0),  (1,  0));
		T.Assert  ((0,  0),  (1,  0), (-1,  0));
		T.Assert ((-1,  0),  (0,  0), (-1,  0));
		T.Assert  ((0,  0), (-1,  0),  (1,  0));
		T.Assert  ((1,  0),  (1,  0),  (0,  0));
		T.Assert ((-1,  0), (-1,  0),  (0,  0));
		T.Assert  ((0,  1),  (0,  0),  (0,  1));
		T.Assert  ((0,  0),  (0,  1),  (0, -1));
		T.Assert  ((0, -1),  (0,  0),  (0, -1));
		T.Assert  ((0,  0),  (0, -1),  (0,  1));
		T.Assert  ((0,  1),  (0,  1),  (0,  0));
		T.Assert  ((0, -1),  (0, -1),  (0,  0));
		T.Assert  ((1,  0),  (0,  2),  (1, -2));
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		Result : Access_Test_Suite := new Test_Suite;
	begin
		Result.Add_Test (new Plus_E_Test);
		Result.Add_Test (new Plus_EI_Test);
		Result.Add_Test (new Plus_EE_Test);
		Result.Add_Test (new Minus_E_Test);
		Result.Add_Test (new Minus_EE_Test);
		return Result;
	end Suite;

end VSSL.Integers.Generic_Eisenstein.Test;