with AUnit.Assertions, AUnit.Simple_Test_Cases;
use  AUnit.Assertions, AUnit.Simple_Test_Cases;
with VSSL.Test_Cases;
use  VSSL.Test_Cases;
with VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;

package body VSSL.Integers.Generic_Gaussian.Test is

	Prefix : constant String := "VSSL.Integers.Generic_Gaussian.";

	Gaussian_Name : constant String := "Gaussian_" & Integer_Name;

	package Integer_Traits is new Type_Traits (Integer_Type, Integer_Name, Integer_Type'Image, "=");

	function Image (X : Gaussian_Integer) return String is ("("
		& Integer_Type'Image (X.Re) & " +" & Integer_Type'Image (X.Im) & " i)");

	package Gaussian_Traits is new Type_Traits (Gaussian_Integer, Gaussian_Name, Image, "=");

	--

	package Plus_G_Case is new Self_Inverse_Unary_Function_Case (
		Gaussian_Traits,
		"+",
		"""+""",
		Prefix);

	type Plus_G_Test is new Plus_G_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_G_Test);

	procedure Run_Test (T : in out Plus_G_Test) is
	begin
		T.Assert_Identity ((0, 0));
		T.Assert_Identity ((1, 0));
		T.Assert_Identity ((0, 1));
		T.Assert_Identity ((2, 1));
	end Run_Test;

	--

	package Plus_GI_Case is new Binary_Function_Case (
		Gaussian_Traits,
		Integer_Traits,
		Gaussian_Traits,
		"+",
		"""+""",
		Prefix);
	type Plus_GI_Test is new Plus_GI_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_GI_Test);

	procedure Run_Test (T : in out Plus_GI_Test) is
	begin
		T.Assert  ((0, 0),  0,  (0,  0));
		T.Assert  ((0, 0),  1,  (1,  0));
		T.Assert  ((1, 0),  0,  (1,  0));
		T.Assert  ((0, 0), -1, (-1,  0));
		T.Assert ((-1, 0),  0, (-1,  0));
		T.Assert  ((1, 0), -1,  (0,  0));
		T.Assert ((-1, 0),  1,  (0,  0));
		T.Assert  ((0, 1),  2,  (2,  1));
	end Run_Test;

	--

	package Plus_IG_Case is new Binary_Function_Case (
		Integer_Traits,
		Gaussian_Traits,
		Gaussian_Traits,
		"+",
		"""+""",
		Prefix);
	type Plus_IG_Test is new Plus_IG_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_IG_Test);

	procedure Run_Test (T : in out Plus_IG_Test) is
	begin
		T.Assert  (0,  (0, 0),  (0, 0));
		T.Assert  (0,  (1, 0),  (1, 0));
		T.Assert  (1,  (0, 0),  (1, 0));
		T.Assert  (0, (-1, 0), (-1, 0));
		T.Assert (-1,  (0, 0), (-1, 0));
		T.Assert  (1, (-1, 0),  (0, 0));
		T.Assert (-1,  (1, 0),  (0, 0));
		T.Assert  (1,  (0, 2),  (1, 2));
	end Run_Test;

	--

	package Plus_GG_Case is new Commutative_Binary_Function_Case (
		Gaussian_Traits,
		Gaussian_Traits,
		"+",
		"""+""",
		Prefix);
	type Plus_GG_Test is new Plus_GG_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_GG_Test);

	procedure Run_Test (T : in out Plus_GG_Test) is
	begin
		T.Assert ((0, 0),  (0,  0),  (0,  0));
		T.Assert ((0, 0),  (1,  0),  (1,  0));
		T.Assert ((0, 0), (-1,  0), (-1,  0));
		T.Assert ((1, 0), (-1,  0),  (0,  0));
		T.Assert ((0, 0),  (0,  1),  (0,  1));
		T.Assert ((0, 0),  (0, -1),  (0, -1));
		T.Assert ((0, 1),  (0, -1),  (0,  0));
		T.Assert ((1, 0),  (0,  2),  (1,  2));
	end Run_Test;

	--

	package Minus_G_Case is new Self_Inverse_Unary_Function_Case (
		Gaussian_Traits,
		"-",
		"""-""",
		Prefix);

	type Minus_G_Test is new Minus_G_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_G_Test);

	procedure Run_Test (T : in out Minus_G_Test) is
	begin
		T.Assert_Identity ((0,  0));
		T.Assert ((1, 0), (-1,  0));
		T.Assert ((0, 1),  (0, -1));
		T.Assert ((2, 1), (-2, -1));
	end Run_Test;

	--

	package Minus_GI_Case is new Binary_Function_Case (
		Gaussian_Traits,
		Integer_Traits,
		Gaussian_Traits,
		"-",
		"""-""",
		Prefix);
	type Minus_GI_Test is new Minus_GI_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_GI_Test);

	procedure Run_Test (T : in out Minus_GI_Test) is
	begin
		T.Assert  ((0,  0),  0,  (0,  0));
		T.Assert  ((1,  0),  0,  (1,  0));
		T.Assert  ((0,  0),  1, (-1,  0));
		T.Assert ((-1,  0),  0, (-1,  0));
		T.Assert  ((0,  0), -1,  (1,  0));
		T.Assert  ((1,  0),  1,  (0,  0));
		T.Assert ((-1,  0), -1,  (0,  0));
		T.Assert  ((0,  1),  0,  (0,  1));
		T.Assert  ((0, -1),  0,  (0, -1));
		T.Assert  ((0,  1),  2, (-2,  1));
	end Run_Test;

	--

	package Minus_IG_Case is new Binary_Function_Case (
		Integer_Traits,
		Gaussian_Traits,
		Gaussian_Traits,
		"-",
		"""-""",
		Prefix);
	type Minus_IG_Test is new Minus_IG_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_IG_Test);

	procedure Run_Test (T : in out Minus_IG_Test) is
	begin
		T.Assert  (0,  (0,  0),  (0,  0));
		T.Assert  (1,  (0,  0),  (1,  0));
		T.Assert  (0,  (1,  0), (-1,  0));
		T.Assert (-1,  (0,  0), (-1,  0));
		T.Assert  (0, (-1,  0),  (1,  0));
		T.Assert  (1,  (1,  0),  (0,  0));
		T.Assert (-1, (-1,  0),  (0,  0));
		T.Assert  (0,  (0,  1),  (0, -1));
		T.Assert  (0,  (0, -1),  (0,  1));
		T.Assert  (1,  (0,  2),  (1, -2));
	end Run_Test;

	--

	package Minus_GG_Case is new Anticommutative_Binary_Function_Case (
		Gaussian_Traits,
		Gaussian_Traits,
		"-",
		"""-""",
		Prefix);
	type Minus_GG_Test is new Minus_GG_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_GG_Test);

	procedure Run_Test (T : in out Minus_GG_Test) is
	begin
		T.Assert  ((0,  0),  (0,  0),  (0,  0));
		T.Assert  ((1,  0),  (0,  0),  (1,  0));
		T.Assert ((-1,  0),  (0,  0), (-1,  0));
		T.Assert  ((1,  0),  (1,  0),  (0,  0));
		T.Assert ((-1,  0), (-1,  0),  (0,  0));
		T.Assert  ((0,  1),  (0,  0),  (0,  1));
		T.Assert  ((0, -1),  (0,  0),  (0, -1));
		T.Assert  ((0,  1),  (0,  1),  (0,  0));
		T.Assert  ((0, -1),  (0, -1),  (0,  0));
		T.Assert  ((1,  0),  (0,  2),  (1, -2));
	end Run_Test;

	--

	package Conjugate_Case is new Self_Inverse_Unary_Function_Case (
		Gaussian_Traits,
		Conjugate,
		"Conjugate",
		Prefix);

	type Conjugate_Test is new Conjugate_Case.Test with null record;
	overriding procedure Run_Test (T : in out Conjugate_Test);

	procedure Run_Test (T : in out Conjugate_Test) is
	begin
		T.Assert_Identity ((0, 0));
		T.Assert_Identity ((1, 0));
		T.Assert ((0, 1), (0, -1));
		T.Assert ((2, 1), (2, -1));
	end Run_Test;

	--

	package Norm_Case is new Unary_Function_Case (
		Gaussian_Traits,
		Integer_Traits,
		Norm,
		"Norm",
		Prefix);

	type Norm_Test is new Norm_Case.Test with null record;
	overriding procedure Run_Test (T : in out Norm_Test);

	procedure Run_Test (T : in out Norm_Test) is
	begin
		T.Assert  ((0,  0), 0);
		T.Assert  ((1,  0), 1);
		T.Assert ((-1,  0), 1);
		T.Assert  ((0,  1), 1);
		T.Assert  ((0, -1), 1);
		T.Assert  ((1,  1), 2);
		T.Assert  ((1, -1), 2);
		T.Assert  ((2,  0), 4);
		T.Assert  ((0,  2), 4);
	end Run_Test;

	--

	package Multiply_GI_Case is new Binary_Function_Case (
		Gaussian_Traits,
		Integer_Traits,
		Gaussian_Traits,
		"*",
		"""*""",
		Prefix);
	type Multiply_GI_Test is new Multiply_GI_Case.Test with null record;
	overriding procedure Run_Test (T : in out Multiply_GI_Test);

	procedure Run_Test (T : in out Multiply_GI_Test) is
	begin
		T.Assert  ((0,  0),  0,  (0,  0));
		T.Assert  ((1,  0),  0,  (0,  0));
		T.Assert  ((0,  0),  1,  (0,  0));
		T.Assert  ((0,  1),  0,  (0,  0));
		T.Assert  ((1,  1),  0,  (0,  0));
		T.Assert  ((1,  0),  1,  (1,  0));
		T.Assert  ((1,  0), -1, (-1,  0));
		T.Assert ((-1,  0),  1, (-1,  0));
		T.Assert ((-1,  0), -1,  (1,  0));
		T.Assert  ((0,  1),  1,  (0,  1));
		T.Assert  ((0, -1),  1,  (0, -1));
		T.Assert  ((0,  1), -1,  (0, -1));
		T.Assert  ((0, -1), -1,  (0,  1));
		T.Assert  ((1,  1),  1,  (1,  1));
	end Run_Test;

	--

	package Multiply_IG_Case is new Binary_Function_Case (
		Integer_Traits,
		Gaussian_Traits,
		Gaussian_Traits,
		"*",
		"""*""",
		Prefix);
	type Multiply_IG_Test is new Multiply_IG_Case.Test with null record;
	overriding procedure Run_Test (T : in out Multiply_IG_Test);

	procedure Run_Test (T : in out Multiply_IG_Test) is
	begin
		T.Assert  (0,  (0,  0),  (0,  0));
		T.Assert  (1,  (0,  0),  (0,  0));
		T.Assert  (0,  (1,  0),  (0,  0));
		T.Assert  (0,  (0,  1),  (0,  0));
		T.Assert  (0,  (1,  1),  (0,  0));
		T.Assert  (1,  (1,  0),  (1,  0));
		T.Assert  (1, (-1,  0), (-1,  0));
		T.Assert (-1,  (1,  0), (-1,  0));
		T.Assert (-1, (-1,  0),  (1,  0));
		T.Assert  (1,  (0,  1),  (0,  1));
		T.Assert  (1,  (0, -1),  (0, -1));
		T.Assert (-1,  (0,  1),  (0, -1));
		T.Assert (-1,  (0, -1),  (0,  1));
		T.Assert  (1,  (1,  1),  (1,  1));
	end Run_Test;

	--

	package Multiply_GG_Case is new Commutative_Binary_Function_Case (
		Gaussian_Traits,
		Gaussian_Traits,
		"*",
		"""*""",
		Prefix);
	type Multiply_GG_Test is new Multiply_GG_Case.Test with null record;
	overriding procedure Run_Test (T : in out Multiply_GG_Test);

	procedure Run_Test (T : in out Multiply_GG_Test) is
	begin
		T.Assert  ((0,  0),  (0,  0),  (0,  0));
		T.Assert  ((1,  0),  (0,  0),  (0,  0));
		T.Assert  ((0,  1),  (0,  0),  (0,  0));
		T.Assert  ((1,  1),  (0,  0),  (0,  0));
		T.Assert  ((1,  0),  (1,  0),  (1,  0));
		T.Assert  ((1,  0), (-1,  0), (-1,  0));
		T.Assert ((-1,  0), (-1,  0),  (1,  0));
		T.Assert  ((1,  0),  (0,  1),  (0,  1));
		T.Assert  ((1,  0),  (0, -1),  (0, -1));
		T.Assert ((-1,  0),  (0,  1),  (0, -1));
		T.Assert ((-1,  0),  (0, -1),  (0,  1));
		T.Assert  ((0,  1),  (0,  1), (-1,  0));
		T.Assert  ((0,  1),  (0, -1),  (1,  0));
		T.Assert  ((0, -1),  (0, -1), (-1,  0));
		T.Assert  ((1,  1),  (1,  1),  (0,  2));
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		Result : Access_Test_Suite := new Test_Suite;
	begin
		Result.Add_Test (new Plus_G_Test);
		Result.Add_Test (new Plus_GI_Test);
		Result.Add_Test (new Plus_IG_Test);
		Result.Add_Test (new Plus_GG_Test);
		Result.Add_Test (new Minus_G_Test);
		Result.Add_Test (new Minus_GI_Test);
		Result.Add_Test (new Minus_IG_Test);
		Result.Add_Test (new Minus_GG_Test);
		Result.Add_Test (new Conjugate_Test);
		Result.Add_Test (new Norm_Test);
		Result.Add_Test (new Multiply_GI_Test);
		Result.Add_Test (new Multiply_IG_Test);
		Result.Add_Test (new Multiply_GG_Test);
		return Result;
	end Suite;

end VSSL.Integers.Generic_Gaussian.Test;