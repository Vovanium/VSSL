with VSSL.Test_Cases, VSSL.Test_Cases.Traits;
use  VSSL.Test_Cases, VSSL.Test_Cases.Traits;
with VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Binary_Functions;

package body VSSL.Reals.Approximate.Test is

	Prefix : constant String := "VSSL.Reals.Approximate.";

	generic
		type Value is digits <>;
		Value_Name : String;
		with function Image (X : Value) return String;
		with function "=" (X, Y : Value) return Boolean is <>;
		with function "<=" (A, B : Value) return Boolean is <>;
	package Float_Type_Traits is
		package Parent is new Type_Traits (Value, Value_Name, Image, "=");
	end Float_Type_Traits;

	package Float_Traits is new Float_Type_Traits (Float, "Float", Float'Image);
	package Long_Float_Traits is new Float_Type_Traits (Long_Float, "Long_Float", Long_Float'Image);

	--

	generic
		with package Value_Traits is new Float_Type_Traits (<>);
		Abs_Error : Value_Traits.Value;
	package Abs_Tests is

		use type Value_Traits.Value;

		function Approx is new Generic_Absolute_Equal (
			Value => Value_Traits.Value,
			Absolute_Error => Abs_Error);

		package Equals_Case is new Binary_Function_Case (
			Value_Traits.Parent,
			Value_Traits.Parent,
			Boolean_Traits,
			Approx,
			"""=""",
			Prefix,
			" with Abs_Error => " & Value_Traits.Value'Image (Abs_Error));
		type Equals_Test is new Equals_Case.Test with null record;

		overriding procedure Run_Test (T : in out Equals_Test);

	end Abs_Tests;

	package body Abs_Tests is
		use type Value_Traits.Value;

		overriding procedure Run_Test (T : in out Equals_Test) is
		begin
			T.Assert (0.0, 0.0, True);
			T.Assert (1.0, 1.0, True);
			T.Assert (0.0, Abs_Error * 0.5, True);
			T.Assert (0.0, Abs_Error * 2.0, False);
			T.Assert (0.0, -Abs_Error * 0.5, True);
			T.Assert (0.0, -Abs_Error * 2.0, False);
			T.Assert (1.0, 1.0 + Abs_Error * 0.5, True);
			T.Assert (1.0, 1.0 + Abs_Error * 2.0, False);
			T.Assert (1.0, 1.0 - Abs_Error * 0.5, True);
			T.Assert (1.0, 1.0 - Abs_Error * 2.0, False);
		end Run_Test;

	end Abs_Tests;

	generic
		with package Value_Traits is new Float_Type_Traits (<>);
		Rel_Error : Value_Traits.Value;
	package Rel_Tests is

		use type Value_Traits.Value;

		function Approx is new Generic_Relative_Equal (
			Value => Value_Traits.Value,
			Relative_Error => Rel_Error);

		package Equals_Case is new Binary_Function_Case (
			Value_Traits.Parent,
			Value_Traits.Parent,
			Boolean_Traits,
			Approx,
			"""=""",
			Prefix,
			" with Rel_Error => " & Value_Traits.Value'Image (Rel_Error));
		type Equals_Test is new Equals_Case.Test with null record;

		overriding procedure Run_Test (T : in out Equals_Test);

	end Rel_Tests;

	package body Rel_Tests is
		use type Value_Traits.Value;

		overriding procedure Run_Test (T : in out Equals_Test) is
		begin
			T.Assert (0.0, 0.0, True);
			T.Assert (1.0, 1.0, True);
			T.Assert (1.0 + Rel_Error * 0.8, 1.0, True);
			T.Assert (1.0 + Rel_Error * 1.2, 1.0, False);
			T.Assert (1.0 - Rel_Error * 0.8, 1.0, True);
			T.Assert (1.0 - Rel_Error * 1.2, 1.0, False);
			T.Assert (-1.0 + Rel_Error * 0.5, -1.0, True);
			T.Assert (-1.0 + Rel_Error * 2.0, -1.0, False);
			T.Assert (-1.0 - Rel_Error * 0.5, -1.0, True);
			T.Assert (-1.0 - Rel_Error * 2.0, -1.0, False);
			T.Assert (100.0 + Rel_Error * 80.0,  100.0, True);
			T.Assert (100.0 + Rel_Error * 120.0, 100.0, False);
			T.Assert (100.0 - Rel_Error * 80.0,  100.0, True);
			T.Assert (100.0 - Rel_Error * 120.0, 100.0, False);
		end Run_Test;

	end Rel_Tests;

	generic
		with package Value_Traits is new Float_Type_Traits (<>);
		Abs_Error : Value_Traits.Value;
		Rel_Error : Value_Traits.Value;
	package AR_Tests is

		use type Value_Traits.Value;

		function Approx is new Generic_Equal (
			Value => Value_Traits.Value,
			Absolute_Error => Abs_Error,
			Relative_Error => Rel_Error);

		package Equals_Case is new Binary_Function_Case (
			Value_Traits.Parent,
			Value_Traits.Parent,
			Boolean_Traits,
			Approx,
			"""=""",
			Prefix,
			" with Abs_Error => " & Value_Traits.Value'Image (Abs_Error)
			& ", Rel_Error => " & Value_Traits.Value'Image (Rel_Error));
		type Equals_Test is new Equals_Case.Test with null record;

		overriding procedure Run_Test (T : in out Equals_Test);

	end AR_Tests;

	package body AR_Tests is
		use type Value_Traits.Value;

		overriding procedure Run_Test (T : in out Equals_Test) is
		begin
			T.Assert (0.0, 0.0, True);
			T.Assert (1.0, 1.0, True);
			T.Assert (Abs_Error * 0.8,  0.0, True);
			T.Assert (Abs_Error * 1.2,  0.0, False);
			T.Assert (-Abs_Error * 0.8, 0.0, True);
			T.Assert (-Abs_Error * 1.2, 0.0, False);
			T.Assert (0.01 + Abs_Error * 0.8, 0.01, True);
			T.Assert (0.01 + Abs_Error * 1.2, 0.01, False);
			T.Assert (0.01 - Abs_Error * 0.8, 0.01, True);
			T.Assert (0.01 - Abs_Error * 1.2, 0.01, False);
			T.Assert (1.0 + Rel_Error * 0.8, 1.0, True);
			T.Assert (1.0 + Rel_Error * 1.2, 1.0, False);
			T.Assert (1.0 - Rel_Error * 0.8, 1.0, True);
			T.Assert (1.0 - Rel_Error * 1.2, 1.0, False);
			T.Assert (-1.0 + Rel_Error * 0.5, -1.0, True);
			T.Assert (-1.0 + Rel_Error * 2.0, -1.0, False);
			T.Assert (-1.0 - Rel_Error * 0.5, -1.0, True);
			T.Assert (-1.0 - Rel_Error * 2.0, -1.0, False);
			T.Assert (100.0 + Rel_Error * 80.0,  100.0, True);
			T.Assert (100.0 + Rel_Error * 120.0, 100.0, False);
			T.Assert (100.0 - Rel_Error * 80.0,  100.0, True);
			T.Assert (100.0 - Rel_Error * 120.0, 100.0, False);
		end Run_Test;

	end AR_Tests;

	package AFT1 is new Abs_Tests (Float_Traits, 0.001);
	package ALFT1 is new Abs_Tests (Long_Float_Traits, 0.001);

	package RFT1 is new Rel_Tests (Float_Traits, 0.01);
	package RLFT1 is new Rel_Tests (Long_Float_Traits, 0.01);

	package FT1 is new AR_Tests (Float_Traits, 0.001, 0.01);
	package LFT1 is new AR_Tests (Long_Float_Traits, 0.001, 0.01);

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new AFT1.Equals_Test);
		R.Add_Test (new ALFT1.Equals_Test);
		R.Add_Test (new RFT1.Equals_Test);
		R.Add_Test (new RLFT1.Equals_Test);
		R.Add_Test (new FT1.Equals_Test);
		R.Add_Test (new LFT1.Equals_Test);
		return R;
	end Suite;

end VSSL.Reals.Approximate.Test;
