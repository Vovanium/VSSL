with VSSL.Floating_Point.Conversion_Test;
with VSSL.Floating_Point.Quad_Floats;
with VSSL.Floating_Point.Quad_Long_Floats;
with VSSL.Floating_Point.Generic_Quad_Reals.Test;
with VSSL.Floating_Point.Quaternions;
with VSSL.Floating_Point.Generic_Quaternions.Test;

package body VSSL.Floating_Point.Test is

	package Quad_Floats_Test
	is new Quad_Floats.Test (Real_Name => "Float");

	package Quad_Long_Floats_Test
	is new Quad_Long_Floats.Test (Real_Name => "Long_Float");

	package Quaternions_Test
	is new Quaternions.Test (Real_Name => "Float");

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (Conversion_Test.Suite);
		R.Add_Test (Quad_Floats_Test.Suite);
		R.Add_Test (Quad_Long_Floats_Test.Suite);
		R.Add_Test (Quaternions_Test.Suite);
		return R;
	end Suite;

end VSSL.Floating_Point.Test;
