with AUnit.Test_Suites;
use  AUnit.Test_Suites;

generic
	Real_Name : in String;
	Epsilon   : Real := Real'Delta;
package VSSL.Fixed_Point.Generic_Functions.Test is

	function Suite return Access_Test_Suite;

end VSSL.Fixed_Point.Generic_Functions.Test;