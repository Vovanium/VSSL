with VSSL.Floating_Point.String_to_Float;
with VSSL.Floating_Point.String_to_Long_Float;
with VSSL.Reals.Approximate;
with VSSL.Test_Cases;
use  VSSL.Test_Cases;
with VSSL.Test_Cases.Traits;
use  VSSL.Test_Cases.Traits;
with VSSL.Test_Cases.Unary_Functions;
use  VSSL.Test_Cases.Unary_Functions;

package body VSSL.Floating_Point.Conversion_Test is

	generic
		type Real_Type is digits <>;
		Real_Name : in String;
		with function String_to_Real (S : String) return Real_Type;
		Epsilon   : Real_Type := Real_Type'Model_Epsilon;
	package Generic_String_to_Float_Test is
		function Suite return Access_Test_Suite;
	end Generic_String_to_Float_Test;

	package body Generic_String_to_Float_Test is
		Prefix : constant String := "VSSL.Floating_Point.";

		function Approx is new Reals.Approximate.Generic_Relative_Equal (Real_Type, Epsilon);

		package Real_Traits is new Type_Traits (Real_Type, Real_Name, Real_Type'Image, Approx);

		package String_to_Real_Case is new Unary_Function_Case (
			String_Traits,
			Real_Traits,
			String_to_Real,
			"String_to_" & Real_Name,
			Prefix);

		type String_to_Real_Test is new String_to_Real_Case.Test with null record;
		overriding procedure Run_Test (T : in out String_to_Real_Test);

		procedure Run_Test (T : in out String_to_Real_Test) is
		begin
			-- Common formats (non exponential)
			T.Assert ("0",           0.0);
			T.Assert (" 0 ",         0.0);
			T.Assert ("1",           1.0);
			T.Assert ("2",           2.0);
			T.Assert ("34",         34.0);
			T.Assert (" 3_5 ",      35.0);
			T.Assert ("01 2",       12.0);
			T.Assert ("1.5",         1.5);
			T.Assert ("1.01",        1.01);
			T.Assert ("0.01",        0.01);
			T.Assert ("15.24",      15.24);
			T.Assert ("3.456789012", 3.456789012);
			T.Assert (".5",          0.5);
			T.Assert ("-0",          0.0);
			T.Assert ("-1",         -1.0);
			T.Assert ("-7",         -7.0);
			T.Assert ("4.5678901234567890123456789012345678901234567890",
			           4.5678901234567890123456789012345678901234567890);

			-- C- and Fortran-style exponential formats
			T.Assert ("1E3",         1.0E+3);
			T.Assert ("1_E3",        1.0E+3);
			T.Assert ("5E+3",        5.0E+3);
			T.Assert ("5E-3",        5.0E-3);
			T.Assert ("1.9E+2",      1.9E+2);
			T.Assert ("1.9E-2",      1.9E-2);
			T.Assert ("4E31",        4.0E+31);
			T.Assert ("4E-31",       4.0E-31);
			T.Assert ("5.2D16",      5.2E+16);
			T.Assert ("5.2D+16",     5.2E+16);
			T.Assert ("5.2D-16",     5.2E-16);

			-- Ada-style based formats
			T.Assert ("16#F#",       16#F.0#);
			T.Assert ("16#F",        16#F.0#);
			T.Assert ("16#Ed#",      16#ED.0#);
			T.Assert ("16#cd#",      16#CD.0#);
			T.Assert ("16:cd:",      16#CD.0#);
			T.Assert ("-16#cd#",    -16#CD.0#);
			T.Assert ("2#1#E0",       2#1.0#E+00);
			T.Assert ("2#1#E1",       2#1.0#E+01);
			T.Assert ("8#1#E1",       8#1.0#E+01);
			T.Assert ("8#1.1#E1",     8#1.1#E+01);
			T.Assert ("8#3.77#E2",    8#3.77#E+02);
			T.Assert ("8#3.77#e2",    8#3.77#E+02);
			T.Assert ("8:3.77:e2",    8#3.77#E+02);

			-- Algol-style based formats
			T.Assert ("16r7FFD.8",   16#7FFD.8#);
			T.Assert ("8r377",        8#377.0#);

			-- Fortan BOZ formats
			T.Assert ("B'1011_0101'", 2#1011_0101.0#);
			T.Assert ("O'376'",       8#376.0#);
			T.Assert ("Z'CAFE'",     16#CAFE.0#);
			T.Assert ("Z""DEAD""",   16#DEAD.0#);

			-- C-style hexadecimal and binary (NO ambiguous octals!)
			T.Assert ("0b1011_0101",  2#1011_0101.0#);
			T.Assert ("0B1111_0101",  2#1111_0101.0#);
			T.Assert ("0xBABE",      16#BABE.0#);
			T.Assert ("0xc0de",      16#C0DE.0#);
			T.Assert ("0XFACE8D",    16#FACE8D.0#);
			T.Assert ("0XFACE.8D",   16#FACE.8D#);
			T.Assert ("0x1p0",        2#0001.0#E0);
			T.Assert ("-0x1EFp0",    -2#0001_1110_1111.0#E0);
			T.Assert ("0xF.p-1",      2#1111.0#E-1);
			T.Assert ("0X0.123P-4",   2#0000.0001_0010_0011#E-4);
			T.Assert ("0x1.8p1",      3.0);

			-- Postfix (Asm-style) hexadecimal and binary formats
			T.Assert ("1001_1011B",   2#1001_1011.0#);
			T.Assert ("0101_1010b",   2#0101_1010.0#);
			T.Assert ("C0ACH",       16#C0AC.0#);
		end Run_Test;

		function Suite return Access_Test_Suite is
			R : Access_Test_Suite := new Test_Suite;
		begin
			R.Add_Test (new String_to_Real_Test);
			return R;
		end Suite;
	end Generic_String_to_Float_Test;

	package String_to_Float_Test is new Generic_String_to_Float_Test (
		Float, "Float", String_To_Float);

	package String_to_Long_Float_Test is new Generic_String_to_Float_Test (
		Long_Float, "Long_Float", String_To_Long_Float);

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (String_to_Float_Test.Suite);
		R.Add_Test (String_to_Long_Float_Test.Suite);
		return R;
	end Suite;

end VSSL.Floating_Point.Conversion_Test;
