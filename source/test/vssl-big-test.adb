with VSSL.Big.Bit_Count.Test;
with VSSL.Big.Numbers.Test;

package body VSSL.Big.Test is

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (VSSL.Big.Numbers.Test.Suite);
		R.Add_Test (VSSL.Big.Bit_Count.Test.Suite);
		return R;
	end Suite;

end VSSL.Big.Test;
