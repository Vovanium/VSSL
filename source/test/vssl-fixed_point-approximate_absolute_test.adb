with VSSL.Test_Cases;
use  VSSL.Test_Cases;
with VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Binary_Functions;
with VSSL.Fixed_Point.Generic_Approximate_Absolute;

package body VSSL.Fixed_Point.Approximate_Absolute_Test is

	Prefix : constant String := "VSSL.Fixed_Point.Approximate_Absolute.";

	package Boolean_Traits is new Type_Traits (Boolean, "Boolean", Boolean'Image);

	generic
		type Value is delta <>;
		Value_Name : String;
		with function Image (X : Value) return String;
		with function "=" (X, Y : Value) return Boolean is <>;
	package Fixed_Type_Traits is
		package Parent is new Type_Traits (Value, Value_Name, Image, "=");
	end Fixed_Type_Traits;

	generic
		with package Value_Traits is new Fixed_Type_Traits (<>);
		Abs_Error : Value_Traits.Value;
	package Tests is

		package Approx is new Generic_Approximate_Absolute (Value_Traits.Value, Abs_Error);

		package Equals_Case is new Binary_Function_Case (
			Value_Traits.Parent,
			Value_Traits.Parent,
			Boolean_Traits,
			Approx."=",
			"""=""",
			Prefix,
			" with Abs_Error => " & Value_Traits.Value'Image (Abs_Error));
		type Equals_Test is new Equals_Case.Test with null record;

		overriding procedure Run_Test (T : in out Equals_Test);

	end Tests;

	package body Tests is
		use type Value_Traits.Value;

		overriding procedure Run_Test (T : in out Equals_Test) is
		begin
			T.Assert (0.0, 0.0, True);
			T.Assert (1.0, 1.0, True);
			T.Assert (0.0, Abs_Error * 0.5, True);
			T.Assert (0.0, Abs_Error * 2.0, False);
			T.Assert (0.0, -Abs_Error * 0.5, True);
			T.Assert (0.0, -Abs_Error * 2.0, False);
			T.Assert (1.0, 1.0 + Abs_Error * 0.5, True);
			T.Assert (1.0, 1.0 + Abs_Error * 2.0, False);
			T.Assert (1.0, 1.0 - Abs_Error * 0.5, True);
			T.Assert (1.0, 1.0 - Abs_Error * 2.0, False);
		end Run_Test;

	end Tests;

	package Fixed_16_16_Traits is new Fixed_Type_Traits (Fixed_16_16, "Fixed_16_16", Fixed_16_16'Image);

	package FxT1 is new Tests (Fixed_16_16_Traits, 0.001);

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new FxT1.Equals_Test);
		return R;
	end Suite;

end VSSL.Fixed_Point.Approximate_Absolute_Test;
