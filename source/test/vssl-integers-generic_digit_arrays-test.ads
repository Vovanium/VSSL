with AUnit.Assertions, AUnit.Test_Suites;
use  AUnit.Assertions, AUnit.Test_Suites;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;

generic
	Digit_Name : in String;
package VSSL.Integers.Generic_Digit_Arrays.Test is

	function Suite return Access_Test_Suite;

end VSSL.Integers.Generic_Digit_Arrays.Test;
