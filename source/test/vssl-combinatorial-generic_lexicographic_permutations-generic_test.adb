with VSSL.Combinatorial.Functions;
use  VSSL.Combinatorial.Functions;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;
with AUnit.Assertions;
use  AUnit.Assertions;

package body VSSL.Combinatorial.Generic_Lexicographic_Permutations.Generic_Test is

	type Next_Lexicographic_Permutation_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Next_Lexicographic_Permutation_Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Next_Lexicographic_Permutation_Test);

	function Name (T : Next_Lexicographic_Permutation_Test) return AUnit.Message_String is
	begin
		return AUnit.Format ("VSSL.Combinatorial.Generic_Lexicographic_Permutations." &
			"Next_Lexicographic_Permutation test with " & Value_Name &
			", Initial'Length => " & Integer'Image (Initial'Length));
	end Name;

	procedure Run_Test (T : in out Next_Lexicographic_Permutation_Test) is
		X  : Set := Initial;
		XL : Set := Initial;
	begin
		for I in 2 .. Factorial (X'Length) loop
			XL := X;
			Next_Lexicographic_Permutation (X);
			for J in X'Range loop
				Assert (not (X (J) < XL (J)), "Not in lexicographic order (step " &
				Integer'Image (I) & " position " & Index'Image (J) & ")");
				exit when XL (J) < X (J);
			end loop;
		end loop;
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Next_Lexicographic_Permutation_Test);
		return R;
	end Suite;

end VSSL.Combinatorial.Generic_Lexicographic_Permutations.Generic_Test;