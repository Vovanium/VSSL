with VSSL.Reals.Approximate.Test;
with VSSL.Reals.String_Conversions.Test;

package body VSSL.Reals.Test is

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (Approximate.Test.Suite);
		R.Add_Test (String_Conversions.Test.Suite);
		return R;
	end Suite;

end VSSL.Reals.Test;
