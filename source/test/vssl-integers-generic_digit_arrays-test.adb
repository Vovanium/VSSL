package body VSSL.Integers.Generic_Digit_Arrays.Test is

	Null_Array : constant Digit_Array (0 .. -1) := (others => 0);

	--

	type First_Nonzero_Test is new Test_Case with null record;
	overriding function Name (T : First_Nonzero_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out First_Nonzero_Test);

	function Name (T : First_Nonzero_Test) return AUnit.Message_String is
	begin
		return AUnit.Format ("VSSL.Integers.Generic_Digit_Arrays.First_Nonzero test with "
		& Digit_Name);
	end Name;

	procedure Run_Test (T : in out First_Nonzero_Test) is
	begin
		Assert (First_Nonzero (Null_Array) = Index'Last, "First_Nonzero (()) = Index'Last");
		for I in Index range -500 .. 500 loop
			for J in -500 .. I loop
				Assert (First_Nonzero ((J .. I => 0)) = Index'Last,
				"First_Nonzero ((" & Index'Image (J) & " .. " &
				Index'Image (I) & " => 0)) = Index'Last");
			end loop;
		end loop;
		for I in Index range -200 .. 200 loop
			for J in Index range -200 .. I loop
				declare
					X : Digit_Array (J .. I) := (others => 0);
				begin
					for K in I .. J loop
						for M in K .. J loop
							X (K) := 1;
							X (M) := 1;
							Assert (First_Nonzero (X) = K,
							"FNZ1 " & Index'Image (J) & Index'Image (I) &
							Index'Image (K));
							X (K) := 0;
							X (M) := 0;
						end loop;
					end loop;
				end;
			end loop;
		end loop;
	end Run_Test;

	--

	type Last_Nonzero_Test is new Test_Case with null record;
	overriding function Name (T : Last_Nonzero_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Last_Nonzero_Test);

	function Name (T : Last_Nonzero_Test) return AUnit.Message_String is
	begin
		return AUnit.Format ("VSSL.Integers.Generic_Digit_Arrays.Last_Nonzero test with "
		& Digit_Name);
	end Name;

	procedure Run_Test (T : in out Last_Nonzero_Test) is
	begin
		Assert (Last_Nonzero (Null_Array) = Index'First, "Last_Nonzero (()) = Index'First");
		for I in Index range -500 .. 500 loop
			for J in Index range -500 .. I loop
				Assert (Last_Nonzero ((J .. I => 0)) = Index'First,
				"Last_Nonzero ((" & Index'Image (J) & " .. " &
				Index'Image (I) & " => 0)) = Index'First");
			end loop;
		end loop;
		for I in Index range -200 .. 200 loop
			for J in Index range -200 .. I loop
				declare
					X : Digit_Array (J .. I) := (others => 0);
				begin
					for K in I .. J loop
						for M in I .. K loop
							X (K) := 1;
							X (M) := 1;
							Assert (Last_Nonzero (X) = K,
							"LNZ1 " & Index'Image (J) & Index'Image (I) &
							Index'Image (K) & Index'Image (M));
							X (K) := 0;
							X (M) := 0;
						end loop;
					end loop;
				end;
			end loop;
		end loop;
	end Run_Test;

	--

	type Is_Fit_Test is new Test_Case with null record;
	overriding function Name (T : Is_Fit_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Is_Fit_Test);

	function Name (T : Is_Fit_Test) return AUnit.Message_String is
	begin
		return AUnit.Format ("VSSL.Integers.Generic_Digit_Arrays.Is_Fit test with "
		& Digit_Name);
	end Name;


	procedure Run_Test (T : in out Is_Fit_Test) is
		procedure Assert_Bounds (
			R : Boolean;
			I, J, M, N : Index)
		is
			X : constant Digit_Array := (I .. J => 1);
			Y : constant Digit_Array := (M .. N => 0);
		begin
			Assert (Is_Fit (X, Y) = R,
				"Is_Fit (([" & Index'Image (I) & ".." & Index'Image (J) & "] => 1),"
				& " ([" & Index'Image (M) & ".." & Index'Image (N) & "])) = "
				& Boolean'Image (R));
		end Assert_Bounds;
		procedure Assert_Bounds (
			R : Boolean;
			I, J, K, L, M, N : Index)
		is
			X : Digit_Array := (I .. L => 0);
			Y : constant Digit_Array := (M .. N => 0);
		begin
			X (J .. K) := (J .. K => 1);
			Assert (Is_Fit (X, Y) = R,
				"Is_Fit (([" & Index'Image (I) & ".. ] => 0 "
				& "[" & Index'Image (J) & ".." & Index'Image (K) & "] => 1 "
				& "[ .." & Index'Image (L) & "] => 0),"
				& " ([" & Index'Image (M) & ".." & Index'Image (N) & "])) = "
				& Boolean'Image (R));
		end Assert_Bounds;
	begin
		Assert (Is_Fit (Null_Array, Null_Array), "Is_Fit ((), ()) = True");
		for I in Index range -100 .. 100 loop
			for J in Index range -100 .. I loop
				declare
					X : Digit_Array (J .. I) := (others => 0);
				begin
					Assert (Is_Fit (Null_Array, X), "Is_Fit ((), (...)) = True");
					Assert (Is_Fit (X, Null_Array), "Is_Fit ((0...0), ()) = True");
					for K in X'Range loop
						X (K) := 1;
						Assert (not Is_Fit (X, Null_Array),
							"Is_Fit ((0...0 1 0...0), ()) = False");
						X (K) := 0;
					end loop;
				end;
				declare
					X : Digit_Array (J .. I) := (others => 1);
				begin
					Assert (not Is_Fit (X, Null_Array), "Is_Fit ((1...1), ()) = False");
				end;
			end loop;
		end loop;
		Assert_Bounds (True,  1, 2,  0, 3);
		Assert_Bounds (True,  1, 2,  1, 3);
		Assert_Bounds (True,  1, 2,  0, 2);
		Assert_Bounds (False, 0, 3,  1, 2);
		Assert_Bounds (False, 0, 2,  1, 3);
		Assert_Bounds (False, 1, 3,  0, 2);
		Assert_Bounds (False, 1, 3,  1, 2);
		Assert_Bounds (False, 0, 2,  1, 2);
		Assert_Bounds (False, 0, 1,  2, 3);
		Assert_Bounds (False, 2, 3,  0, 1);
		Assert_Bounds (True,  0, 2, 4, 6,  1, 5);
		Assert_Bounds (True,  0, 2, 4, 6,  2, 5);
		Assert_Bounds (True,  0, 2, 4, 6,  1, 4);
		Assert_Bounds (True,  0, 2, 4, 6,  2, 4);
		Assert_Bounds (False, 0, 2, 4, 6,  1, 3);
		Assert_Bounds (False, 0, 2, 4, 6,  3, 5);
		Assert_Bounds (False, 0, 2, 6, 8,  3, 5);
		-- todo: more tests needed
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new First_Nonzero_Test);
		R.Add_Test (new Last_Nonzero_Test);
		R.Add_Test (new Is_Fit_Test);
		return R;
	end Suite;

end VSSL.Integers.Generic_Digit_Arrays.Test;