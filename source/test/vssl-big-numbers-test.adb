with AUnit.Simple_Test_Cases;
with AUnit.Assertions;
use  AUnit.Assertions;
with VSSL.Test_Cases;
use  VSSL.Test_Cases;
with VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
with VSSL.Test_Cases.Unary_Procedures;
use  VSSL.Test_Cases.Unary_Procedures;

package body VSSL.Big.Numbers.Test is

	Prefix : constant String := "VSSL.Big.Numbers.";

	package Limb_Index_Traits is new Type_Traits (Limb_Index, "Limb_Index", Limb_Index'Image, "=");
	package Limb_Traits is new Type_Traits (Limb, "Limb", Limb'Image, "=");

	function Image_Naked (A : Big_Number) return String is (
		if A'Length = 0 then "."
		elsif A'Length = 1 then
			Limb_Index'Image (A'Last) & "=>" & Limb'Image (A (A'Last))
		else
			Image_Naked (A ((A'First + A'Last) / 2 + 1 .. A'Last)) & ","
			& Image_Naked (A (A'First .. (A'First + A'Last) / 2)));

	function Image_Internal (X : Big_Number) return String is ("[" & Image_Naked (X) & "]");
	-- Temporary image function
	-- @todo: Make and use normal image function that outputs decimal value

	package Big_Number_Traits is new Type_Traits (Big_Number, "Big_Number", Image_Internal, "=");

	package Sign_Value_Traits is new Type_Traits (Sign_Value, "Sign_Value", Sign_Value'Image, "=");

	--

	package Sign_Case is new Unary_Function_Case (
		Big_Number_Traits,
		Sign_Value_Traits,
		Sign,
		"Sign",
		Prefix);

	type Sign_Test is new Sign_Case.Test with null record;
	overriding procedure Run_Test (T : in out Sign_Test);

	procedure Run_Test (T : in out Sign_Test) is
	begin
		T.Assert (Zero,                             0);
		T.Assert (One,                             +1);
		T.Assert (Minus_One,                       -1);
		T.Assert ((1 => 1),                        -1);
		T.Assert ((1 => 0, 0 => 2**Limb_Bits - 1), +1);
		T.Assert ((2 => 1, 1 => 1),                +1);
	end Run_Test;

	--

	package Compare_Case is new Anticommutative_Binary_Function_Case (
		Big_Number_Traits,
		Sign_Value_Traits,
		Compare,
		"Compare",
		Prefix);

	type Compare_Test is new Compare_Case.Test with null record;
	overriding procedure Run_Test (T : in out Compare_Test);

	procedure Run_Test (T : in out Compare_Test) is
	begin
		T.Assert (Zero,      Zero,       0);
		T.Assert (One,       Zero,      +1);
		T.Assert (One,       One,        0);
		T.Assert (Zero,      Minus_One, +1);
		T.Assert (One,       Minus_One, +1);
	end Run_Test;

	--

	package Negate_Case is new Self_Inverse_Unary_Procedure_Case (
		Big_Number_Traits,
		Negate,
		"Negate",
		Prefix);

	type Negate_Test is new Negate_Case.Test with null record;
	overriding procedure Run_Test (T : in out Negate_Test);

	procedure Run_Test (T : in out Negate_Test) is
	begin
		T.Assert_Identity ((1 .. 0 => <>));
		T.Assert_Identity ((0 => 0));
		T.Assert ((1 => 0, 0 => 1), (1 => 1, 0 => 2**Limb_Bits - 1));
	end Run_Test;

	--

	type Make_with_Bits_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Make_with_Bits_Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Make_with_Bits_Test);

	function Name (T : Make_with_Bits_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format (Prefix & "Make_with_Bits test");
	end Name;

	procedure Assert (
		Highest_Bit : Integer;
		Lowest_Bit  : Integer;
		First_Limb  : Limb_Index;
		Last_Limb   : Limb_Index)
	is
		R : Big_Number := Make_with_Bits (Highest_Bit, Lowest_Bit);
		S : constant String := "Make_with_Bits (" & Integer'Image (Highest_Bit)
		                       & ", " & Integer'Image (Lowest_Bit) & ")";
	begin
		Limb_Index_Traits.Assert (R'First, First_Limb, S & "'First");
		Limb_Index_Traits.Assert (R'Last,  Last_Limb,  S & "'Last");
		for J in R'Range loop
			Limb_Traits.Assert (R (J), 0, S & " (" & Limb_Index'Image (J) & ")");
		end loop;
	end Assert;

	procedure Assert (
		Highest_Bit : Integer;
		Lowest_Bit  : Integer;
		Value       : Integer;
		Expected    : Big_Number)
	is
		R : Big_Number := Make_with_Bits (Highest_Bit, Lowest_Bit, Value);
		S : constant String := "Make_with_Bits (" & Integer'Image (Highest_Bit)
		                       & ", " & Integer'Image (Lowest_Bit) & ")";
	begin
		Limb_Index_Traits.Assert (R'First, Expected'First, S & "'First");
		Limb_Index_Traits.Assert (R'Last,  Expected'Last,  S & "'Last");
		for J in R'Range loop
			Limb_Traits.Assert (R (J), Expected (J), S & " (" & Limb_Index'Image (J) & ")");
		end loop;
	end Assert;

	procedure Run_Test (T : in out Make_with_Bits_Test) is
	begin
		Assert (0, 0,     0, 1);
		Assert (31, 0,    0, 1);
		Assert (32, 0,    0, 2);
		Assert (63, 0,    0, 2);
		Assert (64, 0,    0, 3);
		Assert (31, -1,  -1, 1);

		Assert (1, 0,  1, (1 => 0, 0 => 1));
		Assert (4, 0, 10, (1 => 0, 0 => 10));
		Assert (1, 0, -1, (1 => 1, 0 => 2**Limb_Bits - 1));
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Sign_Test);
		R.Add_Test (new Compare_Test);
		R.Add_Test (new Negate_Test);
		R.Add_Test (new Make_with_Bits_Test);
		return R;
	end Suite;

end VSSL.Big.Numbers.Test;
