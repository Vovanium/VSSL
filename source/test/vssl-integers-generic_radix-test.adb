package body VSSL.Integers.Generic_Radix.Test is

	type Order_Test is new Test_Case with null record;
	overriding function Name (T : Order_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Order_Test);

	function Name (T : Order_Test) return AUnit.Message_String is
	begin
		return AUnit.Format ("VSSL.Integers.Generic_Radix.Order test with "
		& Number_Name & ", "
		& Image (Default_Radix));
	end Name;

	procedure Run_Test (T : in out Order_Test) is
		X    : Number  := One;
		I    : Integer := 0;
		NR   : Number  := Default_Radix - One;
		J    : Integer;
	begin
		loop
		 	J := Order (X);
			Assert (I = J, "Order (" & Image (X) & ") is "
			& Integer'Image (J)
			& " expected " & Integer'Image (I));

			exit when not (X < Last - X / Default_Radix);
			X := X + X / Default_Radix / Default_Radix + One;
			if NR < X then
				I := I + 1;
				NR := (if NR < Last / Default_Radix then
					NR * Default_Radix + (Default_Radix - One)
				else
					Last);
			end if;
		end loop;
	end Run_Test;

	---

	type Digit_Test is new Test_Case with null record;
	overriding function Name (T : Digit_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Digit_Test);

	function Name (T : Digit_Test) return AUnit.Message_String is
	begin
		return AUnit.Format ("VSSL.Integers.Generic_Radix.Digit test with "
		& Number_Name & ", "
		& Image (Default_Radix));
	end Name;

	procedure Run_Test (T : in out Digit_Test) is
		I : Natural;
		V, ID, JD, KD : Number;
	begin
		I := 2;
		while Default_Radix**I < Last / Default_Radix**2 loop
			ID := Zero;
			while ID < Default_Radix loop
				for J in 1 .. I - 1 loop
					JD := Zero;
					while JD < Default_Radix loop
						for K in 0 .. J - 1 loop
							KD := Zero;
							while KD < Default_Radix loop
								V := ID * Default_Radix**I + JD * Default_Radix**J + KD * Default_Radix**K;
								Assert (Digit (V, I) = ID, "Digit I");
								Assert (Digit (V, J) = JD, "Digit J");
								Assert (Digit (V, K) = KD, "Digit K");
								KD := KD + One;
							end loop;
						end loop;
						JD := JD + One;
					end loop;
				end loop;
				ID := ID + One;
			end loop;
			I := I + 1;
		end loop;
	end Run_Test;

	---

	type Is_Palindrome_Test is new Test_Case with null record;
	overriding function Name (T : Is_Palindrome_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Is_Palindrome_Test);

	function Name (T : Is_Palindrome_Test) return AUnit.Message_String is
	begin
		return AUnit.Format ("VSSL.Integers.Generic_Radix.Is_Palindrome test with "
		& Number_Name & ", "
		& Image (Default_Radix));
	end Name;

	procedure Run_Test (T : in out Is_Palindrome_Test) is
		X, Y, Z : Number;
		Juu   : constant Number := Default_Radix;
		Hyaku : constant Number := Default_Radix**2;
		Sen   : constant Number := Default_Radix**3;
		procedure Assert_P (T : Boolean; A, B, C, D, E, F : Number := Zero) is
			V : Number := ((
				((F * Default_Radix + E) * Default_Radix + D)
			* Default_Radix + C) * Default_Radix + B) * Default_Radix + A;
		begin
			Assert (Is_Palindrome (V) = T, Image (V) & "is " & (if T then "" else "not ") & "palindrome");
		end Assert_P;
	begin
		Assert_P (True, Zero);
		X := One;
		while X < Default_Radix loop
			Assert_P (True, X);
			Assert_P (True, X, X);
			Assert_P (True, X, X, X);
			Assert_P (True, X, X, X, X);
			Y := Zero;
			while Y < Default_Radix loop
				if Y /= X then
					Assert_P (False, Y, X);
					Assert_P (True,  X, Y, X);
					Assert_P (True,  X, Y, Y, X);
					Z := Zero;
					while Z < Default_Radix loop
						if Z /= X and then Z /= Y then
							Assert_P (False, Z, Y, X);
							Assert_P (False, Z, Z, Y, X);
							Assert_P (False, Y, Z, Y, X);
							Assert_P (False, X, Z, Y, X);
							Assert_P (False, Z, Y, Y, X);
							Assert_P (False, Z, X, Y, X);
							Assert_P (False, Z, Y, X, X);
							if Default_Radix**2 < Last / Default_Radix**4 then -- Million
								Assert_P (True,  X, Y, Z, Y, X);
								Assert_P (True,  X, Y, Z, Z, Y, X);
							end if;
						end if;
						Z := Z + One;
					end loop;
				end if;
				Y := Y + One;
			end loop;

			X := X + One;
		end loop;
	end Run_Test;

	---

	function Suite return Access_Test_Suite is
		Result : Access_Test_Suite := new Test_Suite;
	begin
		Result.Add_Test (new Order_Test);
		Result.Add_Test (new Digit_Test);
		Result.Add_Test (new Is_Palindrome_Test);
		return Result;
	end Suite;

end VSSL.Integers.Generic_Radix.Test;
