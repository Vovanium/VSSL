--
-- Binary function (Y = f(X1, X2)) testing framework
--

package VSSL.Test_Cases.Binary_Functions is

	--
	-- Most generic case
	--
	generic
		with package Arg_1  is new Type_Traits (<>);
		with package Arg_2  is new Type_Traits (<>);
		with package Result is new Type_Traits (<>);
		with function Call (A : Arg_1.Value; B : Arg_2.Value) return Result.Value;
		Function_Name : String;
		Prefix        : String := "";
		Suffix        : String := "";
	package Binary_Function_Case is

		type Test is abstract new Test_Case with null record;
		overriding function Name (T : Test) return AUnit.Message_String;

		procedure Assert (
			T        : in out Test;
			A        : in     Arg_1.Value;
			B        : in     Arg_2.Value;
			Expected : in     Result.Value;
			Message  : in     String := "");
	end Binary_Function_Case;

	--
	-- Specific case for a binary functions that is commutative
	-- For commutative function following rule is met: f(x, y) = f(y, x)
	-- Both arguments should be of the same type
	--
	generic
		with package Args   is new Type_Traits (<>);
		with package Result is new Type_Traits (<>);
		with function Call (A : Args.Value; B : Args.Value) return Result.Value;
		Function_Name : String;
		Prefix        : String := "";
		Suffix        : String := "";
	package Commutative_Binary_Function_Case is

		type Test is abstract new Test_Case with null record;
		overriding function Name (T : Test) return AUnit.Message_String;

		procedure Assert (
			T        : in out Test;
			A, B     : in     Args.Value;
			Expected : in     Result.Value;
			Message  : in     String := "");
	end Commutative_Binary_Function_Case;

	--
	-- Specific case for a binary functions that is anti-commutative
	-- For anti-commutative function following rule is met: f(x, y) = -f(y, x)
	-- Both arguments should be of the same type
	--
	generic
		with package Args   is new Type_Traits (<>);
		with package Result is new Type_Traits (<>);
		with function Call (A : Args.Value; B : Args.Value) return Result.Value;
		Function_Name : String;
		Prefix        : String := "";
		Suffix        : String := "";
		with function "-" (A : Result.Value) return Result.Value is <>;
	package Anticommutative_Binary_Function_Case is

		type Test is abstract new Test_Case with null record;
		overriding function Name (T : Test) return AUnit.Message_String;

		procedure Assert (
			T        : in out Test;
			A, B     : in     Args.Value;
			Expected : in     Result.Value;
			Message  : in     String := "");
	end Anticommutative_Binary_Function_Case;

end VSSL.Test_Cases.Binary_Functions;
