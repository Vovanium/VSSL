with AUnit.Test_Suites;
use  AUnit.Test_Suites;

generic
	Value_Name : in String;
	Bits : Positive;
package VSSL.Integers.Modular_Binary.Generic_Test is

	function Suite return Access_Test_Suite;

end VSSL.Integers.Modular_Binary.Generic_Test;
