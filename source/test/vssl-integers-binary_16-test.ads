with VSSL.Integers.Modular_Binary.Generic_Test;

package VSSL.Integers.Binary_16.Test is
	new VSSL.Integers.Binary_16.Generic_Test (Value_Name => "Unsigned_16", Bits => 16);
