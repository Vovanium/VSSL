with AUnit.Test_Suites;
use  AUnit.Test_Suites;

generic
	Value_Name : in String;
	Initial    : in Set;
package VSSL.Combinatorial.Generic_Lexicographic_Permutations.Generic_Test is

	function Suite return Access_Test_Suite;

end VSSL.Combinatorial.Generic_Lexicographic_Permutations.Generic_Test;