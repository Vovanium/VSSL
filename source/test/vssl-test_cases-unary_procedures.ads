--
-- Unary procedures (that is Proc (X : in out)) testing framework
--
package VSSL.Test_Cases.Unary_Procedures is

	generic
		with package Arg_1  is new Type_Traits (<>);
		with procedure Call (A : in out Arg_1.Value);
		Procedure_Name : String;
		Prefix         : String := "";
		Suffix         : String := "";
	package Unary_Procedure_Case is

		type Test is abstract new Test_Case with null record;
		overriding function Name (T : Test) return AUnit.Message_String;

		procedure Assert (
			T        : in out Test;
			A_Pre    : in     Arg_1.Value;
			A_Post   : in     Arg_1.Value;
			Message  : in     String := "");

		procedure Assert_Identity (
			T        : in out Test;
			A        : in     Arg_1.Value;
			Message  : in     String := "");
	end Unary_Procedure_Case;

	generic
		with package Arg_1  is new Type_Traits (<>);
		with procedure Call (A : in out Arg_1.Value);
		Procedure_Name : String;
		Prefix         : String := "";
		Suffix         : String := "";
	package Self_Inverse_Unary_Procedure_Case is

		type Test is abstract new Test_Case with null record;
		overriding function Name (T : Test) return AUnit.Message_String;

		procedure Assert (
			T        : in out Test;
			A_Pre    : in     Arg_1.Value;
			A_Post   : in     Arg_1.Value;
			Message  : in     String := "");

		procedure Assert_Identity (
			T        : in out Test;
			A        : in     Arg_1.Value;
			Message  : in     String := "");
	end Self_Inverse_Unary_Procedure_Case;

end VSSL.Test_Cases.Unary_Procedures;
