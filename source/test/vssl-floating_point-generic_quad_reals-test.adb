with AUnit.Assertions;
use  AUnit.Assertions;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;
with VSSL.Test_Cases, VSSL.Test_Cases.Traits;
use  VSSL.Test_Cases, VSSL.Test_Cases.Traits;
with VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
with VSSL.Reals.Approximate;

package body VSSL.Floating_Point.Generic_Quad_Reals.Test is

	Prefix : constant String := "VSSL.Floating_Point.Generic_Quad_Reals.";

	Quad_Name : constant String := "Quad_" & Real_Name;


	function Image (A : Quad_Real) return String is ("("
		& Real_Type'Image (A (0)) & "+"
		& Real_Type'Image (A (1)) & "+"
		& Real_Type'Image (A (2)) & "+"
		& Real_Type'Image (A (3)) & ")");

	function Approx is new Reals.Approximate.Generic_Relative_Equal (Quad_Real, To_Quad_Real (Epsilon));

	package Quad_Real_Traits is new Type_Traits (Quad_Real, Quad_Name, Image, Approx);

	--function Image (X : Real_Type) return String renames Real_Type'Image;

	--package Real_Traits is new Type_Traits (Real_Type, Real_Name, Image, Approx);

	--

	package Plus_I_Case is new Self_Inverse_Unary_Function_Case (
		Quad_Real_Traits,
		"+",
		"""+""",
		Prefix);

	type Plus_I_Test is new Plus_I_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_I_Test);

	procedure Run_Test (T : in out Plus_I_Test) is
	begin
		T.Assert_Identity (To_Quad_Real (0.0));
		T.Assert_Identity (To_Quad_Real (1.0));
		T.Assert_Identity (Value ("12346.95959869395354354385354365"));
	end Run_Test;

	--

	package Minus_I_Case is new Self_Inverse_Unary_Function_Case (
		Quad_Real_Traits,
		"-",
		"""-""",
		Prefix);

	type Minus_I_Test is new Minus_I_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_I_Test);

	procedure Run_Test (T : in out Minus_I_Test) is
	begin
		T.Assert_Identity (To_Quad_Real (0.0));
		T.Assert (To_Quad_Real (1.0), To_Quad_Real (-1.0));
		T.Assert (To_Quad_Real (1.776E-19), To_Quad_Real (-1.776E-19));
		T.Assert (Value ("1.987654321987654321987654321"), Value ("-1.987654321987654321987654321"));
	end Run_Test;

	--

	package Plus_II_Case is new Commutative_Binary_Function_Case (
		Quad_Real_Traits,
		Quad_Real_Traits,
		"+",
		"""+""",
		Prefix);
	type Plus_II_Test is new Plus_II_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_II_Test);

	procedure Run_Test (T : in out Plus_II_Test) is
	begin
		T.Assert (To_Quad_Real (0.0), To_Quad_Real (0.0), To_Quad_Real (0.0));
		T.Assert (To_Quad_Real (0.0), To_Quad_Real (1.0), To_Quad_Real (1.0));
		T.Assert (To_Quad_Real (0.0), To_Quad_Real (-1.0), To_Quad_Real (-1.0));
		T.Assert (To_Quad_Real (1.0), To_Quad_Real (-1.0), To_Quad_Real (0.0));
		T.Assert (To_Quad_Real (16#1.0#E12), To_Quad_Real (1.0),
		          Value ("16#1_0000_0000_0001#"));
		T.Assert (Value ("16#1_0001_0001_0001_0000_0000_0000"),
		          Value ("16#0_0000_0000_0000_0000_0000_0001"),
		          Value ("16#1_0001_0001_0001_0000_0000_0001"));
		T.Assert (Value ("16#1_0001_0001_0001_0001_0001_0000"),
		          Value ("16#0_0000_0000_0000_0000_0000_0001"),
		          Value ("16#1_0001_0001_0001_0001_0001_0001"));
		T.Assert (Value ("16#1_0001_0000_0001_0000_0001_0000"),
		          Value ("16#0_0000_0001_0000_0001_0000_0001"),
		          Value ("16#1_0001_0001_0001_0001_0001_0001"));
		T.Assert (Value ("16#1_0001_0001_0001_0001_0001_0001_0000"),
		          Value ("16#0_0000_0000_0000_0000_0000_0000_0001"),
		          Value ("16#1_0001_0001_0001_0001_0001_0001_0001"));
		T.Assert (Value ("16#1_0000_0001_0000_0001_0000_0001_0000"),
		          Value ("16#0_0001_0000_0001_0000_0001_0000_0001"),
		          Value ("16#1_0001_0001_0001_0001_0001_0001_0001"));
		T.Assert (Value ("16#1.0000_0001_0000_0001_0000_0001_0000"),
		          Value ("16#0.0001_0000_0001_0000_0001_0000_0001"),
		          Value ("16#1.0001_0001_0001_0001_0001_0001_0001"));
		T.Assert (Value ("1_001_001_001_001_001_001_001_001_001_001"),
		          Value ("2_002_002_002_002_002_002_002_002_002_002"),
		          Value ("3_003_003_003_003_003_003_003_003_003_003"));
		T.Assert (Value ("1.001_001_001_001_001_001_001_001_001_001"),
		          Value ("2.002_002_002_002_002_002_002_002_002_002"),
		          Value ("3.003_003_003_003_003_003_003_003_003_003"));
	end Run_Test;

	--

	package Multiply_II_Case is new Commutative_Binary_Function_Case (
		Quad_Real_Traits,
		Quad_Real_Traits,
		"*",
		"""*""",
		Prefix);
	type Multiply_II_Test is new Multiply_II_Case.Test with null record;
	overriding procedure Run_Test (T : in out Multiply_II_Test);

	procedure Run_Test (T : in out Multiply_II_Test) is
	begin
		T.Assert (To_Quad_Real (0.0), To_Quad_Real (0.0), To_Quad_Real (0.0));
		T.Assert (To_Quad_Real (0.0), To_Quad_Real (1.0), To_Quad_Real (0.0));
		T.Assert (To_Quad_Real (1.0), To_Quad_Real (1.0), To_Quad_Real (1.0));
		T.Assert (To_Quad_Real (1.0), To_Quad_Real (-1.0), To_Quad_Real (-1.0));
		--T.Assert (Value ("1.001_001_001_001_001"),
		--          Value ("1.001_001_001_001_001"),
		--          Value ("1.002_003_004_005_006_005_004_003_002_001"));
		-- There's not enough exponent range in Float to represent these digits.
		-- Need some workaround for this (maybe there's a precision/range tradeoff)
	end Run_Test;

	--

	package Divide_II_Case is new Binary_Function_Case (
		Quad_Real_Traits,
		Quad_Real_Traits,
		Quad_Real_Traits,
		"/",
		"""/""",
		Prefix);
	type Divide_II_Test is new Divide_II_Case.Test with null record;
	overriding procedure Run_Test (T : in out Divide_II_Test);

	procedure Run_Test (T : in out Divide_II_Test) is
	begin
		T.Assert (To_Quad_Real (0.0), To_Quad_Real (1.0), To_Quad_Real (0.0));
		T.Assert (To_Quad_Real (1.0), To_Quad_Real (1.0), To_Quad_Real (1.0));
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Plus_I_Test);
		R.Add_Test (new Minus_I_Test);
		R.Add_Test (new Plus_II_Test);
		R.Add_Test (new Multiply_II_Test);
		R.Add_Test (new Divide_II_Test);
		return R;
	end Suite;

end VSSL.Floating_Point.Generic_Quad_Reals.Test;
