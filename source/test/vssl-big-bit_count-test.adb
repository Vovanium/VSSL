with AUnit.Simple_Test_Cases;
with AUnit.Assertions;
use  AUnit.Assertions;

package body VSSL.Big.Bit_Count.Test is

	Prefix : constant String := "VSSL.Big.Bit_Count.";

	type Bits_in_Number_Test is new AUnit.Simple_Test_Cases.Test_Case with null record;

	function Name (T : Bits_in_Number_Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Bits_in_Number_Test);

	function Name (T : Bits_in_Number_Test) return AUnit.Message_String is
		pragma Unreferenced (T);
	begin
		return AUnit.Format (Prefix & "Bits_in_Number test");
	end Name;

	procedure Assert (
		Digit_Count : Integer;
		Radix       : Radix_Type := 10;
		Expected    : Long_Integer)
	is
		Actual : constant Long_Integer := Bits_In_Number (Digit_Count, Radix);
		Params : constant String := " : Bits_In_Number (Digit_Count =>" & Integer'Image (Digit_Count)
			& ", Radix =>" & Radix_Type'Image (Radix) & ")";
	begin
		Assert (Actual >= Expected,
			Long_Integer'Image (Actual) & " >= " & Long_Integer'Image (Expected)
			& Params);
		Assert (abs (Actual - Expected) <= Expected / 1000,
			Long_Integer'Image (Actual) & " <= " & Long_Integer'Image (Expected) & " + 0.1%"
			& Params);
	end Assert;

	procedure Run_Test (T : in out Bits_in_Number_Test) is
	begin
		-- small Digit_Counts
		for J in Radix_Type range  2 .. 36 loop Assert (0, J, 0); end loop;

		for J in Radix_Type range  5 ..  8 loop Assert (1, J, 3); end loop;
		for J in Radix_Type range  9 .. 16 loop Assert (1, J, 4); end loop;
		for J in Radix_Type range 17 .. 32 loop Assert (1, J, 5); end loop;
		for J in Radix_Type range 33 .. 36 loop Assert (1, J, 6); end loop;

		for J in Radix_Type range  6 ..  8 loop Assert (2, J,  6); end loop;
		for J in Radix_Type range  9 .. 11 loop Assert (2, J,  7); end loop;
		for J in Radix_Type range 12 .. 16 loop Assert (2, J,  8); end loop;
		for J in Radix_Type range 17 .. 22 loop Assert (2, J,  9); end loop;
		for J in Radix_Type range 23 .. 32 loop Assert (2, J, 10); end loop;
		for J in Radix_Type range 33 .. 36 loop Assert (2, J, 11); end loop;

		for J in Radix_Type range  9 .. 10 loop Assert (3, J, 10); end loop;
		for J in Radix_Type range 11 .. 12 loop Assert (3, J, 11); end loop;
		for J in Radix_Type range 13 .. 16 loop Assert (3, J, 12); end loop;
		for J in Radix_Type range 17 .. 20 loop Assert (3, J, 13); end loop;
		for J in Radix_Type range 21 .. 25 loop Assert (3, J, 14); end loop;
		for J in Radix_Type range 26 .. 32 loop Assert (3, J, 15); end loop;
		for J in Radix_Type range 33 .. 36 loop Assert (3, J, 16); end loop;

		for J in Radix_Type range 10 .. 11 loop Assert (4, J, 14); end loop;
		for J in Radix_Type range 12 .. 13 loop Assert (4, J, 15); end loop;
		for J in Radix_Type range 14 .. 16 loop Assert (4, J, 16); end loop;
		for J in Radix_Type range 17 .. 19 loop Assert (4, J, 17); end loop;
		for J in Radix_Type range 20 .. 22 loop Assert (4, J, 18); end loop;
		for J in Radix_Type range 23 .. 26 loop Assert (4, J, 19); end loop;
		for J in Radix_Type range 27 .. 32 loop Assert (4, J, 20); end loop;
		for J in Radix_Type range 33 .. 36 loop Assert (4, J, 21); end loop;

		-- binary
		for J in 1 .. 2_000_000 loop
			Assert (Integer (J), 2, Long_Integer (J));
		end loop;
		-- ternary
		Assert (1, 3,  2);
		Assert (2, 3,  4);
		Assert (3, 3,  5);
		Assert (4, 3,  7);
		Assert (5, 3,  8);
		Assert (6, 3, 10);
		Assert (1_261_859, 3, 2_000_000);
		-- quaternary
		for J in 1 .. 1_000_000 loop
			Assert (Integer (J), 4, Long_Integer (J * 2));
		end loop;
		-- quinary
		Assert (2, 5,  5);
		Assert (3, 5,  7);
		Assert (4, 5, 10);
		Assert (861_353, 5, 2_000_000);
		-- senary
		Assert (3, 6, 8);
		Assert (4, 6, 11);
		Assert (773_705, 6, 1_999_999);
		-- septenary
		Assert (3, 7, 9);
		Assert (4, 7, 12);
		Assert (712_414, 7, 1_999_999);
		-- octal
		for J in 3 .. 666_666 loop
			Assert (Integer (J), 8, Long_Integer (J * 3));
		end loop;
		-- nonal
		Assert (4, 9, 13);
		Assert (630_929, 9, 1_999_998);
		-- decimal
		Assert (5, 10, 17);
		Assert (6, 10, 20);
		Assert (7, 10, 24);
		Assert (8, 10, 27);
		Assert (9, 10, 30);
		Assert (10, 10, 34);
		Assert (12, 10, 40);
		Assert (14, 10, 47);
		Assert (16, 10, 54);
		Assert (18, 10, 60);
		Assert (20, 10, 67);
		Assert (602_059, 10, 1_999_997);
		-- undecimal
		Assert (578_129, 11, 1_999_998);
		-- duodecimal
		Assert (557_885, 12, 1_999_997);
		-- tredecimal
		Assert (540_476, 13, 1_999_999);
		-- 14
		Assert (525_299, 14, 2_000_000);
		-- 15
		Assert (511_916, 15, 2_000_000);
		-- hexadecimal
		for J in 4 .. 500_000 loop
			Assert (Integer (J), 16, Long_Integer (J * 4));
		end loop;
		-- 17
		Assert (489_301, 17, 2_000_000);
		-- 18
		Assert (479_624, 18, 1_999_997);
		-- 19
		Assert (470_817, 19, 1_999_997);
		-- 20
		Assert (462_756, 20, 1_999_999);
		-- 21
		Assert (455_340, 21, 1_999_998);
		-- 22
		Assert (448_487, 22, 1_999_998);
		-- 23
		Assert (442_129, 23, 1_999_998);
		-- 24
		Assert (436_208, 24, 1_999_998);
		-- 25
		Assert (430_676, 25, 1_999_998);
		-- 26
		Assert (425_492, 26, 2_000_000);
		-- 27
		Assert (420_619, 27, 1_999_997);
		-- 28
		Assert (416_029, 28, 2_000_000);
		-- 29
		Assert (411_693, 29, 1_999_997);
		-- 30
		Assert (407_590, 30, 2_000_000);
		-- 31
		Assert (403_698, 31, 2_000_000);
		-- 32
		for J in 4 .. 400_000 loop
			Assert (Integer (J), 32, Long_Integer (J) * 5);
		end loop;
		-- 33
		Assert (396_479, 33, 1_999_997);
		-- 34
		Assert (393_123, 34, 1_999_999);
		-- 35
		Assert (389_918, 35, 2_000_000);
		-- 36
		Assert (386_852, 36, 1_999_996);
	end Run_Test;

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Bits_in_Number_Test);
		return R;
	end Suite;

end VSSL.Big.Bit_Count.Test;
