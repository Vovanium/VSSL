with AUnit.Assertions;
use  AUnit.Assertions;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;
with VSSL.Test_Cases, VSSL.Test_Cases.Traits;
use  VSSL.Test_Cases, VSSL.Test_Cases.Traits;
with VSSL.Test_Cases.Unary_Functions;
use  VSSL.Test_Cases.Unary_Functions;

package body VSSL.Extensible_Arithmetic.Test is

	Prefix : constant String := "VSSL.Extensible_Arithmetic.";

	package Limb_Traits is new Type_Traits (Limb, "Limb", Limb'Image, "=");

	--

	package Is_Negative_Case is new Unary_Function_Case (Limb_Traits, Boolean_Traits, Is_Negative, "Is_Negative", Prefix);

	type Is_Negative_Test is new Is_Negative_Case.Test with null record;
	overriding procedure Run_Test (T : in out Is_Negative_Test);

	procedure Run_Test (T : in out Is_Negative_Test) is
	begin
		T.Assert              (0, False);
		T.Assert              (1, False);
		T.Assert (Minus_One_Limb, True);
	end Run_Test;

	--

	type Negate_Test is new Test_Case with null record;
	overriding function Name (T : Negate_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Negate_Test);

	function Name (T : Negate_Test) return AUnit.Message_String is
		(AUnit.Format (Prefix & "Negate test"));

	procedure Run_Test (T : in out Negate_Test) is
	begin
		null;
	end Run_Test;

	--

	type Multiply_Test is new Test_Case with null record;
	overriding function Name (T : Multiply_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Multiply_Test);

	function Name (T : Multiply_Test) return AUnit.Message_String is
		(AUnit.Format (Prefix & "Multiply test"));

	procedure Run_Test (T : in out Multiply_Test) is
		procedure Assert (
			A, B, CI : in     Limb;
			ECO, EM  : in     Limb;
			Message  : in     String := "")
		is
			CO, M : Limb;
		begin
			Multiply (CO, M, A, B, CI);
			Limb_Traits.Assert (M, EM, "M of Multiply (CO, M, "
				& Limb'Image (A) & ", "
				& Limb'Image (B) & ", "
				& Limb'Image (CI) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
			Limb_Traits.Assert (CO, ECO, "CO of Multiply (CO, M, "
				& Limb'Image (A) & ", "
				& Limb'Image (B) & ", "
				& Limb'Image (CI) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;
		M1 : constant Limb := Minus_One_Limb;
		LA : constant Limb := 2 ** (Limb_Size - 2);
		PA : constant Limb := 2 ** (Limb_Size - 4);
	begin
		--       A   B  CI   CO M
		Assert  (0,  0, 0,   0, 0);
		Assert  (0,  1, 0,   0, 0);
		Assert  (1,  0, 0,   0, 0);
		Assert  (1,  1, 0,   0, 1);
		Assert  (0,  0, 1,   0, 1);
		Assert (LA, LA, 0,  PA, 0);
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Is_Negative_Test);
		R.Add_Test (new Multiply_Test);
		return R;
	end Suite;

end VSSL.Extensible_Arithmetic.Test;
