with AUnit.Test_Suites;
use  AUnit.Test_Suites;

generic
	Real_Name : in String;
	Epsilon   : Real := Real'Model_Epsilon;
package VSSL.Floating_Point.Generic_Quaternions.Test is

	function Suite return Access_Test_Suite;

end VSSL.Floating_Point.Generic_Quaternions.Test;