with AUnit.Test_Suites;
use  AUnit.Test_Suites;
generic
	Integer_Name : String;
package VSSL.Integers.Generic_Eisenstein.Test is

	function Suite return Access_Test_Suite;

end VSSL.Integers.Generic_Eisenstein.Test;