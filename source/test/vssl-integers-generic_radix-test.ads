with AUnit.Assertions, AUnit.Test_Suites;
use  AUnit.Assertions, AUnit.Test_Suites;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;

generic
	Number_Name : in String;
	with function "+" (Left, Right : Number) return Number is <>;
	with function "-" (Left, Right : Number) return Number is <>;
	with function "*" (Left, Right : Number) return Number is <>;
	with function "<" (Left, Right : Number) return Boolean is <>;
	with function Image (X : Number'Base) return String;
	Zero, One, Last : in Number;
package VSSL.Integers.Generic_Radix.Test is

	function Suite return Access_Test_Suite;

end VSSL.Integers.Generic_Radix.Test;
