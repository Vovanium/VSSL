with AUnit.Assertions;
use  AUnit.Assertions;

package body VSSL.Test_Cases is

	package body Type_Traits is

		procedure Assert (
			Actual,
			Expected : in Value;
			Message  : in String := "")
		is
		begin
			Assert (Actual = Expected,
				Image (Actual) & " = " & Image (Expected)
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

	end Type_Traits;

end VSSL.Test_Cases;
