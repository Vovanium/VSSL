package body VSSL.Test_Cases.Unary_Functions is

	package body Unary_Function_Case is

		overriding function Name (T : Test) return AUnit.Message_String is
		begin
			return AUnit.Format (Prefix & Function_Name & "(" & Arg_1.Value_Name & ")" & " test" & Suffix);
		end Name;

		procedure Assert (
			T        : in out Test;
			A        : in     Arg_1.Value;
			Expected : in     Result.Value;
			Message  : in     String := "")
		is
		begin
			Result.Assert (Call (A), Expected, Function_Name & " (" & Arg_1.Image (A) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

	end Unary_Function_Case;

	package body Odd_Unary_Function_Case is

		overriding function Name (T : Test) return AUnit.Message_String is
		begin
			return AUnit.Format (Prefix & Function_Name & "(" & Arg_1.Value_Name & ")" & " test" & Suffix);
		end Name;

		procedure Assert (
			T        : in out Test;
			A        : in     Arg_1.Value;
			Expected : in     Result.Value;
			Message  : in     String := "")
		is
			NA : constant Arg_1.Value := -A;
			NE : constant Result.Value := -Expected;
		begin
			Result.Assert (Call (A), Expected, Function_Name & " (" & Arg_1.Image (A) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
			Result.Assert (Call (NA), NE, Function_Name & " (" & Arg_1.Image (NA) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

	end Odd_Unary_Function_Case;

	package body Self_Inverse_Unary_Function_Case is

		overriding function Name (T : Test) return AUnit.Message_String is
		begin
			return AUnit.Format (Prefix & Function_Name & "(" & Traits.Value_Name & ")" & " test" & Suffix);
		end Name;

		procedure Assert (
			T        : in out Test;
			A, B     : in     Traits.Value;
			Message  : in     String := "")
		is
		begin
			Traits.Assert (Call (A), B, Function_Name & " (" & Traits.Image (A) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
			Traits.Assert (Call (B), A, Function_Name & " (" & Traits.Image (B) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert;

		procedure Assert_Identity (
			T        : in out Test;
			A        : in     Traits.Value;
			Message  : in     String := "")
		is
		begin
			Traits.Assert (Call (A), A, Function_Name & " (" & Traits.Image (A) & ")"
				& (if Message /= "" then " : " & Message
				else ""));
		end Assert_Identity;

	end Self_Inverse_Unary_Function_Case;

end VSSL.Test_Cases.Unary_Functions;