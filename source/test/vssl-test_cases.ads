--
-- Test frameworks for common function cases
--

with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;

package VSSL.Test_Cases is

	--
	-- A container for properties and methods of data type involved in testing
	--
	generic
		type Value (<>) is private;                             -- Type itself
		Value_Name : String;                                    -- Its name as a string
		with function Image (X : Value) return String is <>;    -- Type-to-string conversion function
		with function "=" (X, Y : Value) return Boolean is <>;  -- Comparison function
	package Type_Traits is

		procedure Assert (
			Actual,
			Expected : in Value;
			Message  : in String := "");
		-- Assertion function for equality testing

	end Type_Traits;

end VSSL.Test_Cases;
