with VSSL.Fixed_Point.Approximate_Absolute_Test;
with VSSL.Fixed_Point.Functions_16_16;
with VSSL.Fixed_Point.Generic_Functions.Test;

package body VSSL.Fixed_Point.Test is

	package Functions_16_16_Test
	is new Functions_16_16.Test (Real_Name => "Fixed_16_16");

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (Approximate_Absolute_Test.Suite);
		R.Add_Test (Functions_16_16_Test.Suite);
		return R;
	end Suite;

end VSSL.Fixed_Point.Test;
