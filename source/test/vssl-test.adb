with VSSL.Big.Test;
with VSSL.Champernowne_Constant.Test;
with VSSL.Combinatorial.Test;
with VSSL.Reals.Test;
with VSSL.Fixed_Point.Test;
with VSSL.Floating_Point.Test;
with VSSL.Integers.Test;
with VSSL.Extensible_Arithmetic.Test;

package body VSSL.Test is

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (VSSL.Big.Test.Suite);
		R.Add_Test (VSSL.Extensible_Arithmetic.Test.Suite);
		R.Add_Test (VSSL.Champernowne_Constant.Test.Suite);
		R.Add_Test (VSSL.Combinatorial.Test.Suite);
		R.Add_Test (VSSL.Reals.Test.Suite);
		R.Add_Test (VSSL.Fixed_Point.Test.Suite);
		R.Add_Test (VSSL.Floating_Point.Test.Suite);
		R.Add_Test (VSSL.Integers.Test.Suite);
		return R;
	end Suite;

end VSSL.Test;
