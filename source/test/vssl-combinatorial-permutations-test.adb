with AUnit.Assertions;
use  AUnit.Assertions;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;

package body VSSL.Combinatorial.Permutations.Test is

	generic
		type Item is private;
		with procedure Swap (A, B : in out Item);
		Item_A, Item_B : in Item;
		Item_Name : String;
		with function Image (X : Item) return String;
	package Swap_Test is


		type Test is new Test_Case with null record;

		overriding function Name (T : Test) return AUnit.Message_String;

		overriding procedure Run_Test (T : in out Test);
	end Swap_Test;

	package body Swap_Test is

		function Name (T : Test) return AUnit.Message_String is
			pragma Unreferenced (T);
		begin
			return AUnit.Format ("VSSL.Combinatorial.Permutations.Generic_Swap test with " & Item_Name);
		end Name;

		procedure Run_Test (T : in out Test) is
			Var_1 : Item := Item_A;
			Var_2 : Item := Item_B;
		begin
			Swap (Var_1, Var_2);
			Assert (Var_1 = Item_B,
			  "Var_1 actual " & Image (Var_1) & ", expected " & Image (Item_B));
			Assert (Var_2 = Item_A,
			  "Var_2 actual " & Image (Var_1) & ", expected " & Image (Item_A));
			Swap (Var_1, Var_2);
			Assert (Var_1 = Item_A,
			  "Var_1 actual " & Image (Var_1) & ", expected " & Image (Item_A));
			Assert (Var_2 = Item_B,
			  "Var_2 actual " & Image (Var_1) & ", expected " & Image (Item_B));
		end Run_Test;

	end Swap_Test;

	procedure Character_Swap is new Generic_Swap (Character);
	package Character_Swap_Test is new Swap_Test (Character, Character_Swap,
		'a', 'b', "Character", Character'Image);

	type Test_Array is array (1 .. 2) of Integer;

	function Image (X : Test_Array) return String is
	("(" & Integer'Image (X (1)) & ", " & Integer'Image (X (2)) & ")");

	procedure Array_Swap is new Generic_Swap (Test_Array);

	package Array_Swap_Test is new Swap_Test (Test_Array, Array_Swap,
		(1, 2), (-1, 3), "array", Image);

	---

	generic
		type Element is private;
		type Index is (<>);
		type Element_Array is array (Index range <>) of Element;
		with procedure Invert (A : in out Element_Array);
		Sample : Element_Array;
		Array_Name : String;
		with function Image (X : Element) return String;
	package Invert_Test is

		type Test is new Test_Case with null record;

		overriding function Name (T : Test) return AUnit.Message_String;

		overriding procedure Run_Test (T : in out Test);
	end Invert_Test;

	package body Invert_Test is

		function Name (T : Test) return AUnit.Message_String is
			pragma Unreferenced (T);
		begin
			return AUnit.Format ("VSSL.Combinatorial.Permutations.Generic_Invert test with " & Array_Name);
		end Name;

		procedure Run_Test (T : in out Test) is
			Var : Element_Array := Sample;
			J : Index;
		begin
			Invert (Var);
			J := Sample'Last;
			for I in Var'Range loop
				Assert (Var (I) = Sample (J),
				  "Var (I) actual " & Image (Var (I)) & ", expected " & Image (Sample (J)));
				if J /= Index'First then
					J := Index'Pred (J);
				end if;
			end loop;
		end Run_Test;

	end Invert_Test;

	procedure String_Invert is new Generic_Invert (
		Element       => Character,
		Index         => Positive,
		Element_Array => String);

	package String_Invert_Test is new Invert_Test (
		Element       => Character,
		Index         => Positive,
		Element_Array => String,
		Invert        => String_Invert,
		Sample        => "Hello world!",
		Array_Name    => "String",
		Image         => Character'Image);

	---

	generic
		type Element is private;
		Element_Name : String;
		with function To_Element (X : Integer) return Element;
		type Index is range <>;
		Index_Name : String;
		type Element_Array is array (Index range <>) of Element;
		with procedure Bit_Reversal (X : in out Element_Array);
	package Bit_Reversal_Test is
		type Test is new AUnit.Simple_Test_Cases.Test_Case with null record;
		function Name (T : Test) return AUnit.Message_String;
		procedure Run_Test (T : in out Test);
	end Bit_Reversal_Test;

	package body Bit_Reversal_Test is
		function Name (T : Test) return AUnit.Message_String is
			pragma Unreferenced (T);
		begin
			return AUnit.Format ("VSSL.Combinatorial.Permutations.Generic_Bit_Reversal test"
			& " with array (" & Index_Name & ") of " & Element_Name);
		end Name;

		procedure Run_Test (T : in out Test) is
			pragma Unreferenced (T);
			X : Element_Array (0 .. 255);
		begin
			for I in X'Range loop
				X (I) := To_Element (Integer (I));
			end loop;

			Bit_Reversal (X (0 .. 0));
			Assert (X (0) = To_Element (0), "X (0) = 0");

			Bit_Reversal (X (0 .. 1));
			Assert (X (0) = To_Element (0), "X (0) = 0");
			Assert (X (1) = To_Element (1), "X (1) = 1");

			Bit_Reversal (X (0 .. 3));
			Assert (X (0) = To_Element (0), "X (0) = 0");
			Assert (X (1) = To_Element (2), "X (1) = 2");
			Assert (X (2) = To_Element (1), "X (2) = 1");
			Assert (X (3) = To_Element (3), "X (3) = 3");
			Bit_Reversal (X (0 .. 3));

			Bit_Reversal (X (0 .. 15));
			Assert (X (0) = To_Element (0), "X (0) = 0");
			Assert (X (1) = To_Element (8), "X (1) = 8");
			Assert (X (2) = To_Element (4), "X (2) = 4");
			Assert (X (3) = To_Element (12), "X (3) = 12");
			Assert (X (5) = To_Element (10), "X (5) = 10");
			Bit_Reversal (X (0 .. 15));

			for I in X'Range loop
				Assert (X (I) = To_Element (Integer (I)), "X (I) = I, I = " & Index'Image (I));
			end loop;
		end Run_Test;
	end Bit_Reversal_Test;

	type Integer_Array is array (Natural range <>) of Integer;

	procedure Integer_Bit_Reversal is new Generic_Bit_Reversal (
		Element => Integer,
		Index   => Natural,
		Element_Array => Integer_Array);

	function Identity (X : Integer) return Integer is (X);

	package Integer_Bit_Reversal_Test is new Bit_Reversal_Test (
		Element       => Integer,
		Element_Name  => "Integer",
		To_Element    => Identity,
		Index         => Natural,
		Index_Name    => "Natural",
		Element_Array => Integer_Array,
		Bit_Reversal  => Integer_Bit_Reversal);

	---

	function Suite return Access_Test_Suite is
		Result : Access_Test_Suite := new Test_Suite;
	begin
		Result.Add_Test (new Character_Swap_Test.Test);
		Result.Add_Test (new Array_Swap_Test.Test);
		Result.Add_Test (new String_Invert_Test.Test);
		Result.Add_Test (new Integer_Bit_Reversal_Test.Test);
		return Result;
	end Suite;

end VSSL.Combinatorial.Permutations.Test;
