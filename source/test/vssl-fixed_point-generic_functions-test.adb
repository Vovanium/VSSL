with AUnit.Assertions;
use  AUnit.Assertions;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;
with VSSL.Test_Cases, VSSL.Test_Cases.Traits;
use  VSSL.Test_Cases, VSSL.Test_Cases.Traits;
with VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
with VSSL.Reals.Approximate;

package body VSSL.Fixed_Point.Generic_Functions.Test is

	Prefix : constant String := "VSSL.Fixed_Point.Generic_Functions.";

	function Approx is new Reals.Approximate.Generic_Absolute_Equal (Real, Epsilon);

	package Exact_Real_Traits is new Type_Traits (Real, Real_Name, Real'Image, "=");

	package Real_Traits is new Type_Traits (Real, Real_Name, Real'Image, Approx);

	type Rounding_Test is new Test_Case with null record;
	overriding function Name (T : Rounding_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Rounding_Test);

	overriding function Name (T : Rounding_Test) return AUnit.Message_String is
	begin
		return AUnit.Format (Prefix & "<Rounding>" & "(" & Real_Name & ")" & " test");
	end Name;

	overriding procedure Run_Test (T : in out Rounding_Test) is
		procedure Ast (Actual, Expected : Real; Message : String) renames Exact_Real_Traits.Assert;
		procedure Assert (
			A,
			E_Floor,
			E_Ceiling,
			E_Rounding,
			E_Unbiased,
			E_Truncation : Real)
		is
		begin
			Ast (Floor             (A), E_Floor,      "Floor ("             & Real'Image (A) & ")");
			Ast (Ceiling           (A), E_Ceiling,    "Ceiling ("           & Real'Image (A) & ")");
			Ast (Rounding          (A), E_Rounding,   "Rounding ("          & Real'Image (A) & ")");
			Ast (Unbiased_Rounding (A), E_Unbiased,   "Unbiased_Rounding (" & Real'Image (A) & ")");
			Ast (Truncation        (A), E_Truncation, "Truncation ("        & Real'Image (A) & ")");

		end Assert;
	begin
		--        X               Floor  Ceil Round  Unb  Trunc
		Assert  (0.0,              0.0,  0.0,  0.0,  0.0,  0.0);
		Assert  (1.0,              1.0,  1.0,  1.0,  1.0,  1.0);
		Assert  (2.0,              2.0,  2.0,  2.0,  2.0,  2.0);
		Assert  (3.0,              3.0,  3.0,  3.0,  3.0,  3.0);
		Assert (-1.0,             -1.0, -1.0, -1.0, -1.0, -1.0);
		Assert (-3.0,             -3.0, -3.0, -3.0, -3.0, -3.0);
		Assert  (0.2,              0.0,  1.0,  0.0,  0.0,  0.0);
		Assert  (0.5,              0.0,  1.0,  1.0,  0.0,  0.0);
		Assert  (0.8,              0.0,  1.0,  1.0,  1.0,  0.0);
		Assert  (1.3,              1.0,  2.0,  1.0,  1.0,  1.0);
		Assert  (1.5,              1.0,  2.0,  2.0,  2.0,  1.0);
		Assert  (1.7,              1.0,  2.0,  2.0,  2.0,  1.0);
		Assert  (2.5,              2.0,  3.0,  3.0,  2.0,  2.0);
		Assert  (3.3,              3.0,  4.0,  3.0,  3.0,  3.0);
		Assert (-0.2,             -1.0,  0.0,  0.0,  0.0,  0.0);
		Assert (-0.5,             -1.0,  0.0,  0.0,  0.0,  0.0);
		Assert (-0.7,             -1.0,  0.0, -1.0, -1.0,  0.0);
		Assert (-1.3,             -2.0, -1.0, -1.0, -1.0, -1.0);
		Assert (-1.5,             -2.0, -1.0, -1.0, -2.0, -1.0);
		Assert (-1.8,             -2.0, -1.0, -2.0, -2.0, -1.0);
		Assert  (1.0 - Real'Small, 0.0,  1.0,  1.0,  1.0,  0.0);
		Assert  (1.0 + Real'Small, 1.0,  2.0,  1.0,  1.0,  1.0);
		Assert  (1.5 - Real'Small, 1.0,  2.0,  1.0,  1.0,  1.0);
		Assert  (1.5 + Real'Small, 1.0,  2.0,  2.0,  2.0,  1.0);
	end Run_Test;

	--

	package Power_Case is new Binary_Function_Case (
		Real_Traits,
		Integer_Traits,
		Real_Traits,
		"**",
		"""**""",
		Prefix);

	type Power_Test is new Power_Case.Test with null record;
	overriding procedure Run_Test (T : in out Power_Test);

	procedure Run_Test (T : in out Power_Test) is
	begin
		T.Assert (2.0,  0, 1.0);  T.Assert (1.0,  0, 1.0); T.Assert (0.5,  0, 1.0);  T.Assert (1.5,  0, 1.0);
		T.Assert (2.0,  1, 2.0);  T.Assert (1.0,  1, 1.0); T.Assert (0.5,  1, 0.5);  T.Assert (1.5,  1, 1.5);
		T.Assert (2.0,  2, 4.0);  T.Assert (1.0,  2, 1.0); T.Assert (0.5,  2, 0.25); T.Assert (1.5,  2, 2.25);
		T.Assert (2.0, -1, 0.5);  T.Assert (1.0, -1, 1.0); T.Assert (0.5, -1, 2.0);  T.Assert (1.5, -1, 2.0 / 3.0);
		T.Assert (2.0, -2, 0.25); T.Assert (1.0, -2, 1.0); T.Assert (0.5, -2, 4.0);  T.Assert (1.5, -2, 4.0 / 9.0);
		T.Assert (1.1, 10, 1.1**10);
		T.Assert (0.9, 15, 0.9**15);
	end Run_Test;

	--

	package Real_Power_Case is new Binary_Function_Case (
		Real_Traits,
		Real_Traits,
		Real_Traits,
		"**",
		"""**""",
		Prefix);

	type Real_Power_Test is new Real_Power_Case.Test with null record;
	overriding procedure Run_Test (T : in out Real_Power_Test);

	procedure Run_Test (T : in out Real_Power_Test) is
	begin
		T.Assert (1.0,  0.0, 1.0); T.Assert (4.0,  0.0,  1.0);   T.Assert (0.25,  0.0, 1.0);    T.Assert (2.25,  0.0, 1.0);
		T.Assert (1.0,  1.0, 1.0); T.Assert (4.0,  1.0,  4.0);   T.Assert (0.25,  1.0, 0.25);   T.Assert (2.25,  1.0, 2.25);
		T.Assert (1.0,  2.0, 1.0); T.Assert (4.0,  2.0, 16.0);   T.Assert (0.25,  2.0, 0.0625); T.Assert (2.25,  2.0, 5.0625);
		T.Assert (1.0, -1.0, 1.0); T.Assert (4.0, -1.0,  0.25);  T.Assert (0.25, -1.0, 4.0);    T.Assert (2.25, -1.0, 4.0 / 9.0);
		T.Assert (1.0,  0.5, 1.0); T.Assert (4.0,  0.5,  2.0);   T.Assert (0.25,  0.5, 0.5);    T.Assert (2.25,  0.5, 1.5);
		T.Assert (1.0, -0.5, 1.0); T.Assert (4.0, -0.5,  0.5);   T.Assert (0.25, -0.5, 2.0);    T.Assert (2.25, -0.5, 2.0 / 3.0);
		T.Assert (1.0,  1.5, 1.0); T.Assert (4.0,  1.5,  8.0);   T.Assert (0.25,  1.5, 0.125);  T.Assert (2.25,  1.5, 3.375);
		T.Assert (1.0, -1.5, 1.0); T.Assert (4.0, -1.5,  0.125); T.Assert (0.25, -1.5, 8.0);    T.Assert (2.25, -1.5, 8.0 / 27.0);

		T.Assert (256.0, 0.125, 2.0);
		-- T.Assert (27.0, 1.0 / 3.0, 3.0); bad resolution
	end Run_Test;

	--

	package Sqrt_Case is new Unary_Function_Case (Real_Traits, Real_Traits, Sqrt, "Sqrt", Prefix);

	type Sqrt_Test is new Sqrt_Case.Test with null record;
	overriding procedure Run_Test (T : in out Sqrt_Test);

	procedure Run_Test (T : in out Sqrt_Test) is
	begin
		T.Assert   (0.0,      0.0);
		T.Assert   (0.5**12,  0.5**6); -- Note : small numbers are imprecise.
		T.Assert   (0.5**6,   0.5**3); --        Powers of two used as they're stored exactly
		T.Assert   (0.09,     0.3);
		T.Assert   (0.25,     0.5);
		T.Assert   (0.5,      0.7071067811865475244008443621048490392848359376884740365883398689);
		T.Assert   (1.0,      1.0);
		T.Assert   (2.0,      1.4142135623730950488016887242096980785696718753769480731766797379);
		T.Assert   (3.0,      1.7320508075688772935274463415058723669428052538103806280558069794);
		T.Assert   (4.0,      2.0);
		T.Assert   (9.0,      3.0);
		T.Assert  (10.0,      3.1622776601683793319988935444327185337195551393252168268575048527);
		T.Assert (100.0,     10.0);
		T.Assert (102.01,    10.1);
	end Run_Test;

	--

	package Cube_Root_Case is new Odd_Unary_Function_Case (Real_Traits, Real_Traits, Cube_Root, "Cube_Root", Prefix);

	type Cube_Root_Test is new Cube_Root_Case.Test with null record;
	overriding procedure Run_Test (T : in out Cube_Root_Test);

	procedure Run_Test (T : in out Cube_Root_Test) is
	begin
		T.Assert    (0.0,      0.0);
		T.Assert    (0.5**15,  0.5**5);
		T.Assert    (0.125,    0.5);
		T.Assert    (0.343,    0.7);
		T.Assert    (0.5,      0.7937005259840997373758528196361541301957466639499265049041428809);
		T.Assert    (1.0,      1.0);
		T.Assert    (2.0,      1.2599210498948731647672106072782283505702514647015079800819751121);
		T.Assert    (3.0,      1.4422495703074083823216383107801095883918692534993505775464161945);
		T.Assert    (8.0,      2.0);
		T.Assert   (10.0,      2.1544346900318837217592935665193504952593449421921085824892355063);
		T.Assert   (27.0,      3.0);
		T.Assert (1000.0,     10.0);
		T.Assert (1061.208,   10.2);
	end Run_Test;

	--

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Rounding_Test);
		R.Add_Test (new Power_Test);
		R.Add_Test (new Real_Power_Test);
		R.Add_Test (new Sqrt_Test);
		R.Add_Test (new Cube_Root_Test);
		return R;
	end Suite;

end VSSL.Fixed_Point.Generic_Functions.Test;