with AUnit.Assertions;
use  AUnit.Assertions;
with AUnit.Simple_Test_Cases;
use  AUnit.Simple_Test_Cases;
with VSSL.Test_Cases;
use  VSSL.Test_Cases;
with VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;
use  VSSL.Test_Cases.Unary_Functions, VSSL.Test_Cases.Binary_Functions;

package body VSSL.Integers.Big.Test is

	Prefix : constant String := "VSSL.Integers.Big.";

	function Image (X : Limb_Array) return String is (
		if X'Length = 0 then "."
		elsif X'Length = 1 then Limb'Image (X (X'Last))
		else Image (X ((X'First + X'Last) / 2 + 1 .. X'Last)) & ","
		   & Image (X (X'First .. (X'First + X'Last) / 2)));

	function Image (X : Big_Integer) return String is ("<" & Image (X.Data) & ">");
	-- Temporary image function
	-- @todo: Make and use normal image function that outputs decimal value

	package Big_Integer_Traits is new Type_Traits (Big_Integer, "Big_Integer", Image, "=");

	type Representation_Test is new Test_Case with null record;
	overriding function Name (T : Representation_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Representation_Test);

	function Name (T : Representation_Test) return AUnit.Message_String is
		(AUnit.Format (Prefix & "<Representation> test"));

	procedure Run_Test (T : in out Representation_Test) is
		procedure A (X, Y : Big_Integer) is
		begin
			Assert (X.Last = Y.Last, "Last: " & Limb_Index'Image (X.Last) & " = " & Limb_Index'Image (Y.Last));
			for J in X.Data'Range loop
				Assert (X.Data (J) = Y.Data (J),
				"Element (" & Limb_Index'Image (J) & "): "
				& Limb'Image (X.Data (J)) & " = " & Limb'Image (Y.Data (J)));
			end loop;
		end A;
	begin
		A (Zero,                               (Last => 0, Data => (0 => 0)));
		A (One,                                (Last => 0, Data => (0 => 16#0000_0001#)));
		A (Two,                                (Last => 0, Data => (0 => 16#0000_0002#)));
		A (Ten,                                (Last => 0, Data => (0 => 16#0000_000A#)));
		A (Minus_One,                          (Last => 0, Data => (0 => 16#FFFF_FFFF#)));
		A (With_Bits (31),                     (Last => 0, Data => (0 => 0)));
		A (With_Bits (32),                     (Last => 1, Data => (0 .. 1 => 0)));
		A (With_Digits (9),                    (Last => 0, Data => (0 => 0)));
		A (With_Digits (10),                   (Last => 1, Data => (0 .. 1 => 0)));
		A (From_String ("3"),                  (Last => 0, Data => (0 => 16#0000_0003#)));
		A (From_String ("1_000_000_000_000"),  (Last => 1, Data => (0 => 16#D4A5_1000#, 1 => 16#0000_00E8#)));
		A (From_String ("2#1000#"),            (Last => 0, Data => (0 => 16#0000_0008#)));
		A (From_String ("3#100#"),             (Last => 0, Data => (0 => 16#0000_0009#)));
		A (From_String ("4#100#"),             (Last => 0, Data => (0 => 16#0000_0010#)));
		A (From_String ("12#20#"),             (Last => 0, Data => (0 => 16#0000_0018#)));
		A (From_String ("-5"),                 (Last => 0, Data => (0 => 16#FFFF_FFFB#)));
		A (From_String ("-1_000_000_000_000"), (Last => 1, Data => (0 => 16#2B5A_F000#, 1 => 16#FFFF_FF17#)));
	end Run_Test;

	type Comparison_Test is new Test_Case with null record;
	overriding function Name (T : Comparison_Test) return AUnit.Message_String;
	overriding procedure Run_Test (T : in out Comparison_Test);

	function Name (T : Comparison_Test) return AUnit.Message_String is
		(AUnit.Format (Prefix & "<Comparison> test"));

	procedure Run_Test (T : in out Comparison_Test) is
	begin
		Assert (Is_Zero (Zero)      = True,  "Is_Zero (0) = True");
		Assert (Is_Zero (One)       = False, "Is_Zero (1) = False");
		Assert (Is_Zero (Minus_One) = False, "Is_Zero (-1) = False");

		Assert (Is_Negative (Zero)      = False, "Is_Negative (0) = False");
		Assert (Is_Negative (One)       = False, "Is_Negative (1) = False");
		Assert (Is_Negative (Minus_One) = True,  "Is_Negative (-1) = True");

		Assert (Is_Positive (Zero)      = False, "Is_Positive (0) = False");
		Assert (Is_Positive (One)       = True,  "Is_Positive (1) = True");
		Assert (Is_Positive (Minus_One) = False, "Is_Positive (-1) = False");

		Assert (Zero      =  Zero,      "0 = 0");
		Assert (One       =  One,       "1 = 1");
		Assert (Two       =  Two,       "2 = 2");
		Assert (Ten       =  Ten,       "10 = 10");
		Assert (Minus_One =  Minus_One, "-1 = -1");
		Assert (Zero      /= One,       "0 /= 1");
		Assert (Zero      /= Two,       "0 /= 2");
		Assert (One       /= Minus_One, "1 /= -1");
		Assert (Zero      /= Minus_One, "0 /= -1");
	end Run_Test;

	package Plus_I_Case is new Self_Inverse_Unary_Function_Case (
		Big_Integer_Traits,
		"+",
		"""+""", Prefix);
	type Plus_I_Test is new Plus_I_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_I_Test);

	procedure Run_Test (T : in out Plus_I_Test) is
	begin
		T.Assert_Identity (Zero);
		T.Assert_Identity (One);
		T.Assert_Identity (Two);
		T.Assert_Identity (Ten);
		T.Assert_Identity (From_String ("1_000_000_000_000"));
	end Run_Test;

	package Minus_I_Case is new Self_Inverse_Unary_Function_Case (
		Big_Integer_Traits,
		"-",
		"""-""", Prefix);
	type Minus_I_Test is new Minus_I_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_I_Test);

	procedure Run_Test (T : in out Minus_I_Test) is
	begin
		T.Assert_Identity (Zero);
		T.Assert (One,  Minus_One);
		T.Assert (Two,  From_String ("-2"));
		T.Assert (From_String ("1_000_000_000_000"), From_String ("-1_000_000_000_000"));
	end Run_Test;

	package Plus_II_Case is new Commutative_Binary_Function_Case (
		Big_Integer_Traits,
		Big_Integer_Traits,
		"+",
		"""+""", Prefix);
	type Plus_II_Test is new Plus_II_Case.Test with null record;
	overriding procedure Run_Test (T : in out Plus_II_Test);

	procedure Run_Test (T : in out Plus_II_Test) is
	begin
		T.Assert (Zero, Zero,      Zero);
		T.Assert (Zero, One,       One);
		T.Assert (Zero, Two,       Two);
		T.Assert (Zero, Ten,       Ten);
		T.Assert (One,  One,       Two);
		T.Assert (Zero, Minus_One, Minus_One);
		T.Assert (One,  Minus_One, Zero);
		T.Assert (Two,  Minus_One, One);

		T.Assert (From_String ("10_000_000_000"), One, From_String ("10_000_000_001"));

		T.Assert (From_String ("10_000_000_000"), From_String ("-10_000_000_000"), Zero);
	end Run_Test;

	package Minus_II_Case is new Anticommutative_Binary_Function_Case (
		Big_Integer_Traits,
		Big_Integer_Traits,
		"-",
		"""-""", Prefix);
	type Minus_II_Test is new Minus_II_Case.Test with null record;
	overriding procedure Run_Test (T : in out Minus_II_Test);

	procedure Run_Test (T : in out Minus_II_Test) is
	begin
		T.Assert (Zero, Zero,      Zero);
		T.Assert (One,  Zero,      One);
		T.Assert (Two,  Zero,      Two);
		T.Assert (Two,  One,       One);
		T.Assert (One,  Minus_One, Two);
	end Run_Test;

	package Multiplication_Case is new Commutative_Binary_Function_Case (
		Big_Integer_Traits,
		Big_Integer_Traits,
		"*",
		"""*""", Prefix);
	type Multiplication_Test is new Multiplication_Case.Test with null record;
	overriding procedure Run_Test (T : in out Multiplication_Test);

	procedure Run_Test (T : in out Multiplication_Test) is
	begin
		T.Assert (Zero,       Zero,      Zero);
		T.Assert (Zero,       One,       Zero);
		T.Assert (Zero,       Minus_One, Zero);
		T.Assert (Zero,       Two,       Zero);
		T.Assert (One,        One,       One);
		T.Assert (One,        Two,       Two);
		T.Assert (One,        Minus_One, Minus_One);
		T.Assert (Minus_One,  Minus_One, One);
		T.Assert (From_String ("1_000_000_000"), From_String ("1_000_000"), From_String ("1_000_000_000_000_000"));
		T.Assert (From_String ("1_000_000_000"), From_String ("-1_000_000"), From_String ("-1_000_000_000_000_000"));
		T.Assert (From_String ("10_000_000_000"), From_String ("1_000_000"), From_String ("10_000_000_000_000_000"));
		T.Assert (From_String ("10_000_000_000"), From_String ("100_000_000_000"), From_String ("1_000_000_000_000_000_000_000"));
		T.Assert (From_String ("10_000_000_000"), From_String ("-100_000_000_000"), From_String ("-1_000_000_000_000_000_000_000"));
		T.Assert (From_String ("-10_000_000_000"), From_String ("-100_000_000_000"), From_String ("1_000_000_000_000_000_000_000"));
	end Run_Test;

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Representation_Test);
		R.Add_Test (new Comparison_Test);
		R.Add_Test (new Plus_I_Test);
		R.Add_Test (new Minus_I_Test);
		R.Add_Test (new Plus_II_Test);
		R.Add_Test (new Minus_II_Test);
		R.Add_Test (new Multiplication_Test);
		return R;
	end Suite;

end VSSL.Integers.Big.Test;
