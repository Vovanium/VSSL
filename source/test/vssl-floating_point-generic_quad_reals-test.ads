with AUnit.Test_Suites;
use  AUnit.Test_Suites;

generic
	Real_Name : in String;
	Epsilon   : Real_Type := Real_Type'Model_Epsilon**4;
package VSSL.Floating_Point.Generic_Quad_Reals.Test is

	function Suite return Access_Test_Suite;

end VSSL.Floating_Point.Generic_Quad_Reals.Test;