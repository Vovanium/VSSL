pragma SPARK_Mode;

generic
	type Value is range <>;
package VSSL.Combinatorial.Generic_Functions is
	-- This package provides some combinatorics related integer functions
	-- Many of them rize very quickly as their argument increase
	-- however ordinary integer types have very limited range.
	-- This limits its allowed arguments by a non-overflowing range.

	function Factorial (N : Value) return Value with
		Pre => N >= 0,
		Post => Factorial'Result > 0,
		Global => null;
	-- Well-known factorial function, often denoted as n!

	function Factorial (N, A : Value) return Value with
		Pre => N >= 0 and A > 0,
		Post => Factorial'Result > 0,
		Global => null;
	-- Generalization of a factorial : n!(a) = n * (n - a) ...

	function Subfactorial (N : Value) return Value with
		Pre => N >= 0,
		Post => Subfactorial'Result > 0,
		Global => null;
	-- Number of derangements with N elements, often denoted as !n.

	function Permutations (N, R : Value) return Value with
		Pre => R >= 0 and N >= R,
		Post => Permutations'Result > 0,
		Global => null;
	-- Number of ways to arrange R elements picked from the set of N, often denoted as P (N, R).

	function Combinations (N, R : Value) return Value with
		Pre => R >= 0 and N >= R,
		Post => Combinations'Result > 0,
		Global => null;
	-- Number of ways to pick R elements form the set of N, often denoted as C (N, R).
	-- It can raise integer overflow while expected result is within it's type range.
	-- It's safe area is at leasth the same as the one of Permutation function.

	function Binomial (N, R : Value) return Value renames Combinations;
	-- Binomial coefficients (is just a synonym for Combinations).

end VSSL.Combinatorial.Generic_Functions;
