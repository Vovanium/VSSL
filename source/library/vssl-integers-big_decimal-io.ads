pragma SPARK_Mode;

with Ada.Text_IO; use Ada.Text_IO;

package VSSL.Integers.Big_Decimal.IO is

	procedure Put (
		File  : in File_Type;
		Item  : in Big_Decimal_Natural;
		Width : in Field := 0
	);

	procedure Put (
		Item  : in Big_Decimal_Natural;
		Width : in Field := 0
	);

end VSSL.Integers.Big_Decimal.IO;