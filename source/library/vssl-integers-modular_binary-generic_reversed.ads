pragma SPARK_Mode;

generic
	Default_Bits : Positive := Value'Size;
package VSSL.Integers.Modular_Binary.Generic_Reversed is

	Default_One : constant Value := 2**(Default_Bits - 1);

	function Bit_Reverse (X : Value) return Value;

	function Increment (X : Value; One : Value := Default_One)
		return Value with
		Pre => X < One * 2 and One <= Value'Last / 2 + 1;
	-- Bit reverse increment is an operation on reverse-bit order index
	-- Used in FFT and similar transforms.

end VSSL.Integers.Modular_Binary.Generic_Reversed;
