with VSSL.Floating_Point.Generic_Quad_Reals;

package VSSL.Floating_Point.Quad_Long_Floats is new Generic_Quad_Reals (Real_Type => Long_Float);
