with VSSL.Integers.Generic_Radix;
package VSSL.Integers.Radix is new VSSL.Integers.Generic_Radix (
	Number => Natural,
	Default_Radix => 10
) with Pure;