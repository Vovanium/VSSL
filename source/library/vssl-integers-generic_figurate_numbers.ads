--
-- Computing figurate numbers (triangular, pentagonal etc
--
pragma SPARK_Mode;

generic
	type Value is range <>;
package VSSL.Integers.Generic_Figurate_Numbers is

	function Triangular (X : Value) return Value
	with
		Global => null;
	-- X-th triangular number (X * (X + 1) / 2)

	function Pentagonal (X : Value) return Value
	with
		Global => null;
	-- X-th pentagonal number (X * (3 * X - 1) / 2)

	function Hexagonal (X : Value) return Value
	with
		Global => null;
	-- X-th hexagonal number (X * (2 * X - 1))

	function Heptagonal (X : Value) return Value
	with
		Global => null;

	function Octagonal (X : Value) return Value
	with
		Global => null;

	function Nonagonal (X : Value) return Value
	with
		Global => null;

	function Decagonal (X : Value) return Value
	with
		Global => null;

	function Dodecagonal (X : Value) return Value
	with
		Global => null;

end VSSL.Integers.Generic_Figurate_Numbers;
