package body VSSL.Integers.Divisors is

	function Sum_Of_Divisors (X : Positive) return Positive is
		K, L : Natural;
		S    : Positive;
	begin
		S := 1;
		K := 2;
		while K * K < X loop
			L := X / K;
			if K * L = X then -- division test
				S := S + K + L;
			end if;
			K := K + 1;
		end loop;
		if K * K = X then -- special case: square
			S := S + K;
		end if;
		return S;
	end Sum_Of_Divisors;

end VSSL.Integers.Divisors;
