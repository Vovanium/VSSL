--
-- Eisenstein integers
--
generic
	type Integer_Type is range <>;
package VSSL.Integers.Generic_Eisenstein is

	type Eisenstein_Integer is array (1 .. 2) of Integer_Type'Base;

	Omega : constant Eisenstein_Integer := (0, 1);
	ω     : constant Eisenstein_Integer := Omega;

	function "+" (Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (Y);
	function "+" (X : Eisenstein_Integer; Y : Integer_Type'Base) return Eisenstein_Integer
	 is (X (1) + Y, X (2));
	function "+" (X : Integer_Type'Base; Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (X + Y (1), Y (2));
	function "+" (X, Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (X (1) + Y (1), X (2) + Y (2));

	function "-" (Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (-Y (1), -Y (2));
	function "-" (X : Eisenstein_Integer; Y : Integer_Type'Base) return Eisenstein_Integer
	 is (X (1) - Y, X (2));
	function "-" (X : Integer_Type'Base; Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (X - Y (1), -Y (2));
	function "-" (X, Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (X (1) - Y (1), X (2) - Y (2));

	function Conjugate (Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (Y (1), Y (1) - Y (2));

	function Norm (Y : Eisenstein_Integer) return Integer_Type'Base
	 is (Y (1)**2 - Y (1) * Y (2) + Y (2)**2);

	function "*" (X : Eisenstein_Integer; Y : Integer_Type'Base) return Eisenstein_Integer
	 is (X (1) * Y, X (2) * Y);
	function "*" (X : Integer_Type'Base; Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (X * Y (1), X * Y (2));
	function "*" (X, Y : Eisenstein_Integer) return Eisenstein_Integer
	 is (X (1) * Y (1) - X (2) * Y (2), X (2) * Y (1) + X (1) * Y (2) - X (2) * Y (2));

end VSSL.Integers.Generic_Eisenstein;

--                (2)
--                  \
--               .   ω   .   .   .
--                    \
--             .   .   0___1___.___. (1)
--
--               .   .   .   .   .   .
--
--