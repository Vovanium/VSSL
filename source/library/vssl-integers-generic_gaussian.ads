--
-- Gaussian integers, complex numbers whose real and imaginary part are integer
--
generic
	type Integer_Type is range <>;
package VSSL.Integers.Generic_Gaussian is

	type Gaussian_Integer is record
		Re, Im : Integer_Type;
	end record;

	I, J : constant Gaussian_Integer := (0, 1);

	function "+" (Y : Gaussian_Integer) return Gaussian_Integer
	 is (Y);
	function "+" (X : Gaussian_Integer; Y : Integer_Type'Base) return Gaussian_Integer
	 is (X.Re + Y, X.Im);
	function "+" (X : Integer_Type'Base; Y : Gaussian_Integer) return Gaussian_Integer
	 is (X + Y.Re, Y.Im);
	function "+" (X, Y : Gaussian_Integer) return Gaussian_Integer
	 is (X.Re + Y.Re, X.Im + Y.Im);

	function "-" (Y : Gaussian_Integer) return Gaussian_Integer
	 is (-Y.Re, -Y.Im);
	function "-" (X : Gaussian_Integer; Y : Integer_Type'Base) return Gaussian_Integer
	 is (X.Re - Y, X.Im);
	function "-" (X : Integer_Type'Base; Y : Gaussian_Integer) return Gaussian_Integer
	 is (X - Y.Re, -Y.Im);
	function "-" (X, Y : Gaussian_Integer) return Gaussian_Integer
	 is (X.Re - Y.Re, X.Im - Y.Im);

	function Conjugate (Y : Gaussian_Integer) return Gaussian_Integer
	 is (Y.Re, -Y.Im);

	function Norm (Y : Gaussian_Integer) return Integer_Type'Base
	 is (Y.Re**2 + Y.Im**2);

	function "*" (X : Gaussian_Integer; Y : Integer_Type'Base) return Gaussian_Integer
	 is (X.Re * Y, X.Im * Y);
	function "*" (X : Integer_Type'Base; Y : Gaussian_Integer) return Gaussian_Integer
	 is (X * Y.Re, X * Y.Im);
	function "*" (X, Y : Gaussian_Integer) return Gaussian_Integer
	 is (X.Re * Y.Re - X.Im * Y.Im, X.Im * Y.Re + X.Re * Y.Im);

end VSSL.Integers.Generic_Gaussian;
