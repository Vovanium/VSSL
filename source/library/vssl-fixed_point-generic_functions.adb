with Ada.Numerics;
with Ada.Unchecked_Conversion;

package body VSSL.Fixed_Point.Generic_Functions is

	Fraction_Mask : constant Representation := 2**Fraction_Bits - 1;
	Integral_Mask : constant Representation := not Fraction_Mask;
	Real_Last     : constant Representation := 2**(Representation'Size - 1) - 1;

	function To_Representation is new Ada.Unchecked_Conversion (Real, Representation);
	-- This should be analogous to GNAT's Real'Integer_Value
	function From_Representation is new Ada.Unchecked_Conversion (Representation, Real);
	-- This should be analogous to GNAT's Real'Fixed_Value

	function Shift_Left (X : Real; N : Natural) return Real
	is (From_Representation (Shift_Left (To_Representation (X), N)));

	function Shift_Right (X : Real; N : Natural) return Real
	is (From_Representation (Shift_Right_Arithmetic (To_Representation (X), N)));

	function Floor (X : Real) return Real
	is (From_Representation (To_Representation (X) and Integral_Mask));

	function Ceiling (X : Real) return Real is
		R : constant Representation := To_Representation (X);
	begin
		if R in Real_Last - Fraction_Mask + 1 .. Real_Last then
			raise Constraint_Error;
		end if;
		return From_Representation ((R + Fraction_Mask) and Integral_Mask);
	end Ceiling;

	function Rounding (X : Real) return Real is
		R : constant Representation := To_Representation (X);
		M : constant Representation := 2**(Fraction_Bits - 1); -- rounding offset
	begin
		if R in Real_Last - M + 1 .. Real_Last then
			raise Constraint_Error;
		end if;
		return From_Representation ((R + M) and Integral_Mask);
	end Rounding;

	function Unbiased_Rounding (X : Real) return Real is
		R : constant Representation := To_Representation (X);
		M : constant Representation := 2**(Fraction_Bits - 1) - 1
		  + (Shift_Right_Arithmetic (R, Fraction_Bits) and 1);
	begin
		if R in Real_Last - M + 1 .. Real_Last then
			raise Constraint_Error;
		end if;
		return From_Representation ((R + M) and Integral_Mask);
	end Unbiased_Rounding;

	function Truncation (X : Real) return Real is
		R : constant Representation := To_Representation (X);
		M : constant Representation := Fraction_Mask and Shift_Right_Arithmetic (R, Representation'Size - 1);
	begin
		if R in Real_Last - M + 1 .. Real_Last then
			raise Constraint_Error;
		end if;
		return From_Representation ((R + M) and Integral_Mask);
	end Truncation;

	function Copy_Sign (X, Sign : Real) return Real is
		XR : constant Representation := To_Representation (X);
		SR : constant Representation := To_Representation (Sign);
		H  : constant Representation := Shift_Right_Arithmetic (XR xor SR, Representation'Size - 1);
		-- this becomes 1111...1111 if sign need to be changed, 0000...0000 otherwise
	begin
		return From_Representation ((XR xor H) - H); -- binary magic
	end Copy_Sign;

	function "**" (X : Real; N : Integer'Base) return Real is
		X_R : Real := X;
		N_R : Integer'Base := N;
		Y   : Real := 1.0;
	begin
		if N < 0 then
			X_R := Real'(1.0) / X_R;
			--N_R := -N;
		end if;
		loop
			if N_R mod 2 /= 0 then
				Y := Y * X_R;
			end if;
			N_R := N_R / 2; -- as long as division rounds towards zero, this should work with negatives too
			exit when N_R = 0;
			X_R := X_R * X_R;
		end loop;
		return Y;
	end "**";

	function "**" (X, N : Real) return Real is
		XR : Real := X;
		NR : Real := N;
		NI : Integer; -- integer part of power (let's believe it will not overflow)
		XI : Real;
		Y  : Real := 1.0;
	begin
		if N < 0.0 then
			XR := Real'(1.0) / X;
			NR := -N;
		end if;
		NI := Integer (NR);

		if Real (NI) > NR then
			NI := NI - 1;
		end if;

		NR := NR - Real (NI); -- Leave fractional part

		-- Integer part
		XI := XR;
		loop
			if NI mod 2 /= 0 then
				Y := Y * XI;
			end if;
			NI := NI / 2;
			exit when NI = 0;
			XI := XI * XI;
		end loop;

		-- Fractional part
		while NR >= Real'Delta loop
			XR := Sqrt (XR);
			NR := NR * Real'(2.0);
			if NR >= 1.0 then
				Y := Y * XR;
				NR := NR - 1.0;
			end if;
		end loop;
		return Y;
	end "**";
	-- Function uses digit-by-digit squaring which I believe is most precise


	function Sqrt (X : Real) return Real is
		Y   : Real;
		R   : Real;
		N   : Natural := 20; -- Iteration counter
		RSqrt5 : constant := 0.447213595; -- 1 / Sqrt(5)
	begin
		if X < 0.0 then
			raise Ada.Numerics.Argument_Error with "Negative argument to square root";
		elsif X in 0.0 | 1.0 then
			return X;
		end if;

		R := X;
		Y := RSqrt5;
		if R >= 4.0 then
			loop
				R := R * Real'(0.25**2);
				Y := Y * Real'(4.0);
				exit when R < 4.0;
			end loop;
		elsif R < 0.25 then
			loop
				R := R * Real'(4.0**2);
				Y := Y * Real'(0.25);
				exit when R >= 0.25;
			end loop;
		end if;

		Y := Y * (R + 1.0);
		-- 1/sqrt(5) * (X + 1) approximation for 1/4 .. 4 argument range
		-- gives less than ± 0.105 relative error
		loop
			R := X / Y;
			exit when abs (R - Y) <= Real'Delta or N = 0;
			Y := Real'(0.5) * (Y + R);
			N := N - 1;
		end loop;
		return Y;
	end Sqrt;
	 -- Function implements Heron's method.
	 -- It gives good convergence (about 4 iterations average, 10 in worst case for 32 bit type)

	function Cube_Root (X : Real) return Real is
		Y   : Real;
		R   : Real;
		N   : Natural := 48; -- Iteration counter
	begin
		if X in 0.0 | 1.0 | -1.0 then
			return X;
		end if;

		R := abs X;
		Y := (if X < 0.0 then -0.312 else 0.312);
		if R >= 2.828427125 then -- Sqrt (8)
			loop
				R := R * Real'(0.5**3);
				Y := Y * Real'(2.0);
				exit when R < 2.828427125;
			end loop;
		elsif abs R < 0.3535533906 then -- 1 / sqrt (8)
			loop
				R := R * Real'(2.0**3);
				Y := Y * Real'(0.5);
				exit when R >= 0.3535533906;
			end loop;
		end if;

		Y := Y * (R + 2.0);
		-- 0.312 * (X + 2) approximation for sqrt (1/8) .. sqrt (8) argument range
		-- gives less than 0.065 % relative error

		loop
			R := Real (X / Y) / Y;
			-- Cannot replace double division by multiplication
			-- - large values cause overflow
			-- - small values lose precision
			exit when abs (R - Y) <= 3.0 * Real'Delta or N = 0;
			Y := Y + Real'(1.0 / 3.0) * (R - Y);
			N := N - 1;
		end loop;
		Y := Y + Real'(1.0 / 3.0) * (R - Y);
		return Y;
	end Cube_Root;
	-- Function implements Newton's iteration method, same as Sqrt.
	-- Here it converges slower.

end VSSL.Fixed_Point.Generic_Functions;
