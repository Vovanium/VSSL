package body VSSL.Integers.Generic_Figurate_Numbers is

	function Triangular (X : Value) return Value is
		(if X mod 2 = 0 then (X / 2) * (X + 1) else X * ((X + 1) / 2));
	-- Avoid overflow

	function Pentagonal (X : Value) return Value is
		(if X mod 2 = 0 then (X / 2) * (3 * X - 1) else X * ((3 * X - 1) / 2));
	-- Avoid overflow too

	function Hexagonal (X : Value) return Value is (2 * X * (X - 1));

	function Heptagonal (X : Value) return Value is
		(if X mod 2 = 0 then (X / 2) * (5 * X - 3) else X * ((5 * X - 3) / 2));

	function Octagonal (X : Value) return Value is (X * (3 * X - 2));

	function Nonagonal (X : Value) return Value is
		(if X mod 2 = 0 then (X / 2) * (7 * X - 5) else X * ((7 * X - 5) / 2));

	function Decagonal (X : Value) return Value is (X * (4 * X - 3));

	function Dodecagonal (X : Value) return Value is (X * (5 * X - 4));

	function Tetrahedral (X : Value) return Value is
		(if X mod 3 = 1 then Triangular (X) * ((X + 2) / 3) else (Triangular (X) / 3) * (X + 2));

end VSSL.Integers.Generic_Figurate_Numbers;
