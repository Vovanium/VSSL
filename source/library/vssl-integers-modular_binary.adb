package body VSSL.Integers.Modular_Binary is

	pragma SPARK_Mode (Off);
	function Floor_Log_2 (X : Value) return Natural is
		R  : Natural := 0;
		XT : Value'Base := X;
		K  : Natural := Value'Base'Size;
	begin
		loop
			XT := Shift_Right (XT, 1);
			exit when XT = 0;
			R := R + 1;
		end loop;
		pragma Assume (R < Value'Base'Size);
		return R;
	end Floor_Log_2;
	-- Note: This should be replaced with an intrinsic or an assembly code in the real application

end VSSL.Integers.Modular_Binary;
