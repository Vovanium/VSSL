package body VSSL.Integers.Generic_Digit_Arrays.Multiplication is

	function "*" (X, Y : Digit_Array) return Digit_Array is
		First_X : constant Index := First_Nonzero (X);
		First_Y : constant Index := First_Nonzero (Y);
		Last_X  : constant Index := Last_Nonzero (X);
		Last_Y  : constant Index := Last_Nonzero (Y);
		XP, P,
		Carry   : Digit_Product_Type;
	begin
		return R : Digit_Array := (First_X + First_Y .. Last_X + Last_Y + 1 => 0) do
			for J in First_X .. Last_X loop
				Carry := 0;
				XP := Digit_Product_Type (X (J));
				for K in First_Y .. Last_Y loop
					P := XP * Digit_Product_Type (Y (K)) + Digit_Product_Type (R (J + K)) + Carry;
					R (J + K) := Digit_Type (P mod Digit_Type'Modulus);
					Carry := P / Digit_Type'Modulus;
				end loop;
				R (J + Last_Y + 1) := Digit_Type (Carry);
			end loop;
		end return;
	end "*";

end VSSL.Integers.Generic_Digit_Arrays.Multiplication;