--
-- Arbitrary length Integers stored in decimal-friendly way
--
pragma SPARK_Mode;

with VSSL.Integers.Decimal_Chunks;
with VSSL.Integers.Generic_Digit_Arrays;

package VSSL.Integers.Big_Decimal is

	type Big_Decimal_Natural (<>) is private;

	function Make_With_Places (
		Places  : Positive;
		Initial : Natural := 0)
		return Big_Decimal_Natural;
	-- Allocate bit natural capable of storing Places-digit value
	-- and initializes it with Initial
	-- Raises an exception if allocated spaces is not capable of storing Initial value

	function To_Big (
		X      : Natural;
		Places : Positive := 1)
		return   Big_Decimal_Natural;
	-- Same as Make_With Places (Places, X),
	-- except it allocates more space if needed to store X

	function "=" (X, Y : Big_Decimal_Natural) return Boolean;
	function "<" (X, Y : Big_Decimal_Natural) return Boolean;
	function ">" (X, Y : Big_Decimal_Natural) return Boolean is (Y < X);
	function "<=" (X, Y : Big_Decimal_Natural) return Boolean is (not (X > Y));
	function ">=" (X, Y : Big_Decimal_Natural) return Boolean is (not (X < Y));

	procedure Set (
		X :    out Big_Decimal_Natural;
		Y : in     Big_Decimal_Natural);
	procedure Set (
		X :    out Big_Decimal_Natural;
		Y : in     Natural);

	function "+" (X, Y : Big_Decimal_Natural) return Big_Decimal_Natural;
	function "*" (X, Y : Big_Decimal_Natural) return Big_Decimal_Natural;

	function ILog10 (X : Big_Decimal_Natural) return Integer;
	function Digit (X : Big_Decimal_Natural; N : Natural) return Natural;
private
	type Decimal_Digit is mod 10 with Default_Value => 0;
	subtype Index is Natural;

	type Big_Decimal_Natural is array (Index range <>) of Decimal_Digit;

	package Digit_Arrays is new VSSL.Integers.Generic_Digit_Arrays (
		Digit_Type => Decimal_Digit,
		Index => Index,
		Digit_Array => Big_Decimal_Natural);

end VSSL.Integers.Big_Decimal;
