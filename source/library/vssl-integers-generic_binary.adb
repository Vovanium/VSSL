package body VSSL.Integers.Generic_Binary is

	-- This function is only used in SPARK
	--function Log_2_R (X : Value'Base) return Natural
	--is (if X < 2 then 0 else Log_2_R (X / 2) + 1)
	--with
	--	Pre => X > 0,
	--	Post => Value (Log_2_R'Result) < X and 2**Log_2_R'Result in X / 2 + 1 .. X;

	function Floor_Log_2 (X : Value) return Natural is
		R  : Natural := 0;
		XT : Value'Base := 1;
	begin
		while XT <= X / 2 loop
			pragma Loop_Invariant (XT = 2**R);
			pragma Loop_Invariant (Value (R) <= XT / 2);
			--pragma Loop_Invariant (R = Log_2_R (XT));
			--pragma Assert (XT < Value'Last);
			--pragma Assert (R < Value'Size);
			XT := XT * 2;
			R := R + 1;
		end loop;
		return R;
	end Floor_Log_2;
	-- Note: This should be replaced with an intrinsic or an assembly code in the real application

end VSSL.Integers.Generic_Binary;
