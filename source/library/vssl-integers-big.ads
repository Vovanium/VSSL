--
-- Implementation of fixed size integers beyond standard `range` types
-- Contrary to Ada 202x Big Number implementation they don't use memory allocation
--
with Interfaces;
use  Interfaces;
with VSSL.Extensible_Arithmetic;
use  VSSL.Extensible_Arithmetic;

package VSSL.Integers.Big with Pure is

	type Big_Integer (<>) is private;

	Zero      : constant Big_Integer;
	One       : constant Big_Integer;
	Two       : constant Big_Integer;
	Ten       : constant Big_Integer;
	Minus_One : constant Big_Integer;

	--
	-- Making and converting big numbers
	--

	function With_Bits (N : Positive) return Big_Integer;
	-- Allocate an object carrying at least `N` binary digits (bits) excluding sign,
	-- Thus have a range of -(2**N - 1) .. (2**N - 1)

	function With_Digits (N : Positive) return Big_Integer;
	-- Allocate an object carrying at least `N` decimal digits,
	-- thus have a range of -(10**N - 1) .. (10**N - 1)

	-- Two functions above are useful in variable initialization

	function From_String (S : String) return Big_Integer;
	-- Convert string representing integer in Ada format to big integer

	generic
		type Value is range <>;
	package Signed_Conversions is
		function To_Big (X : Value) return Big_Integer;
	end Signed_Conversions;

	--
	-- Checks and comparisons
	--

	function Is_Fit (X, Where : Big_Integer) return Boolean;
	-- Test whether a number fits given storage object

	function Is_Zero (X : Big_Integer) return Boolean with Inline;
	-- Test if number is zero

	function Is_Negative (X : Big_Integer) return Boolean with Inline;
	-- Test is number is less than zero

	function Is_Positive (X : Big_Integer) return Boolean
	is (not (Is_Negative (X) or else Is_Zero (X)));

	function "=" (X, Y : Big_Integer) return Boolean;
	-- Test if two numbers are equal

	--
	-- Assignments
	--

	procedure Set (T : out Big_Integer; Y : in Big_Integer);
	-- Copy a number to a variable with possibly different constraints

	--
	-- Athimetic that puts result to already allocated storage
	--

	procedure Negate (T : in out Big_Integer);
	-- Inplace version of negation

	procedure Add (T : in out Big_Integer; X : in Big_Integer);
	-- Inplace version of addition

	procedure Subtract (T : in out Big_Integer; X : in Big_Integer);
	-- Inplace version of subtraction

	--
	-- Conventional arithmetic
	--
	function "+" (X : Big_Integer) return Big_Integer;
	-- Does nothing, just returns its operand (but optimizes its storage)

	function "-" (X : Big_Integer) return Big_Integer;
	-- Negates the argument

	function "+" (X, Y : Big_Integer) return Big_Integer;
	-- Sum of operands

	function "-" (X, Y : Big_Integer) return Big_Integer;
	-- Difference between operands

	function "*" (A, B : Big_Integer) return Big_Integer;
	-- Product of operands
private

	-- Scalar implementation of big integers
	-- Implementation details:
	-- Value is stored as series of 32 bit 'limbs'
	-- highest order limb stores negative bit.
	-- Limb index corresponds to its factor: (2**32)*Index

	type Big_Integer (Last : Limb_Index) is record
		Data : Limb_Array (0 .. Last) := (others => 0);
	end record;

	-- This type represent integer split into `Limb_Size` bit wide parts
	-- with last element is in two's complement form so that its msb indicates sign

	Zero      : constant Big_Integer := (Last => 0, Data => (0 => 0));
	One       : constant Big_Integer := (Last => 0, Data => (0 => 1));
	Two       : constant Big_Integer := (Last => 0, Data => (0 => 2));
	Ten       : constant Big_Integer := (Last => 0, Data => (0 => 10));
	Minus_One : constant Big_Integer := (Last => 0, Data => (0 => Minus_One_Limb));

end VSSL.Integers.Big;
