--
-- This package implements a technique of exteding floating point precision
-- by storing a value as a sum of four fp numbers, first keeping its coarse value
-- and every next adds finer part
-- Look http://web.mit.edu/tabbott/Public/quaddouble-debian/qd-2.3.4-old/docs/qd.pdf
-- Yozo Hida, X. Li, and D. H. Bailey, Quad-Double Arithmetic: Algorithms, Implementation, and Application
--

generic
	type Real_Type is digits <>;
package VSSL.Floating_Point.Generic_Quad_Reals is

	type Quad_Real is private;

	function To_Quad_Real (A : Real_Type) return Quad_Real;

	function To_Real (A : Quad_Real) return Real_Type;

	function To_Quad_Real (A : Integer) return Quad_Real;

	function Value (S : String) return Quad_Real;

	function "+" (A : Quad_Real) return Quad_Real is (A);

	function "-" (A : Quad_Real) return Quad_Real;

	function "+" (A : Quad_Real; B : Real_Type) return Quad_Real;

	function "+" (A : Real_Type; B : Quad_Real) return Quad_Real is (B + A);

	function "+" (A, B : Quad_Real) return Quad_Real;

	function "-" (A : Quad_Real; B : Real_Type) return Quad_Real is (A + (-B));

	function "-" (A : Real_Type; B : Quad_Real) return Quad_Real is (A + (-B));

	function "-" (A, B : Quad_Real) return Quad_Real is (A + (-B));

	function "*" (A : Quad_Real; B : Real_Type) return Quad_Real;

	function "*" (A : Real_Type; B : Quad_Real) return Quad_Real is (B * A);

	function "*" (A, B : Quad_Real) return Quad_Real;

	function "/" (A : Quad_Real; B : Real_Type) return Quad_Real;

	function "/" (A, B : Quad_Real) return Quad_Real;

	function "/" (A : Real_Type; B : Quad_Real) return Quad_Real is (To_Quad_Real (A) / B);

	function "**" (A : Quad_Real; B : Integer'Base) return Quad_Real;

	function "=" (A, B : Quad_Real) return Boolean;

	function "<" (A, B : Quad_Real) return Boolean;

	function ">" (A, B : Quad_Real) return Boolean is (B < A);

	function "<=" (A, B : Quad_Real) return Boolean;

	function ">=" (A, B : Quad_Real) return Boolean is (B <= A);

	function "abs" (A : Quad_Real) return Quad_Real;

	function Floor (A : Quad_Real) return Quad_Real;

	function Ceiling (A : Quad_Real) return Quad_Real;

private
	type Real_Array is array (Natural range <>) of Real_Type;

	type Quad_Real is new Real_Array (0 .. 3);

	function To_Quad_Real (A : Real_Type) return Quad_Real is (1 => A, 2 .. 4 => 0.0);

	function To_Real (A : Quad_Real) return Real_Type is (A (3) + A (2) + A (1) + A (0));

end VSSL.Floating_Point.Generic_Quad_Reals;