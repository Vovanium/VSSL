package body VSSL.Combinatorial.Permutations is

	procedure Generic_Swap (X, Y : in out Item) is
		T : Item := X;
	begin
		X := Y;
		Y := T;
	end Generic_Swap;

	procedure Generic_Invert (X : in out Element_Array) is
		I : Index := X'First;
		J : Index := X'Last;
		procedure Swap is new Generic_Swap (Element);
	begin
		while I < J loop
			Swap (X (I), X (J));
			I := Index'Succ (I);
			J := Index'Pred (J);
		end loop;
	end Generic_Invert;

	procedure Generic_Bit_Reversal (X : in out Element_Array) is
		F  : constant Index := X'First; -- Offset
		H  : constant Index := X'Length / 2;
		I  : Index := 0;
		J  : Index := 0;
		B  : Index;
		T  : Element;
	begin
		if X'Length <= 2 then
			return;
		end if;
		while I < X'Length / 2 loop
			if I < J then
				-- Swap 0abc0/0cba0
				T := X (I + F);
				X (I + F) := X (J + F);
				X (J + F) := T;
				-- Swap 1abc1/1cba1
				T := X (I + H + 1 + F);
				X (I + H + 1 + F) := X (J + H + 1 + F);
				X (J + H + 1 + F) := T;
			end if;
			-- Swap 0abc1 and 1cba0
			T := X (I + 1 + F);
			X (I + 1 + F) := X (J + H + F);
			X (J + H + F) := T;

			-- Increment (by two)
			I := I + 2;
			-- Bit reversal increment (by two)
			B := X'Length / 4;
			while J >= B and B /= 0 loop
				J := J - B;
				B := B / 2;
			end loop;
			J := J + B;
		end loop;
	end Generic_Bit_Reversal;

end VSSL.Combinatorial.Permutations;
