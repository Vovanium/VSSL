package body VSSL.Extensible_Arithmetic is

	Product_Size : constant := 64;
	subtype Product is Unsigned_64;

	No_Sign_Mask : constant Limb := 2**(Limb_Size - 1) - 1;

	function Product_Sign_Extend (A : Limb) return Product
	is (Shift_Right (Shift_Left (Product (A), Product_Size - Limb_Size), Product_Size - Limb_Size));

	--

	function Compare_to_Zero (X : Limb_Array) return Compared is
	begin
		for J in reverse X'Range loop
			if X (J) /= Zero_Limb then
				return (if Is_Negative (X (J)) then Less else Greater);
			end if;
		end loop;
		return Equal;
	end Compare_to_Zero;

	function Compare_to_Minus_One (X : Limb_Array) return Compared is
	begin
		for J in reverse X'Range loop
			if X (J) /= Minus_One_Limb then
				return (if Is_Negative (X (J)) then Less else Greater);
			end if;
		end loop;
		return Equal;
	end Compare_to_Minus_One;

	function Compare_Unsigned_Common (X, Y : Limb_Array) return Compared is
	begin
		for J in reverse X'Range loop
			if X (J) /= Y (J) then
				return (if X (J) < Y (J) then Less else Greater);
			end if;
		end loop;
		return Equal;
	end Compare_Unsigned_Common;

	--
	function Carry_Gen (Y, A : Limb; C : Carry) return Carry is
	(if Y < A then Carry_1 elsif Y > A then Carry_0 else C);

	procedure Add (
		OV   :    out Boolean;
		CO   :    out Carry;
		Y    :    out Limb;
		A, B : in     Limb;
		CI   : in     Carry := Carry_0)
	is
		CS : Carry;
	begin
		Y  := A + B + CI;
		CO := Carry_Gen (Y, A, CI);
		CS := Carry_Gen ((Y and No_Sign_Mask), (A and No_Sign_Mask), CI);
		OV := (CO = Carry_1) xor (CS = Carry_1);
		-- Signed overflow detection:
		-- Overflow occured when carry into sign bit is not equal to carry out
	end Add;

	function Borrow_Gen (Y, A : Limb; C : Borrow) return Borrow is
	(if Y > A then Borrow_1 elsif Y < A then Borrow_0 else C);

	procedure Subtract (
		OV   :    out Boolean;
		BO   :    out Borrow;
		Y    :    out Limb;
		A, B : in     Limb;
		BI   : in     Borrow := Borrow_0)
	is
		CS : Borrow;
	begin
		Y  := A - B - BI;
		BO := Borrow_Gen (Y, A, BI);
		CS := Borrow_Gen ((Y and No_Sign_Mask), (A and No_Sign_Mask), BI);
		OV := (BO = Borrow_1) xor (CS = Borrow_1);
		-- Signed overflow detection:
		-- Overflow occured when borrow into sign bit is not equal to borrow out
	end Subtract;

	procedure Add_Common (
		CO   :    out Carry;
		Y    :    out Limb_Array;
		A, B : in     Limb_Array;
		CI   : in     Carry := Carry_0)
	is
		C : Carry := CI;
	begin
		for J in Y'Range loop
			Y (J) := A (J) + B (J) + C;
			C := (if Y (J) < A (J) then Carry_1 elsif Y (J) > A (J) then Carry_0 else C);
		end loop;
		CO := C;
	end Add_Common;

	procedure Add_Extend (
		CO :    out Carry;
		Y  :    out Limb_Array;
		A  : in     Limb_Array;
		CI : in     Carry)
	is
		C : Carry := CI;
	begin
		for J in Y'Range loop
			Y (J) := A (J) + C;
			C := (if Y (J) /= 0 then Carry_0 else C);
		end loop;
		CO := C;
	end Add_Extend;

	procedure Negate (
		BO :    out Borrow;
		Y  :    out Limb;
		A  : in     Limb;
		BI : in     Borrow := Borrow_0)
	is
	begin
		Y := BI - A;
		BO := (if A = 0 then BI else Borrow'Last);
	end Negate;

	--

	procedure Multiply_Add (
		CO,
		M  : in out Limb;
		A,
		B  : in     Limb;
		CI : in     Limb := 0)
	is
		P : Product;
	begin
		P := Product (A) * Product (B) + Product (M) + Product (CI);
		M := Limb (P and (2**Limb_Size - 1));
		CO := Limb (Shift_Right (P, Limb_Size));
	end Multiply_Add;

	procedure Multiply_Add_SU (
		CO,
		M  : in out Limb;
		A,
		B  : in     Limb;
		CI : in     Limb := 0)
	is
		P : Product;
	begin
		P := Product_Sign_Extend (A) * Product (B) + Product (M) + Product (CI);
		M := Limb (P and (2**Limb_Size - 1));
		CO := Limb (Shift_Right (P, Limb_Size));
	end Multiply_Add_SU;

	procedure Multiply_Add_Signed (
		CO,
		M  : in out Limb;
		A,
		B  : in     Limb;
		CI : in     Limb := 0)
	is
		P : Product;
	begin
		P := Product_Sign_Extend (A) * Product_Sign_Extend (B) + Product (M) + Product (CI);
		M := Limb (P and (2**Limb_Size - 1));
		CO := Limb (Shift_Right (P, Limb_Size));
	end Multiply_Add_Signed;

	procedure Multiply (
		CO,
		M  :    out Limb;
		A,
		B  : in     Limb;
		CI : in     Limb := 0)
	is
		P : Product;
	begin
		P := Product (A) * Product (B) + Product (CI);
		M := Limb (P and (2**Limb_Size - 1));
		CO := Limb (Shift_Right (P, Limb_Size));
	end Multiply;

end VSSL.Extensible_Arithmetic;
