with VSSL.Reals.String_Conversions;
use  VSSL.Reals.String_Conversions;

function VSSL.Floating_Point.Generic_String_to_Float (
	S    : String)
	return Number_Type
is

	subtype Exponent_Type is Integer; -- Believe no FP support values out of this range

	-- Convert simple decimal number
	function Decimal (S : String) return Exponent_Type
	renames Decimal_to_Integer;

	-- Convert mantissa
	procedure Mantissa (
		S : in     String;
		R : in     Radix_Type;
		N :    out Number_Type;
		E :    out Exponent_Type)
	is
		Rdx : Number_Type := Number_Type (R);
		Frac : Boolean := False;
		D : Integer;
	begin
		E := 0;
		N := 0.0;
		for J in S'Range loop
			case S (J) is
			when Ignored_Character =>
				null;
			when Digit_Character =>
				D := To_Digit_Value (S (J));
				if D >= R then
					raise Constraint_Error with "Invalid digit " &
						Character'Image (S (J));
				end if;
				N := N * Rdx + Number_Type (D);
				E := E - 1;
			when '.' | ',' =>
				Frac := True;
				E := 0;
			when others =>
				raise Constraint_Error with "Invalid character " &
					Character'Image (S (J)) & " in number";
			end case;
		end loop;
		if not Frac then
			E := 0;
		end if;
	end Mantissa;


	Radix   : Radix_Type;
	Minus   : Boolean;
	M_First : Positive;
	M_Last  : Natural;
	E_Base  : Radix_Type;
	E_Minus : Boolean;
	E_First : Positive;
	E_Last  : Natural;

	EE     : Exponent_Type := 0;
	EI     : Natural;
	EF     : Natural;
	EAdj   : Exponent_Type;
	Sep    : Positive;
	N      : Number_Type := 0.0;
	NF     : Number_Type := 0.0; -- fractonal part
	P      : Natural;
	D      : Integer;

	Rdx : Number_Type;
begin
	Parse_Number_Format (S, Radix, Minus, M_First, M_Last, E_Base, E_Minus, E_First, E_Last);

	if E_First <= E_Last then -- Have exponent
		EE := Decimal (S (E_First .. E_Last));
		if EE < 0 then
			raise Constraint_Error with "Exponent value is out of range";
		end if;
		if E_Minus then
			EE := -EE;
		end if;
	end if;

	Split_Mantissa (S (M_First .. M_Last), Sep, EI, EF);

	-- adjustment of exponent and frctional separator
	if Radix = E_Base then
		EAdj := Exponent_Type'Max (Exponent_Type'Min (EE, EF), -EI);
		EE := EE - EAdj;
		EI := EI + EAdj;
		EF := EF - EAdj;
	end if;

	Rdx := Number_Type (Radix);
	P := M_First;
	-- Convert integer part
	while EI > 0 loop
		if S (P) in Digit_Character then
			D := To_Digit_Value (S (P));
			if D >= Radix then
				raise Constraint_Error with "Invalid digit " &
					Character'Image (S (P));
			end if;
			N := N * Rdx + Number_Type (D);
			EI := EI - 1;
		end if;
		P := P + 1;
	end loop;

	P := M_Last;
	while EF > 0 loop
		if S (P) in Digit_Character then
			D := To_Digit_Value (S (P));
			if D >= Radix then
				raise Constraint_Error with "Invalid digit " &
					Character'Image (S (P));
			end if;
			NF := (NF + Number_Type (D)) / Rdx;
			EF := EF - 1;
		end if;
		P := P - 1;
	end loop;

	N := N + NF;

	N := N * Number_Type (E_Base)**EE;

	if Minus then
		N := -N;
	end if;

	return N;

end VSSL.Floating_Point.Generic_String_to_Float;