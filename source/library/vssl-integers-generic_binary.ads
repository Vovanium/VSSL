--
-- Functions dealing with powers of two
--
pragma SPARK_Mode;

generic
	type Value is range <>;
package VSSL.Integers.Generic_Binary is

	function Floor_Log_2 (X : Value) return Natural with
		Pre  => X > 0,
		Post => 2**Floor_Log_2'Result in X / 2 + 1 .. X;
	-- Integer part of logarithm base 2,
	-- which is actually a highest nonzero digit in binary representation

end VSSL.Integers.Generic_Binary;
