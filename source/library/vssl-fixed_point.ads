pragma SPARK_Mode;

package VSSL.Fixed_Point with Pure is

	type Fixed_16_16 is delta 2.0**(-16) range -2.0**15 + 2.0**(-16) .. 2.0**15 - 2.0**(-16);

end VSSL.Fixed_Point;
