--
-- Counting bits in big numbers
--

package VSSL.Big.Bit_Count is

	subtype Radix_Type is Integer range 2 .. 36;

	function Bits_in_Number (
		Digit_Count : Natural;
		Radix       : Radix_Type := 10)
		return        Long_Integer;
	-- Bits required to store a number with given number of digits in the integer part
	-- or, if Digit_Count is negative, number of leading zeros in fractional part

private

	type Ext_Radix_Type is range 1 .. Radix_Type'Last;

	function Radix_to_Mibibits (A : Ext_Radix_Type) return Integer;

end VSSL.Big.Bit_Count;
