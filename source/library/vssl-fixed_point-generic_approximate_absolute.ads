--
-- Approximate comparison, values with difference not more than Absolute_Error considered equal
--
pragma SPARK_Mode;

generic
	type Value is delta <>;
	Absolute_Error : Value;
package VSSL.Fixed_Point.Generic_Approximate_Absolute is

	function "=" (A, B : Value) return Boolean is (A in B - Absolute_Error .. B + Absolute_Error);

	function "<" (A, B : Value) return Boolean is (A < B - Absolute_Error);

	function "<=" (A, B : Value) return Boolean is (A <= B + Absolute_Error);

	function ">" (A, B : Value) return Boolean is (A > B + Absolute_Error);

	function ">=" (A, B : Value) return Boolean is (A >= B - Absolute_Error);

end VSSL.Fixed_Point.Generic_Approximate_Absolute;