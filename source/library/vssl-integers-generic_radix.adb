package body VSSL.Integers.Generic_Radix is

	function Order (X : Number; Radix : Number := Default_Radix) return Natural is
		T  : Number := X;
		M  : Natural := 0;
	begin
		while not (T < Radix) loop
			T := T / Radix;
			M := M + 1;
		end loop;
		return M;
	end Order;

	function Is_Palindrome (
		X     : Number;
		Radix : Number := Default_Radix)
		return Boolean
	is
		M  : Natural := Order (X, Radix);
		T  : Number  := X;
		P  : Number  := Radix**M; -- Higher digit position in T
	begin
		for I in 0 .. M / 2 loop
			if not (T / P = T mod Radix) then
				return False;
			end if;
			T := (T mod P) / Radix; -- cut down digits one by one
			P := P / Radix**2;
		end loop;
		return True;
	end Is_Palindrome;

end VSSL.Integers.Generic_Radix;
