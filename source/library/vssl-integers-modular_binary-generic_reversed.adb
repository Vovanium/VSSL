package body VSSL.Integers.Modular_Binary.Generic_Reversed is

	function Bit_Reverse (X : Value) return Value is
		T : Value := X;
		R : Value := 0;
	begin
		for I in 1 .. Default_Bits loop
			R := Shift_Left (R, 1) or (T and 1);
			T := Shift_Right (T, 1);
		end loop;
		return R;
	end Bit_Reverse;

	function Increment (X : Value; One : Value := Default_One) return Value is
		Bit : Value := One;
		R   : Value := X;
	begin
		while R >= Bit and Bit /= 0 loop
			pragma Loop_Invariant (R < Bit * 2);
			R := R - Bit; -- clear set bit
			pragma Assert (R < Bit);
			pragma Assume (Bit / 2 * 2 = Bit or Bit = 1); -- because bit is power of 2
			Bit := Shift_Right (Bit, 1);
		end loop;
		pragma Assert (R < Bit or (Bit = 0 and R = 0));
		pragma Assert (R <= Value'Base'Last / 2);
		pragma Assert (R + Bit <= Value'Base'Last);
		R := R + Bit; -- set first cleared bit
		return R;
	end Increment;

end VSSL.Integers.Modular_Binary.Generic_Reversed;
