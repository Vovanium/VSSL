with VSSL.Reals.String_Conversions;
use  VSSL.Reals.String_Conversions;

function VSSL.Reals.Generic_String_to_Number (
	S    : String)
	return Number_Type
is
	-- Convert simple decimal number
	function Decimal is
	new Generic_Decimal_to_Integer (Exponent_Type);

	-- Convert mantissa
	procedure Mantissa (
		S : in     String;
		R : in     Radix_Type;
		N :    out Number_Type;
		E :    out Exponent_Type)
	is
		Rdx : Number_Type := To_Number (R);
		Frac : Boolean := False;
		D : Integer;
	begin
		E := 0;
		N := To_Number (0);
		for J in S'Range loop
			case S (J) is
			when Ignored_Character =>
				null;
			when Digit_Character =>
				D := To_Digit_Value (S (J));
				if D >= R then
					raise Constraint_Error with "Invalid digit " &
						Character'Image (S (J));
				end if;
				N := N * Rdx + To_Number (D);
				E := E - 1;
			when '.' | ',' =>
				Frac := True;
				E := 0;
			when others =>
				raise Constraint_Error with "Invalid character " &
					Character'Image (S (J)) & " in number";
			end case;
		end loop;
		if not Frac then
			E := 0;
		end if;
	end Mantissa;


	Radix   : Radix_Type;
	Minus   : Boolean;
	M_First : Positive;
	M_Last  : Natural;
	E_Base  : Radix_Type;
	E_Minus : Boolean;
	E_First : Positive;
	E_Last  : Natural;

	EP,
	EE     : Exponent_Type := 0;
	N      : Number_Type := To_Number (0);

begin
	Parse_Number_Format (S, Radix, Minus, M_First, M_Last, E_Base, E_Minus, E_First, E_Last);

	Mantissa (S (M_First .. M_Last), Radix, N, EP);

	if E_First <= E_Last then -- Have exponent
		EE := Decimal (S (E_First .. E_Last));
		if EE < 0 then
			raise Constraint_Error with "Exponent value is out of range";
		end if;
		if E_Minus then
			EE := -EE;
		end if;
	end if;

	if E_Base /= Radix then
		N := Multiply_by_Power (N, To_Number (Radix), EP);
		N := Multiply_by_Power (N, To_Number (E_Base), EE);
	else
		N := Multiply_by_Power (N, To_Number (Radix), EP + EE);
	end if;

	if Minus then
		N := -N;
	end if;

	return N;

end VSSL.Reals.Generic_String_to_Number;