generic
package VSSL.Integers.Generic_Digit_Arrays.Addition with Pure is

	function "+" (X, Y : Digit_Array) return Digit_Array;

end VSSL.Integers.Generic_Digit_Arrays.Addition;
