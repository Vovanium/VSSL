pragma SPARK_Mode;

with Interfaces;
use  Interfaces;

with VSSL.Integers.Modular_Binary.Generic_Reversed;
package VSSL.Integers.Binary_16.Reversed is new VSSL.Integers.Binary_16.Generic_Reversed (
	Default_Bits => 16);