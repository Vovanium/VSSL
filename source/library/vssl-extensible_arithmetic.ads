--
-- Basic arbitrary precision arithmeric building blocks.
-- 32 bit (with use of 64 bit intemediate type) portable implementation
-- This code could be replaced by instrinsics or inline assembly
--

with Interfaces;
use  Interfaces;

package VSSL.Extensible_Arithmetic with Pure is

	Limb_Size : constant := 32; -- Bits in a limb

	subtype Limb is Unsigned_32; -- An arbitrary type holding part of a number

	Zero_Limb      : constant Limb := 0;
	One_Limb       : constant Limb := 1;
	Minus_One_Limb : constant Limb := Limb'Last;

	subtype Carry is Limb;         -- An arbitrary type holding addtion carry value
	Carry_0 : constant Carry := 0;
	Carry_1 : constant Carry := 1;

	subtype Borrow is Limb;        -- An arbitrary type holding subtraction borrow value
	Borrow_0 : constant Borrow := 0;
	Borrow_1 : constant Borrow := 1;

	subtype Limb_Index is Integer'Base; -- Index in limb array

	type Limb_Array is array (Limb_Index range <>) of Limb; -- A type holding sequence of limbs

	-- Comparison

	type Compared is (Less, Equal, Greater); -- Ternary comparison result

	function Is_Negative (X : Limb) return Boolean
	is ((X and 2**(Limb_Size - 1)) /= 0);
	-- Test if a limb in signed representation is holding negative value

	function Compare_to_Zero (X : Limb_Array) return Compared;
	-- Compare number to zero

	function Compare_to_Minus_One (X : Limb_Array) return Compared;
	-- Compare number to minus one

	function Compare_Unsigned_Common (X, Y : Limb_Array) return Compared
	with
		Pre => X'First = Y'First and X'Last = Y'Last;

	--
	-- Spectial Arithmetic Tests
	--

	function Negation_Overflows (X : Limb) return Boolean
	is (X = 2**(Limb_Size - 1));

	function Addition_Overflows (A, B : Limb) return Boolean
	is (Shift_Right_Arithmetic (A, 1) + Shift_Right_Arithmetic (B, 1) in
		2**(Limb_Size - 2) - 1 .. 3 * 2**(Limb_Size - 2) - 1);
	-- Sum of two give numbers will overflow (with or without carry)
	-- FFFFFFFF
	-- C0000000 -- safe addition of negatives
	--
	-- 3FFFFFFE - safe addition of positives
	-- 00000000 -

	function Subtraction_Overflows (A, B : Limb) return Boolean
	is (Shift_Right_Arithmetic (A, 1) - Shift_Right_Arithmetic (B, 1) in
		2**(Limb_Size - 2) .. 3 * 2**(Limb_Size - 2));
	-- FFFFFFFF
	-- C0000001 -- safe subtraction of negative - positive
	--
	-- 3FFFFFFF - safe subtraction of positive - negative
	-- 00000000 -

	--
	-- Addition / Subraction
	--

	procedure Add (
		OV   :    out Boolean;           -- Overflow
		CO   :    out Carry;             -- Carry out
		Y    :    out Limb;              -- Sum
		A, B : in     Limb;              -- Arguments
		CI   : in     Carry := Carry_0); -- Carry in

	procedure Subtract (
		OV   :    out Boolean;             -- Overflow
		BO   :    out Borrow;              -- Borrow out
		Y    :    out Limb;                -- Sum
		A, B : in     Limb;                -- Arguments
		BI   : in     Borrow := Borrow_0); -- Carry in

	procedure Add_Common (
		CO   :    out Carry;            -- Carry out
		Y    :    out Limb_Array;       -- Sum
		A, B : in     Limb_Array;       -- Arguments
		CI   : in     Carry := Carry_0) -- Carry in
	with
		Pre => Y'First = A'First and Y'Last = A'Last and A'First = B'First and A'Last = B'Last;

	procedure Add_Extend (
		CO :    out Carry;
		Y  :    out Limb_Array;
		A  : in     Limb_Array;
		CI : in     Carry)
	with
		Pre => Y'First = A'First and Y'Last = A'Last;

	procedure Negate (
		BO :    out Borrow;              -- Borrow out
		Y  :    out Limb;                -- Result
		A  : in     Limb;                -- Argument
		BI : in     Borrow := Borrow_0)  -- Borrow in
	with Inline;

	-- Multiplication

	procedure Multiply_Add (
		CO,                     -- Product carry out (higher part)
		M  : in out Limb;       -- Product (lower part)
		A,
		B  : in     Limb;       -- Multiplicands
		CI : in     Limb := 0)  -- Carry In (added to product)
	with Inline;

	procedure Multiply (
		CO,                     -- Product carry out (higher part)
		M  :    out Limb;       -- Product (lower part)
		A,
		B  : in     Limb;       -- Multiplicands
		CI : in     Limb := 0)  -- Carry In (added to product)
	with Inline;

end VSSL.Extensible_Arithmetic;
