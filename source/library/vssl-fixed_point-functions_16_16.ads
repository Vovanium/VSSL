--
-- Elementary functions for Fixed_16_16 type
--
with VSSL.Fixed_Point.Generic_Functions;
with Interfaces;
use  Interfaces;

package VSSL.Fixed_Point.Functions_16_16
is new VSSL.Fixed_Point.Generic_Functions (
	Real           => Fixed_16_16,
	Representation => Interfaces.Unsigned_32,
	Fraction_Bits  => 16);
