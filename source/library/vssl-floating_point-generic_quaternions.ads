--
-- Quaternion type and its operations
--
generic
	type Real is digits <>;
package VSSL.Floating_Point.Generic_Quaternions with Pure is

	type Quaternion is record
		W, X, Y, Z : Real'Base;
	end record;

	function To_Quaternion (N : Real) return Quaternion is (N, 0.0, 0.0, 0.0);

	function "+" (Q : Quaternion) return Quaternion is (Q);
	function "-" (Q : Quaternion) return Quaternion is (-Q.W, -Q.X, -Q.Y, -Q.Z);
	function Conjugate (Q : Quaternion) return Quaternion is (Q.W, -Q.X, -Q.Y, -Q.Z);

	function "+" (P : Quaternion; N : Real) return Quaternion is (P.W + N, P.X, P.Y, P.Z);
	function "+" (N : Real; Q : Quaternion) return Quaternion is (N + Q.W, Q.X, Q.Y, Q.Z);
	function "+" (P, Q : Quaternion) return Quaternion is (P.W + Q.W, P.X + Q.X, P.Y + Q.Y, P.Z + Q.Z);

	function "-" (P : Quaternion; N : Real) return Quaternion is (P.W - N,  P.X,  P.Y,  P.Z);
	function "-" (N : Real; Q : Quaternion) return Quaternion is (N - Q.W, -Q.X, -Q.Y, -Q.Z);
	function "-" (P, Q : Quaternion) return Quaternion is (P.W - Q.W, P.X - Q.X, P.Y - Q.Y, P.Z - Q.Z);

	function "*" (P : Quaternion; K : Real) return Quaternion is (P.W * K, P.X * K, P.Y * K, P.Z * K);
	function "*" (N : Real'Base; Q : Quaternion) return Quaternion is (N * Q.W, N * Q.X, N * Q.Y, N * Q.Z);
	function "*" (P, Q : Quaternion) return Quaternion is (
		P.W * Q.W - P.X * Q.X - P.Y * Q.Y - P.Z * Q.Z,
		P.W * Q.X + P.X * Q.W + P.Y * Q.Z - P.Z * Q.Y,
		P.W * Q.Y - P.X * Q.Z + P.Y * Q.W + P.Z * Q.X,
		P.W * Q.Z + P.X * Q.Y - P.Y * Q.X + P.Z * Q.W);

	function "/" (P : Quaternion; N : Real) return Quaternion is (P.W / N, P.X / N, P.Y / N, P.Z / N);
	function "/" (N : Real; Q : Quaternion) return Quaternion is (
		N / (Q.W * Q.W + Q.X * Q.X + Q.Y * Q.Y + Q.Z * Q.Z) * Conjugate (Q));
	function "/" (P, Q : Quaternion) return Quaternion is (
		P * Conjugate (Q) / (Q.W * Q.W + Q.X * Q.X + Q.Y * Q.Y + Q.Z * Q.Z));

	i : constant Quaternion := (0.0, 1.0, 0.0, 0.0);
	j : constant Quaternion := (0.0, 0.0, 1.0, 0.0);
	k : constant Quaternion := (0.0, 0.0, 0.0, 1.0);

end VSSL.Floating_Point.Generic_Quaternions;
