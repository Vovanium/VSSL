package body VSSL.Integers.Primes.Generic_Sieve is

	Wheel          : constant := 210; -- 2*3*5*7 wheel
	Wheel_Active   : constant := 48;
	N_Wheels       : constant Positive := (Limit + 1) / Wheel;
	Small_Primes   : array (1 .. Wheel - 1) of Boolean;
	Wheel_Map      : array (0 .. Wheel - 1) of Boolean;
	Wheel_Index    : array (0 .. Wheel - 1) of Natural;
	Wheel_Offset   : array (1 .. Wheel_Active) of Natural;

	type Prime_Matrix is array (1 .. N_Wheels, 1 .. Wheel_Active) of Boolean;
	pragma Pack (Prime_Matrix);
	Primes         : Prime_Matrix;

	function Is_Prime (N : Positive) return Boolean is
		Row, Column : Natural;
	begin
		if N > Limit then
			raise Constraint_Error;
		elsif N >= Wheel then
			Row := N / Wheel;
			Column := N mod Wheel;
			return Wheel_Map (Column) and then Primes (Row, Wheel_Index (Column));
		else
			return Small_Primes (N);
		end if;
	end Is_Prime;

	function First (Object : Prime_Iterator) return Positive is (2);

	function Next (Object : Prime_Iterator; Position : Positive) return Positive is
		P    : Positive;
		R, C : Natural;
	begin
		if Position < Wheel then
			P := Position;
			loop
				P := P + 1;
				exit when P >= Wheel;
				if Small_Primes (P) then
					return P;
				end if;
			end loop;
			R := 1;
			C := 0;
		else
			R := Position / Wheel;
			C := Wheel_Index (Position mod Wheel);
		end if;
		loop
			C := C + 1;
			if C > Wheel_Offset'Last then
				R := R + 1;
				if R > N_Wheels then
					return 1;
				end if;
				C := 1;
			end if;
			exit when Primes (R, C);
		end loop;
		P := R * Wheel + Wheel_Offset (C);
		return (if P <= Limit then P else 1);
	end Next;

	function Iterate return Prime_Iterator is (Prime_Iterator'(others => <>));

	procedure Initialize_Small is
	begin
		Small_Primes (1) := False;
		for I in 2 .. Wheel - 1 loop
			Small_Primes (I) := True;
		end loop;
		for I in 2 .. Wheel - 1 loop
			exit when I * I >= Wheel;
			if Small_Primes (I) then
				for J in I .. (Wheel - 1) / I loop
					Small_Primes (I * J) := False;
				end loop;
			end if;
		end loop;
	end Initialize_Small;

	procedure Initialize_Wheel is
		K : Natural;
	begin
		Wheel_Map (0) := False;
		for I in 1 .. Wheel_Map'Last loop
			Wheel_Map (I) := True;
		end loop;

		for I in 2 .. 7 loop
			if Small_Primes (I) then
				for J in 1 .. (Wheel - 1) / I loop
					Wheel_Map (I * J) := False;
				end loop;
			end if;
		end loop;

		K := 0;
		for I in Wheel_Index'Range loop
			if Wheel_Map (I) then
				K := K + 1;
				Wheel_Offset (K) := I;
			end if;
			Wheel_Index (I) := K;
		end loop;
	end Initialize_Wheel;

	function Max (A, B : Integer) return Integer is (if A < B then B else A);

	procedure Sift (N : Positive) is
		M    : Positive;
		R, C : Natural;
	begin
		for K in Max (Wheel / N + 1, N) .. Limit / N loop
			M := K * N;
			R := M / Wheel;
			C := M mod Wheel;
			if Wheel_Map (C) then
				Primes (R, Wheel_Index (C)) := False;
			end if;
		end loop;
	end Sift;

	procedure Initialize_Matrix is
		V : Positive;
	begin
		for I in Primes'Range (1) loop
			for J in Primes'Range (2) loop
				Primes (I, J) := True;
			end loop;
		end loop;

		for I in 11 .. Wheel - 1 loop
			if Small_Primes (I) then
				Sift (I);
			end if;
		end loop;

		for I in Primes'Range (1) loop
			exit when (I * Wheel)**2 > Limit;
			for J in Primes'Range (2) loop
				if Primes (I, J) then
					V := I * Wheel + Wheel_Offset (J);
					Sift (V);
				end if;
			end loop;
		end loop;
	end Initialize_Matrix;

begin
	Initialize_Small;
	Initialize_Wheel;
	Initialize_Matrix;

end VSSL.Integers.Primes.Generic_Sieve;
