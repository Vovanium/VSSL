package body VSSL.Reals.String_Conversions is

	function To_Upper (C : Character) return Character is
	(if C in 'a' .. 'z' then
		Character'Val (Character'Pos (C) - (Character'Pos ('a') - Character'Pos ('A')))
	else
		C);

	function Equal_No_Case (A, B : String) return Boolean is
	(A'Length = B'Length and then (for all J in A'Range =>
		To_Upper (A (J)) = To_Upper (B (J - A'First + B'First))));

	function Have_Affix (S, Prefix, Postfix : String) return Boolean is
	(S'Length > Prefix'Length + Postfix'Length and then
		Equal_No_Case (S (S'First .. S'First + Prefix'Length - 1), Prefix) and then
		Equal_No_Case (S (S'Last - Postfix'Length + 1 .. S'Last), Postfix));

	procedure Remove_Affix (F, L : in out Natural; Prefix, Postfix : in String) is
	begin
		F := F + Prefix'Length;
		L := L - Postfix'Length;
	end Remove_Affix;

	procedure Skip_Spaces (S : in String; J : in out Natural; L : in Natural) is
	begin
		while J <= L and then S (J) in Ignored_Character loop
			J := J + 1;
		end loop;
		if J > L then
			raise Constraint_Error with "No number";
		end if;
	end Skip_Spaces;

	procedure Sign (S : in String; F : in out Natural; L : in Natural; Neg : out Boolean) is
	begin
		Neg := False;
		if S (F) = '+' then
			F := F + 1;
		elsif S (F) = '-' then
			F := F + 1;
			Neg := True;
		end if;
	end Sign;

	-- Convert simple decimal number
	function Generic_Decimal_to_Integer (S : String) return Integer_Type is
		N : Integer_Type := 0;
		D : Integer_Type;
	begin
		for C of S loop
			if C in '0' .. '9' then
				D := Character'Pos (C) - Character'Pos ('0');
				if N > (Integer_Type'Last - D) / 10 then
					return Integer_Type'First; -- carefully treat overflow
				end if; -- to have meaningful exception
				N := N * 10 + D;
			elsif C not in Ignored_Character then
				raise Constraint_Error with "Illegal character " & Character'Image (C);
			end if;
		end loop;
		return N;
	end Generic_Decimal_to_Integer;

	function Decimal_to_Integer_Instant is new Generic_Decimal_to_Integer (Integer);

	function Decimal_to_Integer (S : String) return Integer renames Decimal_to_Integer_Instant;

	procedure Parse_Number_Format (
		S       : in     String;
		Radix   :    out Radix_Type;
		Minus   :    out Boolean;
		M_First :    out Positive;
		M_Last  :    out Natural;
		E_Base  :    out Radix_Type;
		E_Minus :    out Boolean;
		E_First :    out Positive;
		E_Last  :    out Natural)
	is
		RF : Positive := 1;
		RL : Natural := 0;
		Based  : Boolean := False; -- Flag for number with explicit radix
	begin
		Radix := 10;
		Minus := False;
		M_First := S'First;
		M_Last := S'Last;

		E_Base  := 10;
		E_Minus := False;
		E_First := 1; -- default value for no exponent
		E_Last := 0;

		while M_Last >= M_First and then S (M_Last) in Ignored_Character loop -- skip
			M_Last := M_Last - 1;
		end loop;
		Skip_Spaces (S, M_First, M_Last);

		Sign (S, M_First, M_Last, Minus);
		Skip_Spaces (S, M_First, M_Last);

		if M_Last -  M_First >= 2                        -- 3 chars min
		and then S (M_First) = '0'
		and then S (M_First + 1) in 'X' | 'x' | 'B' | 'b'
		then                                             -- 0b or 0x...[p...] C-style format
			Radix := (case S (M_First + 1) is
				when 'B' | 'b' => 2,
				when 'X' | 'x' => 16,
				when others => raise Program_Error with "Radix marker");
			M_First := M_First + 2; -- cut out 0x prefix
			Bin_Exp_Find : for J in M_First + 1 .. M_Last - 1 loop
				if S (J) in 'P' | 'p' then
					E_Last := M_Last;
					E_First := J + 1;
					M_Last := J - 1;
					E_Base := 2;
					exit Bin_Exp_Find;
				end if;
			end loop Bin_Exp_Find;

		elsif M_Last - M_First >= 3
		and then S (M_First) in 'B' | 'b' | 'O' | 'o' | 'Z' | 'z'
		and then S (M_Last) in ''' | '"' -- "
		and then S (M_First + 1) = S (M_Last) -- opening and closing characters should match
		then                                  -- Fortran BOZ-format (B' ' / B" ")
			Radix := (case S (M_First) is
				when 'B' | 'b' =>  2,
				when 'O' | 'o' =>  8,
				when 'Z' | 'z' => 16,
				when others => raise Program_Error with "Radix marker");
			M_First := M_First + 2;
			M_Last := M_Last - 1;
		else

			Radix_Find : for J in M_First + 1 .. M_Last loop
				if S (J) in '#' | ':' then -- Ada-style based
					if not Based then -- first occurence of '#'
						RF := M_First;
						RL := J - 1;
						M_First := J + 1;
						Based := True;
					else -- second occurence of '#'
						if J < M_Last - 1 and then S (J + 1) in 'E' | 'e' then
							E_Last := M_Last;
							E_First := J + 2;
							M_Last := J - 1;
							exit Radix_Find;
						elsif J = M_Last then
							M_Last := J - 1;
						end if;
					end if;
				elsif not Based and S (J) in 'R' | 'r' then -- Algol-style
					RF := M_First;
					RL := J - 1;
					M_First := J + 1;
					Based := True;
					exit Radix_Find;
				end if;
			end loop Radix_Find;

			if Based then
				Radix := Decimal_to_Integer (S (RF .. RL));
				E_Base := Radix;
				-- raise constraint error when not in range
			elsif not Based then
				Dec_Exp_Find : for J in M_First + 1 .. M_Last - 1 loop
					if S (J) in 'D' | 'd' | 'E' | 'e' then
						E_Last := M_Last;
						E_First := J + 1;
						M_Last := J - 1;
						exit Dec_Exp_Find;
					end if;
				end loop Dec_Exp_Find;
				if not (E_First <= E_Last) then -- No exponent
					if Have_Affix (S (M_First .. M_Last), "", "B") then
						Radix := 2;
						Remove_Affix (M_First, M_Last, "", "B");
					elsif Have_Affix (S (M_First .. M_Last), "", "H") then
						Radix := 16;
						Remove_Affix (M_First, M_Last, "", "H");
					end if;
				end if;
			end if;
		end if;
		if E_First <= E_Last then
			Sign (S, E_First, E_Last, E_Minus);
		end if;
	end Parse_Number_Format;

	procedure Split_Mantissa (
		S           : in     String;
		Separator   :    out Positive;
		Int_Digits  :    out Natural;
		Frac_Digits :    out Natural)
	is
		J : Positive := S'First;
	begin
		Int_Digits := 0;
		Frac_Digits := 0;
		Separator := S'Last + 1;
		Int_Part : loop
			if J > S'Last then
				return;
			end if;
			case S (J) is
			when Digit_Character =>
				Int_Digits := Int_Digits + 1;
			when '.' | ',' =>
				exit Int_Part;
			when Ignored_Character =>
				null;
			when others =>
				raise Constraint_Error with "Invalid character " &
					Character'Image (S (J)) & " in number";
			end case;
			J := J + 1;
		end loop Int_Part;
		Separator := J;
		Frac_Part : loop
			J := J + 1;
			exit Frac_Part when J > S'Last;
			case S (J) is
			when Digit_Character =>
				Frac_Digits := Frac_Digits + 1;
			when '.' | ',' =>
				raise Constraint_Error with "Excessive fractional separator";
			when Ignored_Character =>
				null;
			when others =>
				raise Constraint_Error with "Invalid character " &
					Character'Image (S (J)) & " in number";
			end case;
		end loop Frac_Part;
	end Split_Mantissa;

end VSSL.Reals.String_Conversions;
