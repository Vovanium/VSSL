--
-- Mainly a collection of generic packages for arbitrary real types
--

package VSSL.Reals with Pure is

	subtype Radix_Type is Integer range 2 .. 36;
	-- Type for radix (number base). Used in conversion subroutines

end VSSL.Reals;
