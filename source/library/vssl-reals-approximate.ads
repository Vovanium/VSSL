--
-- Approximate equality comparisons for arbitrary real types
--
-- It is believed that many of them can be used on integer
-- types also, treating them as reals with delta of 1.0.
--

pragma SPARK_Mode;

package VSSL.Reals.Approximate is

	generic
		type Value (<>) is private;
		Absolute_Error : Value;
		with function "<=" (A, B : Value) return Boolean is <>;
		with function "+" (A, B : Value) return Value is <>;
		with function "-" (A, B : Value) return Value is <>;
	function Generic_Absolute_Equal (A, B : Value) return Boolean with Inline;
	-- Approximate comparison, values differ not more than Absolute_Error considered equal.

	generic
		type Value (<>) is private;
		Relative_Error : Value;
		with function "<=" (A, B : Value) return Boolean is <>;
		with function "+" (A, B : Value) return Value is <>;
		with function "-" (A, B : Value) return Value is <>;
		with function "*" (A, B : Value) return Value is <>;
		with function "abs" (A : Value) return Value is <>;
	function Generic_Relative_Equal (A, B : Value) return Boolean with Inline;
	-- Approximate comparison, values differ not more than Relative_Error
	-- (multiplied by right parameter) considered equal.
	-- Example: let Relative_Error be 0.01 (1 % comparison precision)
	-- then 1.007 = 1.0, 0.996 = 1.0, but 1.012 /= 1.000 (values from 0.99 to 1.01 approx. equal to 1.0)
	--      3.024 = 3.0, 2.989 = 3.0, but 2.964 /= 3.000 (values from 2.97 to 3.03 approx. equal to 3.0)

	generic
		type Value (<>) is private;
		Absolute_Error : Value;
		Relative_Error : Value;
		with function "<=" (A, B : Value) return Boolean is <>;
		with function "+" (A, B : Value) return Value is <>;
		with function "-" (A, B : Value) return Value is <>;
		with function "*" (A, B : Value) return Value is <>;
		with function "abs" (A : Value) return Value is <>;
	function Generic_Equal (A, B : Value) return Boolean with Inline;
	-- Approximate comparison, combines both relative and absolute errors.
	-- Values considered equal if their difference is less than either error
	-- (which is greater).
	-- See Generic_Absolute_Equal and Generic_Relative_Equal for reference.

end VSSL.Reals.Approximate;