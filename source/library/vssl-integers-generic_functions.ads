--
-- Elementary functions on integers;
--
pragma SPARK_Mode;

generic
	type Value is range <>;
package VSSL.Integers.Generic_Functions is

	function Floor_Log (X, Y : Value) return Integer
	with
		Pre => X >= 0 and Y >= 2,
		Post => (if X = 0 then Floor_Log'Result = Integer'First
			else Floor_Log'Result >= 0),
		Global => null;
	-- Integer part of logarithm

	function Floor_Log_10 (X : Value) return Integer
	with
		Pre => X >= 0,
		Post => (if X = 0 then Floor_Log_10'Result = Integer'First
			else Floor_Log_10'Result >= 0),
		Global => null;
	-- Integer part of logarithm base 10

	function GCD (X, Y : Value) return Value
	with
		Pre => X > 0 and Y > 0,
		Post => GCD'Result > 0,
		Global => null;
	-- Greatest Common Denominator

end VSSL.Integers.Generic_Functions;