--
-- Data permutation procedures
--
pragma SPARK_Mode;

package VSSL.Combinatorial.Permutations is

	generic
		type Item is private;
	procedure Generic_Swap (X, Y : in out Item)
	with Inline,
		Post => X = Y'Old and Y = X'Old;

	generic
		type Element       is private;
		type Index         is (<>);
		type Element_Array is array (Index range <>) of Element;
	procedure Generic_Invert (X : in out Element_Array);

	generic
		type Element       is private;
		type Index         is range <>;
		type Element_Array is array (Index range <>) of Element;
	procedure Generic_Bit_Reversal (X : in out Element_Array);
	-- Swaps elements those indices are reverse in binary notation
	-- array should be power of two in size

end VSSL.Combinatorial.Permutations;