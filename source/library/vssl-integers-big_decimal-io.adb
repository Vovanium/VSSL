package body VSSL.Integers.Big_Decimal.IO is

	procedure Put (
		File  : in File_Type;
		Item  : in Big_Decimal_Natural;
		Width : in Field := 0
	) is
		W       : Integer  := (if Width = 0 then Integer (Item'Last + 1) else Width);
		L       : Positive := Integer'Max (ILog10 (Item) + 1, 1);
		S       : String (1 .. Integer'Max (W, L)) := (others => ' ');
		Pos     : Natural;
	begin
		for I in reverse 0 .. L - 1 loop
			Pos := S'Last - I;
			S (Pos) := Character'Val (Character'Pos ('0') + Digit (Item, I));
		end loop;
		Put (File, S);
	end Put;

	procedure Put (
		Item  : in Big_Decimal_Natural;
		Width : in Field := 0
	) is
	begin
		Put (Standard_Output, Item, Width);
	end Put;

end VSSL.Integers.Big_Decimal.IO;