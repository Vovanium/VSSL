package body VSSL.Integers.Primes is

	function Least_Prime_Factor (X : Natural) return Natural is
		J : Natural := 5;
		D : Integer := X - J**2;
	begin
		if X mod 2 = 0 then -- this should be fast on binary machine
			return 2;
		end if;
		if X mod 3 = 0 then
			return 3;
		end if;

		while D > 0 loop
			if X mod J = 0 then -- 5, 11...
				return J;
			end if;
			if X mod (J + 2) = 0 then -- 7, 13...
				return J + 2;
			end if;
			D := D - (2 * 6 * J + 6**2); -- subtract (J+6)**2 - J**2
			J := J + 6;
		end loop;
		return X;
	end Least_Prime_Factor;
	-- Using wheel 2*3 optimization

	function Least_Prime_Factor_Opt (X : Natural) return Natural is
		J : Natural := 5;
		D : Integer := X - J**2;
	begin
		if X mod 2 = 0 then
			return 2;
		end if;

		if X mod 3 = 0 then
			return 3;
		end if;

		while J < 29 and then D > 0 loop
			if X mod J = 0 then -- 5, 11, 17, 23
				return J;
			end if;
			if X mod (J + 2) = 0 then -- 7, 13, 19, (25)
				return J + 2;
			end if;
			D := D - (2 * 6 * J + 6**2); -- subtract (J+6)**2 - J**2
			J := J + 6;
		end loop;

		while D > 0 loop
			if X mod J = 0 then -- 29, 59...
				return J;
			end if;

			if X mod (J + 2) = 0 then -- 31, 61...
				return J + 2;
			end if;

			if X mod (J + 8) = 0 then -- 37, 67...
				return J + 8;
			end if;

			if X mod (J + 12) = 0 then -- 41, 71...
				return J + 12;
			end if;

			if X mod (J + 12) = 0 then -- 41, 71...
				return J + 12;
			end if;

			if X mod (J + 14) = 0 then -- 43, 73...
				return J + 14;
			end if;

			if X mod (J + 18) = 0 then -- 47, (77)...
				return J + 18;
			end if;

			if X mod (J + 20) = 0 then -- (49), 79...
				return J + 20;
			end if;

			if X mod (J + 24) = 0 then -- 53, 83...
				return J + 24;
			end if;

			if X mod (J + 30) = 0 then -- 59, 89...
				return J + 30;
			end if;

			D := D - (2 * 30 * J + 30**2); -- subtract (J+30)**2 - J**2
			J := J + 30;
		end loop;

		return X;

	end Least_Prime_Factor_Opt;
	-- Using wheel 2*3*5 optimization

	function Is_Prime (X : Natural) return Boolean is (X >= 2 and then Least_Prime_Factor (X) = X);

end VSSL.Integers.Primes;
