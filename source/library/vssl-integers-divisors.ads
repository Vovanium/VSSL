pragma SPARK_Mode;

package VSSL.Integers.Divisors is

	function Sum_Of_Divisors (X : Positive) return Positive;

end VSSL.Integers.Divisors;
