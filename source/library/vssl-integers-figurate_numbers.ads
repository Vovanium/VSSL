pragma SPARK_Mode;

with VSSL.Integers.Generic_Figurate_Numbers;

package VSSL.Integers.Figurate_NUmbers is new VSSL.Integers.Generic_Figurate_Numbers (Value => Integer);
