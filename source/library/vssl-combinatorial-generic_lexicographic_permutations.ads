pragma SPARK_Mode;

generic
	type Element is private;
	with function "<" (X, Y : Element) return Boolean is <>;
	type Index is (<>);
	type Set is array (Index range <>) of Element;
package VSSL.Combinatorial.Generic_Lexicographic_Permutations is

	procedure Next_Lexicographic_Permutation (
		P : in out Set);

end VSSL.Combinatorial.Generic_Lexicographic_Permutations;