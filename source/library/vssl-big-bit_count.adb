package body VSSL.Big.Bit_Count is

	-- Required size for integer value up to A in 1024-ths of bit (milli-binary-bits)
	function Radix_to_Mibibits (A : Ext_Radix_Type) return Integer
	is (case A is
		when  1 => 0,    when  2 => 1024,
		when  3 => 1624, when  4 => 2048,
		when  5 => 2378, when  6 => 2648,
		when  7 => 2875, when  8 => 3072,
		when  9 => 3247, when 10 => 3402,
		when 11 => 3543, when 12 => 3672,
		when 13 => 3790, when 14 => 3899,
		when 15 => 4001, when 16 => 4096,
		when 17 => 4186, when 18 => 4271,
		when 19 => 4350, when 20 => 4426,
		when 21 => 4498, when 22 => 4567,
		when 23 => 4633, when 24 => 4696,
		when 25 => 4756, when 26 => 4814,
		when 27 => 4870, when 28 => 4923,
		when 29 => 4975, when 30 => 5025,
		when 31 => 5074, when 32 => 5120,
		when 33 => 5166, when 34 => 5210,
		when 35 => 5253, when 36 => 5295);
	-- The table is a hardcoded formula: ceiling (1024 * log2(A))

	function Bits_in_Number (
		Digit_Count : Natural;
		Radix       : Radix_Type := 10)
		return        Long_Integer
	is
		RL : constant Integer := Radix_to_Mibibits (Ext_Radix_Type (Radix));
	begin
		return (Long_Integer (Digit_Count) * Long_Integer (RL) + 1023) / 1024;
	end Bits_in_Number;

end VSSL.Big.Bit_Count;
