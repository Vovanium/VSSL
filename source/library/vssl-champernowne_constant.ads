pragma SPARK_Mode;
package VSSL.Champernowne_Constant is

	function Champernowne_Constant_Digit (X : Positive) return Natural;

end VSSL.Champernowne_Constant;
