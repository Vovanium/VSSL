-- Implementation of arbitrary precision (binary) numbers
--
-- This is a sort of semi-floating point
-- the point position is fixed at the variable initialization
-- and cannot be changed by assignment / copying
with Interfaces;
use  Interfaces;

package VSSL.Big.Numbers with Pure is

	type Big_Number (<>) is private;
	-- The type for arbitrary precision numbers

	--
	-- Comparisons, tests, sign extraction

	function Is_Fit_Imprecise (X, Where : Big_Number) return Boolean;
	-- Test whether a number fits given storage object
	-- with possible precision loss
	-- note that this can mean loss of all significant bits
	-- resulting zero, but is guaranteed to have no overflow

	function Is_Fit (X, Where : Big_Number) return Boolean;
	-- Test whether a number fits given storage object precisely

	function Sign (A : Big_Number) return Sign_Value;
	-- Sign of a number: -1 for negative, 0 for zero and +1 for positive argument

	function Compare (A, B : Big_Number) return Sign_Value;
	-- Comparison of two numbers: -1 if A < B, 0 if A = B, +1 if A > B
	-- This is essentialy a Sign (A - B) without computing intermediate result.

	function "=" (A, B : Big_Number) return Boolean
	is (Compare (A, B) = 0);

	function "<" (A, B : Big_Number) return Boolean
	is (Compare (A, B) < 0);

	function "<=" (A, B : Big_Number) return Boolean
	is (Compare (A, B) <= 0);

	function ">" (A, B : Big_Number) return Boolean
	is (Compare (A, B) > 0);

	function ">=" (A, B : Big_Number) return Boolean
	is (Compare (A, B) >= 0);

	--
	-- Making, copying subprograms
	--

	function Zero return Big_Number
	with Post => Sign (Zero'Result) = 0;

	function One return Big_Number;

	function Minus_One return Big_Number;

	function Make_With_Bits (
		Highest_Bit : Integer;      -- Highest significant bit (excluding sign)
		Lowest_Bit  : Integer := 0; -- Lowest significant bit position
		Value       : Integer := 0)
		return        Big_Number;
	-- Make a big number with constraints to fit chosen range
	-- High_bits is count of integer bits required to represent
	-- chosent range (or unnecessary high fraction bits if negative)
	-- Low_Bits is position of lowest bit requied for chosen precision
	-- That is Low_Bits = 0 exactly represent integer numbers
	-- High_Bits - Low_Bits is the required precision in bits
	-- Real range will likely be wider as it is rounded to
	-- internal element (that is 32 bits for now)

	procedure Set (T : out Big_Number; S : in Big_Number)
	with Pre => Is_Fit (S, T);
	-- Copies S to T

	--
	-- Arithmetic
	--

	procedure Negate (X : in out Big_Number);
	-- Changes the sign

private
	type Limb_Index is range -2_000_000 .. 2_000_000;

	Limb_Bits : constant := 32;

	subtype Limb is Unsigned_32;
	-- Data type for minimal number segment

	Limb_Base : constant := -(2**Limb_Bits);

	subtype Accumulator is Integer_64;
	subtype U_Accumulator is Unsigned_64;

	type Big_Number is array (Limb_Index range <>) of Limb;

	-- Implemetation detail
	-- Big number x represented as polynome:
	-- x = Sum (i in m .. n) a (i) b**i
	-- where a (i) is element of array, m and n are its boundaries
	-- b is fixed base.
	-- Base is neagtive so it represent negative numbers naturally
	-- however it require additional limb for storing a sign for
	-- symmetric range
	-- Due to negative base even limbs have positive weight
	-- and odd limbs have negative weight.

	function Zero      return Big_Number is (1 .. 0 => <>);
	function One       return Big_Number is (0 => 1);
	function Minus_One return Big_Number is (1 => 1, 0 => 2**Limb_Bits - 1);

	function Is_Fit_Imprecise (X : Big_Number; Last : Limb_Index) return Boolean;
	-- Test whether a number fits storage with given Last limb

end VSSL.Big.Numbers;
