--
-- Various Scientific Subprogram Library
--
pragma SPARK_Mode;

package VSSL with Pure is

	subtype Sign_Value is Integer range -1 .. 1;
	-- The result of sign function

end VSSL;
