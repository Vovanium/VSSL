with VSSL.Integers.Generic_Digit_Arrays.Addition;
with VSSL.Integers.Generic_Digit_Arrays.Multiplication;

package body VSSL.Integers.Big_Decimal is

	function Need_Places (X : Natural) return Natural is
		P      : Natural := 0;
		V      : Natural := X;
	begin
		while V /= 0 loop
			V := V / Decimal_Digit'Modulus;
			P := P + 1;
		end loop; -- todo: replace with ILog10 when available
		return P;
	end Need_Places;

	function Make_With_Places (
		Places  : Positive;
		Initial : Natural := 0)
		return Big_Decimal_Natural
	is
		Last : Integer := Places - 1;
	begin
		return R : Big_Decimal_Natural := (0 .. Index (Last) => 0)
		do
			Set (R, Initial);
		end return;
	end Make_With_Places;

	function To_Big (
		X      : Natural;
		Places : Positive := 1)
		return   Big_Decimal_Natural
	is
	begin
		return Make_With_Places (Natural'Max (Places, Need_Places (X)), X);
	end To_Big;

	function "=" (X, Y : Big_Decimal_Natural) return Boolean
	renames Digit_Arrays."=";

	function "<" (X, Y : Big_Decimal_Natural) return Boolean
	renames Digit_Arrays."<";

	procedure Set (
		X :    out Big_Decimal_Natural;
		Y : in     Big_Decimal_Natural)
	renames Digit_Arrays.Set;

	procedure Set (
		X :    out Big_Decimal_Natural;
		Y : in     Natural)
	is
		T    : Natural := Y;
	begin
		for I in X'Range loop
			X (I) := Decimal_Digit (T mod Decimal_Digit'Modulus);
			T := T / Decimal_Digit'Modulus;
		end loop;
		if T /= 0 then
			raise Constraint_Error with "Initial value does not fit to the given width";
		end if;
	end Set;

	--package Addition is new Digit_Arrays.Addition;

	--function "+" (X, Y : Big_Decimal_Natural) return Big_Decimal_Natural renames Addition."+";

	-- due to bug in GNAT it fails to compile this and following instantiation

	function "+" (X, Y : Big_Decimal_Natural) return Big_Decimal_Natural is
		First_X : constant Index := Digit_Arrays.First_Nonzero (X);
		First_Y : constant Index := Digit_Arrays.First_Nonzero (Y);
		First   : constant Index := Index'Min (First_X, First_Y);
		Last_X  : constant Index := Digit_Arrays.Last_Nonzero (X);
		Last_Y  : constant Index := Digit_Arrays.Last_Nonzero (Y);
		Last    : constant Index := Index'Max (Last_X, Last_Y) + 1;
		Carry   : Decimal_Digit := 0;
		XJ, YJ,
		Z       : Decimal_Digit;
	begin
		return R : Big_Decimal_Natural := (First .. Last => 0) do
			for J in First .. Last loop
				XJ := Digit_Arrays.Element (X, J);
				YJ := Digit_Arrays.Element (Y, J);
				Z := XJ + YJ + Carry;
				R (J) := Z;
				Carry := (if Z < XJ then 1 elsif Z = XJ then Carry else 0);
			end loop;
		end return;
	end "+";

	type Product is mod Decimal_Digit'Modulus**2;

	--package Multiplication is new Digit_Arrays.Multiplication (Digit_Product_Type => Product);

	--function "*" (X, Y : Big_Decimal_Natural) return Big_Decimal_Natural renames Multiplication."*";

	function "*" (X, Y : Big_Decimal_Natural) return Big_Decimal_Natural is
		First_X : constant Index := Digit_Arrays.First_Nonzero (X);
		First_Y : constant Index := Digit_Arrays.First_Nonzero (Y);
		Last_X  : constant Index := Digit_Arrays.Last_Nonzero (X);
		Last_Y  : constant Index := Digit_Arrays.Last_Nonzero (Y);
		XP, P,
		Carry   : Product;
	begin
		return R : Big_Decimal_Natural := (First_X + First_Y .. Last_X + Last_Y + 1 => 0) do
			for J in First_X .. Last_X loop
				Carry := 0;
				XP := Product (X (J));
				for K in First_Y .. Last_Y loop
					P := XP * Product (Y (K)) + Product (R (J + K)) + Carry;
					R (J + K) := Decimal_Digit (P mod Decimal_Digit'Modulus);
					Carry := P / Decimal_Digit'Modulus;
				end loop;
				R (J + Last_Y + 1) := Decimal_Digit (Carry);
			end loop;
		end return;
	end "*";

	function ILog10 (X : Big_Decimal_Natural) return Integer
	is (Integer (Digit_Arrays.Last_Nonzero (X)));

	function Digit (X : Big_Decimal_Natural; N : Natural) return Natural
	is (Integer (Digit_Arrays.Element (X, Index (N))));


end VSSL.Integers.Big_Decimal;
