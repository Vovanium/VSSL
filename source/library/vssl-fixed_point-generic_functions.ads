--
-- Common implementations for elementary functions on fixed point types
-- For CPUs with fast multiplication
--
-- Warning: Library deals with internal binary representation of FP numbers
--          it requires they're just integers scaled by Real'Small
--
pragma SPARK_Mode;

generic
	type Real is delta <>; -- Fixed point type
	type Representation is mod <>;   -- Internal representation backup type (for fast)
	Fraction_Bits : Integer; -- Number of bits in fractional part
	with function Shift_Left (X : Representation; N : Natural) return Representation is <>;
	with function Shift_Right_Arithmetic (X : Representation; N : Natural) return Representation is <>;
package VSSL.Fixed_Point.Generic_Functions is

	pragma Assert (Real'Size = Representation'Size);
	pragma Assert (Real'Small = 0.5 ** Fraction_Bits);

	function Floor (X : Real) return Real;
	-- Greatest integer less or equal to X
	-- (analogous to T'Floor of a floating point type)

	function Ceiling (X : Real) return Real;
	-- Smallest integer greater or equal to X
	-- (analogous to T'Ceiling of a floating point type)

	function Rounding (X : Real) return Real;
	-- Integer nearest to X.
	-- If an argument is exactly halfway between two integers greater one is returned.
	-- (analogous to T'Rounding of a floating point type except for half-intger numbers)

	function Unbiased_Rounding (X : Real) return Real;
	-- Integer nearest to X.
	-- If an argument is exactly halfway between two integers even one is returned.
	-- (analogous to T'Unbiased_Rounding of a floating point)

	function Truncation (X : Real) return Real;
	-- Ceiling (X) if X negative, Floor (X) otherwise
	-- (analogous to T'Truncation of a floating point)

	function Copy_Sign (X, Sign : Real) return Real;
	-- Value with magnitude of X and sign of Sign
	-- (analogous to T'Copy_Sign of a floating point type)

	function "**" (X : Real; N : Integer'Base) return Real
	with Pre => (N >= 0 or X >= 1.0 / Real'Last or X <= 1.0 / Real'First);
	-- Raise X to the power N (integer)

	function "**" (X, N : Real) return Real;
	-- Raise X to the power N (real)

	function Sqrt (X : Real) return Real;
	-- Square root
	-- Raises Ada.Numerics.Argument_Error on neative arguments

	function Cube_Root (X : Real) return Real;
	-- Cube root

end VSSL.Fixed_Point.Generic_Functions;
