with VSSL.Reals.Generic_String_to_Number;

package body VSSL.Floating_Point.Generic_Quad_Reals is

	function To_Quad_Real (A : Integer) return Quad_Real is
		R : Quad_Real;
		B : Integer := A;
	begin
		for J in R'Range loop
			R (J) := Real_Type (B);
			B := B - Integer (R (J));
		end loop;
		return R;
	end To_Quad_Real;

	procedure Quick_Two_Sum (S, E : out Real_Type; A, B : in Real_Type)
	with Pre => abs A > abs B;
		-- Sum of two numbers with double precision
	procedure Quick_Two_Sum (S, E : out Real_Type; A, B : in Real_Type) is
	begin
		S := A + B;
		E := B - (S - A);
	end Quick_Two_Sum;

	procedure Two_Sum (S, E : out Real_Type; A, B : in Real_Type) is
		-- Sum of two numbers with double precision
		V : Real_Type;
	begin
		S := A + B;
		V := S - A;
		E := (A - (S - V)) + (B - V);
	end Two_Sum;

	procedure Split (Hi, Lo : out Real_Type; A : in Real_Type) is
		T : Real_Type;
	begin
		T := ((Real_Type (Real_Type'Machine_Radix))**(Real_Type'Machine_Mantissa / 2 + 1) + 1.0) * A;
		Hi := T - (T - A);
		Lo := A - Hi;
	end Split;

	procedure Two_Prod (P, E : out Real_Type; A, B : in Real_Type) is
		AHi, ALo : Real_Type;
		BHi, BLo : Real_Type;
	begin
		P := A * B;
		Split (AHi, ALo, A);
		Split (BHi, BLo, B);
		E := ((AHi * BHi - P) + AHi * BLo + ALo * BHi) + ALo * BLo;
	end Two_Prod;

	subtype Real_Array_5 is Real_Array (0 .. 4);

	function Renormalize (A : in Real_Array_5) return Quad_Real is
		S : Real_Type := A (4);
		T : Real_Array (0 .. 4);
		K : Natural;
		E : Real_Type;
		B : Quad_Real;
	begin
		for I in reverse 0 .. 3 loop
			Quick_Two_Sum (S, T (I + 1), A (I), S);
		end loop;
		T (0) := S;
		K := 0;
		B := (others => 0.0);
		for I in 1 .. 4 loop
			Quick_Two_Sum (S, E, S, T (I));
			if E /= 0.0 then
				B (K) := S;
				S := E;
				K := K + 1;
			end if;
		end loop;
		if K <= B'Last then
			B (K) := S; -- this is missing in article
		end if;
		return B;
	end Renormalize;

	function "+" (A : Quad_Real; B : Real_Type) return Quad_Real is
		E : Real_Type;
		T : Real_Array (0 .. 4);
	begin
		E := B;
		for I in 0 .. 3 loop
			Two_Sum (T (I), E, E, A (I));
		end loop;
		T (4) := E;
		return Renormalize (T);
	end "+";

	subtype Real_Array_8 is Real_Array (0 .. 7);

	procedure Sort_8 (X : out Real_Array_8; A, B : in Quad_Real) is
		-- Sort two quad numbers in one sorted array
		I, J, K : Natural := 0;
	begin
		while I < 4 and J < 4 loop
			if abs A (I) > abs B (J) then
				X (K) := A (I);
				I := I + 1;
			else
				X (K) := B (J);
				J := J + 1;
			end if;
			K := K + 1;
		end loop;
		while I < 4 loop
			X (K) := A (I);
			I := I + 1;
			K := K + 1;
		end loop;
		while J < 4 loop
			X (K) := B (J);
			J := J + 1;
			K := K + 1;
		end loop;
	end Sort_8;

	procedure Double_Accumulate (S : out Real_Type; U, V : in out Real_Type; X : in Real_Type) is
		-- Routine for addition
	begin
		Two_Sum (S, V, V, X);
		Two_Sum (S, U, U, S);
		if U = 0.0 then
			U := S;
			S := 0.0;
		end if;
		if V = 0.0 then
			V := U;
			U := S;
			S := 0.0;
		end if;
	end Double_Accumulate;

	function "+" (A, B : Quad_Real) return Quad_Real is
		X : Real_Array (0 .. 7);
		S, U, V : Real_Type := 0.0;
		I, K : Natural := 0;
		C : Real_Array (0 .. 4) := (others => 0.0);
	begin
		Sort_8 (X, A, B);
		while K < 4 and I < 8 loop
			Double_Accumulate (S, U, V, X (I));
			if S /= 0.0 then
				C (K) := S;
				K := K + 1;
			end if;
			I := I + 1;
		end loop;
		if K <= 3 then -- condition in article looks bit wrong
			C (K + 1) := V;
		end if;
		--if K <= 4 then -- this too
			C (K) := U;
		--end if;
		return Renormalize (C);
	end "+";

	function "-" (A : Quad_Real) return Quad_Real is (-A (0), -A (1), -A (2), -A (3));

	--      X
	--      |
	-- Y ->[+]-V---.
	--      |U     |
	-- Z ->[+]-W->[+]-.
	--      |      '--+-> R1
	--      R0        '-> R2

	procedure Three_Sum (R0, R1, R2 : out Real_Type; X, Y, Z : in Real_Type) is
		-- Add three numbers with triple precision
		U, V, W : Real_Type;
	begin
		Two_Sum (U, V, Y, X);
		Two_Sum (R0, W, Z, U);
		Two_Sum (R1, R2, W, V);
	end Three_Sum;

	procedure Three_Sum (R0, R1 : out Real_Type; X, Y, Z : in Real_Type) is
		-- Add three numbers with double precision
		U, V, W : Real_Type;
	begin
		Two_Sum (U, V, Y, X);
		Two_Sum (R0, W, Z, U);
		R1 := W + V;
	end Three_Sum;

	--       A0      A1      A2      A3
	--       |       |       |       |
	-- B ->-[*]-. B>[*]-. B>[*]-. B>(*)
	--       |  |    |  |    |  |    |
	--       |  |    |  |    |  'W->[ ]
	--       |  |    |  'V->[ ]--V->[+]--V---.
	--       |  'U->[+]--U->[+]--U---+------(+)
	--       |T0     |T1     |T2     |T3     |T4
	--      [         Renormalization         ]
	--       |       |       |       |
	--       S0      S1      S2      S3

	function "*" (A : Quad_Real; B : Real_Type) return Quad_Real is
		T : Real_Array (0 .. 4);
		U, V, W : Real_Type;
	begin
		Two_Prod (T (0), U, A (0), B);
		Two_Prod (T (1), V, A (1), B);
		Two_Prod (T (2), W, A (2), B);
		T (3) := A (3) * B;
		Two_Sum (T (1), U, T (1), U);
		Three_Sum (T (2), V, U, T (2), V, U);
		Three_Sum (T (3), V,    T (3), W, V);
		T (4) := V + U;
		return Renormalize (T);
	end "*";

	-- -->[ ]
	-- -->[+]----------.
	-- -->[ ]----------+------.
	--     '----.      |      |
	-- -->[ ]   |      |      |
	-- -->[+]---+---->[+]-.   |
	-- -->[ ]---+------+--+->(+)
	--     |    |      | Y|   |
	--     |    |    R1|  '->[ ]
	--     '-->[+]-W->[+]-W->[+]
	--          |      |      |
	--          |      '------+-> R1
	--          R0            '-> R2

	procedure Six_Three_Sum (R0, R1, R2 : out Real_Type; A0, A1, A2, A3, A4, A5 : in Real_Type) is
		-- Sum six numbers with triple precision
		U0, U1, U2, V0, V1, V2 : Real_Type;
		W, Y : Real_Type;
	begin
		Three_Sum (U0, U1, U2, A0, A1, A2);
		Three_Sum (V0, V1, V2, A3, A4, A5);
		Two_Sum (R0, W, U0, V0);
		Two_Sum (R1, Y, U1, V1);
		R2 := U2 + V2;
		Two_Sum (R1, W, R1, W);
		R2 := R2 + Y + W;
	end Six_Three_Sum;

	procedure Double_Sum (S0, S1 : out Real_Type; A0, A1, B0, B1 : in Real_Type) is
		-- Sum two double precision
		E, T1, T2 : Real_Type;
	begin
		T1 := A0 + B0;
		E := T1 - A0;
		T2 := ((B0 - E) + (A0 - (T1 - E))) + A1 + B1;

		S0 := T1 + T2;
		S1 := T2 - (S0 - T1);
	end Double_Sum;

	procedure Nine_Two_Sum (R0, R1 : out Real_Type; A0, A1, A2, A3, A4, A5, A6, A7, A8 : in Real_Type) is
		-- Sum nine numbers with double precision
		U0, U1, V0, V1, W0, W1 : Real_Type;
	begin
		Two_Sum (U0, U1, A0, A1);
		Two_Sum (V0, V1, A2, A3);
		Double_Sum (U0, U1, U0, U1, V0, V1);

		Two_Sum (V0, V1, A4, A5);
		Two_Sum (W0, W1, A6, A7);
		Double_Sum (V0, V1, V0, V1, W0, W1);
		Double_Sum (U0, U1, U0, U1, V0, V1);
		Double_Sum (R0, R1, A8, 0.0, U0, U1);
	end Nine_Two_Sum;

	function "*" (A, B : Quad_Real) return Quad_Real is
		T : Real_Array (0 .. 4);
		E0 : Real_Type;
		P1, E1 : Real_Array (0 .. 1);
		P2, E2 : Real_Array (0 .. 2);
		P3, E3 : Real_Array (0 .. 3);
		V3, V4 : Real_Type;
		X : Real_Type;
	begin
		Two_Prod (T (0), E0, A (0), B (0));

		Two_Prod (P1 (0), E1 (0), A (0), B (1));
		Two_Prod (P1 (1), E1 (1), A (1), B (0));

		Three_Sum (T (1), T (2), V3, E0, P1 (0), P1 (1));

		Two_Prod (P2 (0), E2 (0), A (0), B (2));
		Two_Prod (P2 (1), E2 (1), A (1), B (1));
		Two_Prod (P2 (2), E2 (2), A (2), B (0));

		Six_Three_Sum (T (2), T (3), V4, T (2), E1 (0), E1 (1), P2 (0), P2 (1), P2 (2));

		Two_Prod (P3 (0), E3 (0), A (0), B (3));
		Two_Prod (P3 (1), E3 (1), A (1), B (2));
		Two_Prod (P3 (2), E3 (2), A (2), B (1));
		Two_Prod (P3 (3), E3 (3), A (3), B (0));

		Nine_Two_Sum (T (3), T (4), T (3), V3, E2 (0), E2 (1), E2 (2),
			P3 (0), P3 (1), P3 (2), P3 (3));

		Nine_Two_Sum (T (4), X, T (4), V4, E3 (0), E3 (1), E3 (2), E3 (3),
			A (1) * B (3), A (2) * B (2), A (3) * B (1));

		return Renormalize (T);
	end "*";

	function "/" (A : Quad_Real; B : Real_Type) return Quad_Real is
		R : Quad_Real := A;
		Q : Real_Array (0 .. 4);
	begin
		for I in 0 .. 4 loop
			Q (I) := R (0) / B;
			R := R - (Q (I) * B);
		end loop;
		return Renormalize (Q);
	end "/";

	function "/" (A, B : Quad_Real) return Quad_Real is
		R : Quad_Real := A;
		Q : Real_Array (0 .. 4);
	begin
		for I in 0 .. 4 loop
			Q (I) := R (0) / B (0);
			R := R - (Q (I) * B);
		end loop;
		return Renormalize (Q);
	end "/";

	function "**" (A : Quad_Real; B : Integer'Base) return Quad_Real is
		N : Integer'Base := B;
		R : Quad_Real := To_Quad_Real (1.0);
		Q : Quad_Real := A;
	begin
		while N /= 0 loop
			if N mod 2 /= 0 then
				R := R * Q;
			end if;
			N := N / 2;
			Q := Q * Q; -- TODO: Optimize squaring
		end loop;
		if B < 0 then
			R := 1.0 / R;
		end if;
		return R;
	end "**";

	function Multiply_by_Power (A, R : Quad_Real; N : Integer) return Quad_Real
	is (A * R**N);

	function String_to_Number is new VSSL.Reals.Generic_String_to_Number (
		Number_Type => Quad_Real,
		To_Number => To_Quad_Real,
		Exponent_Type => Integer);

	function Value (S : String) return Quad_Real renames String_to_Number;

	function "=" (A, B : Quad_Real) return Boolean
	is (for all J in A'Range => A (J) = B (J));


	function "<" (A, B : Quad_Real) return Boolean
	is
	begin
		for J in A'Range loop
			if A (J) /= B (J) then
				return A (J) < B (J);
			end if;
		end loop;
		return False;
	end "<";

	function "<=" (A, B : Quad_Real) return Boolean
	is
	begin
		for J in A'Range loop
			if A (J) /= B (J) then
				return A (J) < B (J);
			end if;
		end loop;
		return True;
	end "<=";

	function "abs" (A : Quad_Real) return Quad_Real
	is (if A (0) < 0.0 then -A else A);

	function Floor (A : Quad_Real) return Quad_Real is
		R : Real_Array (0 .. 4) := (others => 0.0);
	begin
		for I in 0 .. 3 loop
			R (I) := Real_Type'Floor (A (I));
			exit when R (I) /= A (I);
		end loop;
		return Renormalize (R);
	end Floor;

	function Ceiling (A : Quad_Real) return Quad_Real is
		R : Real_Array (0 .. 4) := (others => 0.0);
	begin
		for I in 0 .. 3 loop
			R (I) := Real_Type'Ceiling (A (I));
			exit when R (I) /= A (I);
		end loop;
		return Renormalize (R);
	end Ceiling;

end VSSL.Floating_Point.Generic_Quad_Reals;
