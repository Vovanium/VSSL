pragma SPARK_Mode;

with Interfaces;
use  Interfaces;
with VSSL.Integers.Modular_Binary;

package VSSL.Integers.Binary_16 is new VSSL.Integers.Modular_Binary (Unsigned_16);