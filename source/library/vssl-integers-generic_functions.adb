package body VSSL.Integers.Generic_Functions is

	function Floor_Log (X, Y : Value) return Integer is
		R : Integer := 0;
		P : Value := 1;
	begin
		if X = 0 then
			return Integer'First;
		end if;
		declare
			XL : constant Value := X / Y;
		begin
			while P <= XL loop
				P := P * Y;
				R := R + 1;
				pragma Loop_Variant (Increases => P);
				pragma Loop_Invariant (Value (R) < P);
				pragma Loop_Invariant (P > 0);
				pragma Loop_Invariant (R > 0);
			end loop;
			return R;
		end;
	end Floor_Log;

	function Floor_Log_10 (X : Value) return Integer is (Floor_Log (X, 10));

	function GCD (X, Y : Value) return Value is
		U : Value := X;
		V : Value := Y mod X;
		T : Value;
	begin
		while V /= 0 loop
			pragma Assert (U > 0 and V > 0);
			T := U mod V;
			pragma Assert (T >= 0 and T < V and T <= U);
			pragma Assert (T /= 0 or V <= U);
			U := V;
			V := T;
			pragma Loop_Invariant (V <= X and V < U and U <= Y and U > 0 and V >= 0);
		end loop;
		return U;
	end GCD;

end VSSL.Integers.Generic_Functions;