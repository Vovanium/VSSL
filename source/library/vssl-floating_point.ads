pragma SPARK_Mode;

package VSSL.Floating_Point with Pure is

	function Multiply_by_Power (A, R : Float; N : Integer) return Float
	is (A * R**N);
	-- used in conversion subprograms

	function To_Number (N : Integer) return Float
	is (Float (N));
	-- used in conversion subprograms

	function Multiply_by_Power (A, R : Long_Float; N : Integer) return Long_Float
	is (A * R**N);
	-- used in conversion subprograms

	function To_Number (N : Integer) return Long_Float
	is (Long_Float (N));
	-- used in conversion subprograms

end VSSL.Floating_Point;
