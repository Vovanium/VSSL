package body VSSL.Integers.Generic_Digit_Arrays.Addition is

	function "+" (X, Y : Digit_Array) return Digit_Array is
		First_X : constant Index := First_Nonzero (X);
		First_Y : constant Index := First_Nonzero (Y);
		First   : constant Index := Index'Min (First_X, First_Y);
		Last_X  : constant Index := Last_Nonzero (X);
		Last_Y  : constant Index := Last_Nonzero (Y);
		Last    : constant Index := Index'Max (Last_X, Last_Y) + 1;
		Carry   : Digit_Type := 0;
		XJ, YJ,
		Z       : Digit_Type;
	begin
		return R : Digit_Array := (First .. Last => 0) do
			for J in First .. Last loop
				XJ := Element (X, J);
				YJ := Element (Y, J);
				Z := XJ + YJ + Carry;
				R (J) := Z;
				Carry := (if Z < XJ then 1 elsif Z = XJ then Carry else 0);
			end loop;
		end return;
	end "+";

end VSSL.Integers.Generic_Digit_Arrays.Addition;
