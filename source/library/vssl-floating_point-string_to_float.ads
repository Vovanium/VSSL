with VSSL.Floating_Point.Generic_String_to_Float;

function VSSL.Floating_Point.String_to_Float is new VSSL.Floating_Point.Generic_String_to_Float (
	Number_Type => Float);
