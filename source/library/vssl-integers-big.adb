package body VSSL.Integers.Big is

	--
	-- Common primitives
	--

	function Get_Limb (X : Big_Integer; N : Limb_Index'Base) return Limb
	is (
		if  N <= X.Last then X.Data (N)
		elsif Is_Negative (X.Data (X.Last)) then Minus_One_Limb else 0);

	--
	-- Making numbers
	--

	function Digits_to_Bits (N : Natural) return Natural is (N * 3 + (N + 2) / 3);
	-- ceiling (N * log_2 (10)) approximation giving ~ 1% error

	function Log2_mult30 (N : Positive) return Natural
	is (case N is
		when  1 =>   0,
		when  2 =>  30,
		when  3 =>  48,
		when  4 =>  60,
		when  5 =>  70,
		when  6 =>  78,
		when  7 =>  85,
		when  8 =>  90,
		when  9 =>  96,
		when 10 => 100,
		when 11 => 104,
		when 12 => 108,
		when 13 => 112,
		when 14 => 115,
		when 15 => 118,
		when others => 120);
	-- Logarithm base 2 multiplied by 30 rounded up. Used for required storage size calculations.

	function Digits_to_Bits (N : Natural; Radix : Natural) return Natural
	is ((Log2_mult30 (Radix) * N + 29) / 30);

	function With_Bits (N : Positive) return Big_Integer
	is (Last => N / Limb_Size, Data => (others => 0));

	function With_Digits (N : Positive) return Big_Integer
	is (With_Bits (Digits_to_Bits (N)));

	function From_String (S : String) return Big_Integer is
		Is_Neg : Boolean := False;
		DN     : Natural := 0;
		Based  : Boolean := False;
		Radix  : Limb := 0;
		Mant_F : Natural := S'First;
	begin
		-- First pass: detecting negative, radix and number of digits
		for J in S'Range loop
			case S (J) is
				when '-' =>
					Is_Neg := True;
				when '+' =>
					Is_Neg := False;
				when ' ' | '_' =>
					null;
				when '0' .. '9' =>
					DN := DN + 1;
					if (not Based) and then Radix <= 16 then
						Radix := Radix * 10 + (Character'Pos (S (J)) - Character'Pos ('0'));
					end if;
				when 'A' .. 'F' | 'a' .. 'f' =>
					if not Based then
						raise Constraint_Error with "Illegal digit in non-based big number";
					end if;
					DN := DN + 1;
				when '#' =>
					if not Based then
						Based := True;
						Mant_F := J + 1;
						DN := 0;
					end if;
				when others =>
					raise Constraint_Error with "Illeal character in big number image";
			end case;
		end loop;
		if not Based then
			Radix := 10;
		else
			if Radix < 2 or Radix > 16 then
				raise Constraint_Error with "Big number radix is out of range";
			end if;
		end if;
		return R : Big_Integer := With_Bits (Digits_to_Bits (DN, Integer (Radix))) do
			declare
				procedure Next_Digit (X : in Limb) is
					C : Limb := X;
				begin
					for J in R.Data'Range loop
						Multiply (C, R.Data (J), R.Data (J), Radix, C);
					end loop;
				end Next_Digit;
			begin
				-- Second pass: converting a number
				for J in Mant_F .. S'Last loop
					case S (J) is
						when '0' .. '9' =>
							Next_Digit (Character'Pos (S (J)) - Character'Pos ('0'));
						when 'A' .. 'F' =>
							Next_Digit (Character'Pos (S (J)) - Character'Pos ('A') + 10);
						when 'a' .. 'f' =>
							Next_Digit (Character'Pos (S (J)) - Character'Pos ('a') + 10);
						when others =>
							null;
					end case;
				end loop;
			end;
			if Is_Neg then
				Negate (R);
			end if;
		end return;
	end From_String;

	-- @todo: Improve estimation by first digit (so 2E9 fit 32 bit)
	-- @todo: Skip leading zeros
	-- @todo: Allow radix as second parameter

	package body Signed_Conversions is
		function To_Big (X : Value) return Big_Integer is
			T : Value := X;
		begin
			if Limb'Modulus < Value'Last then -- wish it be compile-time check
				return R : Big_Integer := With_Bits (Value'Size) do
					for J in R.Data'Range loop
						R.Data (J) := Limb (T mod Limb'Modulus);
						T := (T - Value (R.Data (J))) / Limb'Modulus;
					end loop;
				end return;
			else
				return (Last => 0, Data => (0 => Limb (X)));
			end if;
		end To_Big;
	end Signed_Conversions;

	--
	-- Comparisons
	--

	function Fit_Last (X : Big_Integer) return Limb_Index is
	begin
		if Is_Negative (X) then
			for J in reverse X.Data'Range loop
				if X.Data (J) /= Minus_One_Limb then
					if Is_Negative (X.Data (J)) then
						return J;
					else
						return J + 1;
					end if;
				end if;
			end loop;
			return 0; -- case of -1
		else
			for J in reverse X.Data'Range loop
				if X.Data (J) /= Zero_Limb then
					if Is_Negative (X.Data (J)) then
						return J + 1;
					else
						return J;
					end if;
				end if;
			end loop;
			return 0; -- case of 0
		end if;
	end Fit_Last;
	-- Minimum last limb of a storage given number fits

	function Is_Fit (X : Big_Integer; Last : Limb_Index'Base) return Boolean is
		(Fit_Last (X) <= Last);
	-- Check if number fits in storage with given bounds

	function Is_Fit (X, Where : Big_Integer) return Boolean is
		(Is_Fit (X, Where.Last));

	function Is_Zero (X : Big_Integer) return Boolean is
		(for all J of X.Data => J = 0);

	function Is_Negative (X : Big_Integer) return Boolean is
		(Is_Negative (X.Data (X.Last)));

	function "=" (X, Y : Big_Integer) return Boolean is
		LX : constant Limb_Index'Base := Fit_Last (X);
		LY : constant Limb_Index'Base := Fit_Last (Y);
	begin
		return LX = LY and then
			(for all J in 0 .. LX => X.Data (J) = Y.Data (J));
	end "=";

	--
	-- Assignments
	--

	procedure Set (T : out Big_Integer; Y : in Big_Integer) is
		LY : constant Limb_Index'Base := Fit_Last (Y);
	begin
		if LY > T.Last then
			raise Constraint_Error with "Value does not fit in target";
		end if;
		T.Data (0 .. LY) := Y.Data (0 .. LY);
		T.Data (LY + 1 .. T.Last) := (others => (if Is_Negative (Y) then Minus_One_Limb else 0));
	end Set;

	--
	-- Implace arithmetic
	--

	procedure Add (T : in out Big_Integer; X : in Big_Integer) is
		LX    : constant Limb_Index'Base := Fit_Last (X);
		C     : Carry := Carry_0;
		Ov    : Boolean;
	begin
		if LX > T.Last then
			raise Constraint_Error with "Result of addition does not fit in target";
		end if;
		for J in 0 .. T.Last loop
			Add (OV => Ov, CO => C, Y => T.Data (J),
				A => T.Data (J), B => Get_Limb (X, J), CI => C);
		end loop;
		if Ov then
			raise Constraint_Error with "Result of addition does not fit in target (due to carry out)";
		end if;
	end Add;

	procedure Negate (T : in out Big_Integer) is
		F : Limb_Index := 0; -- first non-zero
	begin
		while F < T.Last and then T.Data (F) = 0 loop
			F := F + 1;
		end loop;
		if F = T.Last and then T.Data (F) = 2**(Limb_Size - 1) then -- corner case
			raise Constraint_Error with "Result of negation does not fit in target";
		end if;
		T.Data (F) := -T.Data (F);

		for J in F + 1 .. T.Last loop
			T.Data (J) := not T.Data (J);
		end loop;

	end Negate;

	procedure Subtract (T : in out Big_Integer; X : in Big_Integer) is
		LX     : constant Limb_Index'Base := Fit_Last (X);
		B      : Borrow := Borrow_0;
		Ov     : Boolean;
	begin
		if LX > T.Last then
			raise Constraint_Error with "Result of subtraction does not fit in target";
		end if;
		for J in 0 .. T.Last loop
			Subtract (OV => Ov, BO => B, Y => T.Data (J),
				A => T.Data (J), B => Get_Limb (X, J), BI => B);
		end loop;
		if Ov then
			raise Constraint_Error with "Result of subtraction overflows";
		end if;
	end Subtract;

	procedure Multiply (T : in out Big_Integer; A, B : in Big_Integer) is
		LA : constant Limb_Index := Fit_Last (A);
		LB : constant Limb_Index := Fit_Last (B);
		CM : Limb    := 0; -- Multiplication carry
		Ov : Boolean;
		Bw : Borrow; -- Adjustment subtraction carry
		Cy : Carry;  -- Addition carry
		SC : Limb    := 0;
	begin
		if T.Last < LA + LB then
			raise Constraint_Error with "Result of multiplication does not fit in target";
		end if;
		Set (T, Zero);
		Cy := Carry_0;
		for J in 0 .. LA loop
			T.Data (J + LB) := CM; -- multiplication carry from previous cycle
			CM := 0;
			for K in 0 .. LB loop
				Multiply_Add (CM, T.Data (J + K), A.Data (J), B.Data (K), CM);
			end loop;
		end loop;

		-- CM now should be at LA + LB + 1 but it can be absent

		-- Adjust result if either operand A is negative as multiplication above is unsigned
		if Is_Negative (A) then
			Bw := Borrow_0;
			for J in 0 .. LB - 1 loop
				Subtract (OV => Ov, BO => Bw, Y => T.Data (J + LA + 1),
					A => T.Data (J + LA + 1), B => B.Data (J), BI => Bw);
			end loop;
			Subtract (OV => Ov, BO => Bw, Y => CM,
				A => CM, B => B.Data (LB), BI => Bw);
		end if;

		if Is_Negative (B) then
			Bw := Borrow_0;
			for J in 0 .. LA - 1 loop
				Subtract (OV => Ov, BO => Bw, Y => T.Data (J + LB + 1),
					A => T.Data (J + LB + 1), B => A.Data (J), BI => Bw);
			end loop;
			Subtract (OV => Ov, BO => Bw, Y => CM,
				A => CM, B => A.Data (LA), BI => Bw);
		end if;

		if T.Last = LA + LB and then
			not (if Is_Negative (T.Data (LA + LB)) then CM = Minus_One_Limb
			                                       else CM = Zero_Limb)
		then
			raise Constraint_Error with "Result of multiplication overflows";
		end if;
		for J in LA + LB + 1 .. T.Last loop
			T.Data (J) := CM;
			CM := Shift_Right_Arithmetic (CM, Limb_Size);
		end loop;

	end Multiply;

	--    LA                LB
	--  +---+---+---+     +---+---+---+
	--  |   |   |   |  X  |   |   |   |
	--  +---+---+---+     +---+---+---+
	--                +---+---+---+---+
	--                | C |   |   |   |
	--        LA+     +---+---+---+---+
	--        LB+ +---+---+---+---+
	--         1  | C |   |   |   |
	--            +---+---+---+---+
	--        +---+---+---+---+
	--        | C |   |   |   |
	--        +---+---+---+---+

	--
	-- Conventional arithmetic
	--

	function "+" (X : Big_Integer) return Big_Integer is
		LX : constant Limb_Index'Base := Fit_Last (X);
	begin
		return Big_Integer'(Last => LX, Data => X.Data (0 .. LX));
	end "+";

	function "-" (X : Big_Integer) return Big_Integer is
		LX : constant Limb_Index'Base := Fit_Last (X);
	begin
		return R : Big_Integer (if Negation_Overflows (X.Data (LX)) then LX + 1 else LX) do
			-- corner case: 2**n need one more bit than -2**n
			R.Data (R.Last) := Minus_One_Limb; -- this will be used in corner case
			R.Data (0 .. LX) := X.Data (0 .. LX);
			Negate (R);
		end return;
	end "-";

	function "+" (X, Y : Big_Integer) return Big_Integer is
		LX : constant Limb_Index'Base := Fit_Last (X);
		LY : constant Limb_Index'Base := Fit_Last (Y);
		L  : constant Limb_Index'Base := Limb_Index'Base'Max (LX, LY);
		LR : constant Limb_Index'Base := (if Addition_Overflows (Get_Limb (X, L), Get_Limb (Y, L))
			then L + 1 else L); -- estimate last index of result
	begin
		return R : Big_Integer (Last => LR) do
			Set (R, X);
			Add (R, Y);
		end return;
	end "+";

	function "-" (X, Y : Big_Integer) return Big_Integer is
		LX : constant Limb_Index'Base := Fit_Last (X);
		LY : constant Limb_Index'Base := Fit_Last (Y);
		L  : constant Limb_Index'Base := Limb_Index'Base'Max (LX, LY);
		LR : constant Limb_Index'Base := (if Subtraction_Overflows (Get_Limb (X, L), Get_Limb (Y, L))
			then L + 1 else L); -- estimate last index of result
	begin
		return R : Big_Integer (Last => LR) do
			Set (R, X);
			Subtract (R, Y);
		end return;
	end "-";

	function "*" (A, B : Big_Integer) return Big_Integer is
		LA : constant Limb_Index := Fit_Last (A);
		LB : constant Limb_Index := Fit_Last (B);
	begin
		return R : Big_Integer (Last => LA + LB + 1) do
			Multiply (R, A, B);
		end return;
	end "*";

end VSSL.Integers.Big;
