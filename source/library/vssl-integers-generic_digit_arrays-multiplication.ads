generic
	type Digit_Product_Type is mod <>;
package VSSL.Integers.Generic_Digit_Arrays.Multiplication is
	pragma Assert (Digit_Product_Type'Modulus >= Digit_Type'Modulus**2);

	function "*" (X, Y : Digit_Array) return Digit_Array;

end VSSL.Integers.Generic_Digit_Arrays.Multiplication;