package body VSSL.Big.Numbers is

	generic
		type Value_Type is range <>;
	procedure Generic_Extract_Limb (
		V : in     Value_Type;  -- the value
		L :    out Limb;        -- low-order limb
		W :    out Value_Type); -- high order residual

	-- Tests

	function Is_Fit_Imprecise (X : Big_Number; Last : Limb_Index) return Boolean
	is
	begin
		if X'Last <= Last then
			return True;
		end if;
		for J in Limb_Index'Max (X'First, Last + 1) .. X'Last loop
			if X (J) /= 0 then
				return False;
			end if;
		end loop;
		return True;
	end Is_Fit_Imprecise;

	function Is_Fit_Imprecise (X, Where : Big_Number) return Boolean
	is (Is_Fit_Imprecise (X, Where'Last));

	function Is_Fit (X : Big_Number; First, Last : Limb_Index) return Boolean
	is
	begin
		if not Is_Fit_Imprecise (X, Last) then
			return False;
		end if;
		if X'First >= First then
			return True;
		end if;
		for J in X'First .. Limb_Index'Min (X'Last, First - 1) loop
			if X (J) /= 0 then
				return False;
			end if;
		end loop;
		return True;
	end Is_Fit;

	function Is_Fit (X, Where : Big_Number) return Boolean
	is (Is_Fit (X, Where'First, Where'Last));

	--

	function Sign (A : Big_Number) return Sign_Value
	is
	begin
		for J in reverse A'Range loop
			if A (J) /= 0 then
				return (if J mod 2 = 0 then +1 else -1);
			end if;
		end loop;
		return 0;
	end Sign;

	function Compare (A, B : Big_Number) return Sign_Value
	is
		S : Sign_Value;
	begin
		-- if one of numbers have higher limbs, test them first;
		if A'Last > B'Last then
			S := Sign (A (Limb_Index'Max (B'Last + 1, A'First) .. A'Last));
			if S /= 0 then
				return S;
			end if;
		elsif A'Last < B'Last then
			S := Sign (B (Limb_Index'Max (A'Last + 1, B'First) .. B'Last));
			if S /= 0 then
				return -S;
			end if;
		end if;
		-- test common parts next
		for J in reverse Limb_Index'Max (A'First, B'First)
		              .. Limb_Index'Min (A'Last, B'Last)
		loop
			if A (J) /= B (J) then
				return (if A (J) < B (J) xor J mod 2 = 0 then +1 else -1);
			end if;
		end loop;
		-- test remaining part
		if A'First > B'First then
			S := Sign (B (B'First .. Limb_Index'Min (B'Last, A'First - 1)));
			if S /= 0 then
				return -S;
			end if;
		elsif A'First < B'First then
			S := Sign (A (A'First .. Limb_Index'Min (A'Last, B'First - 1)));
			if S /= 0 then
				return S;
			end if;
		end if;
		return 0;
	end Compare;

	-- Internal Manipulation

	procedure Generic_Extract_Limb (
		V : in     Value_Type;
		L :    out Limb;
		W :    out Value_Type)
	is
	begin
		-- compile time check for case when Value_Type is smaller than Normalized_Limb
		if Value_Type'Base'Last < 2**Limb_Bits then
			if Value_Type'Base'First >= 0 then
				L := Limb (V);
				W := 0;
			else
				declare
					A : Accumulator := Accumulator (V);
				begin
					L := Limb (A mod 2**Limb_Bits);
					W := Value_Type ((A - Accumulator (L)) / Limb_Base);
				end;
			end if;
		else
			L := Limb (V mod 2**Limb_Bits);
			W := (V - Value_Type (L)) / Limb_Base;
		end if;
	end Generic_Extract_Limb;

	procedure Make_Limb is new Generic_Extract_Limb (Integer);

	procedure Make_Limb is new Generic_Extract_Limb (Accumulator);

	-- Making and copying

	function Make_With_Bits (
		Highest_Bit : Integer;
		Lowest_Bit  : Integer := 0;
		Value     : Integer := 0)
		return      Big_Number
	is
		Last  : constant Limb_Index := Limb_Index (Highest_Bit / Limb_Bits + 1);
		-- Last is highest limb in the number
		-- High_bits up to Limb_Bits - 1 should give First = 1
		First : constant Limb_Index
		      := Limb_Index ((Lowest_Bit - Lowest_Bit mod Limb_Bits) / Limb_Bits);
		-- First is lowest limb in the number
		-- Low_bits = 0 should give Last = 0
		-- Low_bits in -Limb_Bits .. -1 should give Last = -1
		V : Integer := Value;
		J : Limb_Index := 0;
	begin
		return R : Big_Number := (First .. Last => 0) do
			while V /= 0 loop
				Make_Limb (V, R (J), V);
				J := J + 1;
			end loop;
		end return;
	end Make_With_Bits;

	procedure Set (T : out Big_Number; S : in Big_Number)
	is
		CF : constant Limb_Index := Limb_Index'Max (T'First, S'First);
		CL : constant Limb_Index := Limb_Index'Min (T'Last, S'Last);
	begin
		if CF > CL then
			-- S is completely out of T representation range.
			-- the only result there is 0 (if rounding to nearest)
			T (T'First .. T'Last) := (others => 0);
			return;
		end if;
		if T'First < CF then
			T (T'First .. CF - 1) := (others => 0);
		end if;
		T (CF .. CL) := S (CF .. CL);
		if T'Last > CL then
			T (CL + 1 .. T'Last) := (others => 0);
		end if;
	end Set;

	--

	procedure Negate (X : in out Big_Number)
	is
		A : Accumulator := 0;
	begin
		for J in X'Range loop
			A := A - Accumulator (X (J));
			Make_Limb (A, X (J), A);
		end loop;
		if A /= 0 then
			raise Constraint_Error with "Negation overflow";
		end if;
	end Negate;

end VSSL.Big.Numbers;