--
-- Functions on powers of two on modular numbers
--
pragma SPARK_Mode;

generic
	type Value is mod <>;
	with function Shift_Left (X : Value; N : Natural) return Value is <>;
	with function Shift_Right (X : Value; N : Natural) return Value is <>;
package VSSL.Integers.Modular_Binary is

	function Is_Power_of_2 (X : Value) return Boolean is
		((X and (X - 1)) = 0);
	pragma Global (Is_Power_of_2 => null);
	-- It is a small hack: when x is power of 2
	-- it looks like 10000...0 in binary and
	-- x - 1 looks like 01111...1
	-- for all other numbers they'll have
	-- common 1's set.

	function Floor_Log_2 (X : Value) return Natural with
		Pre => X > 0,
		Post => Floor_Log_2'Result < Value'Base'Size and 2**Floor_Log_2'Result in X / 2 + 1 .. X;
	pragma Global (Floor_Log_2 => null);

end VSSL.Integers.Modular_Binary;
