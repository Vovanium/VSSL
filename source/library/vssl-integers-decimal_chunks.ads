pragma SPARK_Mode;

package VSSL.Integers.Decimal_Chunks is

	Chunk_Places : constant := 5;

	type Decimal_Chunk is mod 10**Chunk_Places;

	pragma Assert (Decimal_Chunk'Modulus <= Integer'Last);

end VSSL.Integers.Decimal_Chunks;