--
-- Integer instantiation for Generic_Binary
--
pragma SPARK_Mode;

with VSSL.Integers.Generic_Binary;
package VSSL.Integers.Binary is new VSSL.Integers.Generic_Binary (Integer'Base);
