package body VSSL.Combinatorial.Generic_Functions is

	function Factorial (N : Value) return Value is
		F : Value := 1;
	begin
		for I in 2 .. N loop
			F := F * I;
		end loop;
		return F;
	end Factorial;

	function Factorial (N, A : Value) return Value is
		F : Value := 1;
		I : Value := N;
	begin
		while I >= A loop
			F := F * I;
			I := I - A;
		end loop;
		return F;
	end Factorial;

	function Subfactorial (N : Value) return Value is
		X : Value := 1;
		Y : Value := 0;
		T : Value;
	begin
		if N = 0 then
			return X;
		end if;
		for I in 1 .. N - 1 loop
			T := I * (X + Y);
			X := Y;
			Y := T;
		end loop;
		return Y;
	end Subfactorial;

	function Permutations (N, R : Value) return Value is
		P : Value := 1;
	begin
		for I in N - R + 1 .. N loop
			P := P * I;
		end loop;
		return P;
	end Permutations;

	function Combinations (N, R : Value) return Value is
		C : Value := 1;
	begin
		for I in 1 .. R loop
			C := C * (I + (N - R)) / I;
		end loop;
		return C;
	end Combinations;

end VSSL.Combinatorial.Generic_Functions;
