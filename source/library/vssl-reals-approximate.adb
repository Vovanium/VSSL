package body VSSL.Reals.Approximate is

	function Generic_Absolute_Equal (A, B : Value) return Boolean is
	(B - Absolute_Error <= A and A <= B + Absolute_Error);

	function Generic_Relative_Equal (A, B : Value) return Boolean is
		E : Value := abs (B) * Relative_Error;
	begin
		return B - E <= A and A <= B + E;
	end Generic_Relative_Equal;

	function Generic_Equal (A, B : Value) return Boolean is
		E : Value := abs (B) * Relative_Error;
	begin
		if E <= Absolute_Error then
			E := Absolute_Error;
		end if;
		return B - E <= A and A <= B + E;
	end Generic_Equal;

end VSSL.Reals.Approximate;