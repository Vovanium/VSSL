pragma SPARK_Mode;

package VSSL.Integers.Primes with Pure is

	function Is_Prime (X : Natural) return Boolean;

end VSSL.Integers.Primes;
