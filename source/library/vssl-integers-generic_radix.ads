--
-- A set of routines on positional integer number representation
--
pragma SPARK_Mode;

generic
	type Number is private;
	with function "<" (Left, Right : Number) return Boolean is <>;
	with function "=" (Left, Reght : Number) return Boolean is <>;
	with function "/" (Left, Right : Number) return Number is <>;
	with function "mod" (Left, Right : Number) return Number is <>;
	with function "**" (Left : Number; Right : Natural) return Number is <>;
	Default_Radix : in Number;
package VSSL.Integers.Generic_Radix with Pure is

	function Order (
		X     : Number;                  -- The number
		Radix : Number := Default_Radix) -- Radix
		return  Natural;
	-- The position of highest non-zero digit

	function Digit (
		X     : Number;                  -- The number
		N     : Natural;                 -- Position
		Radix : Number := Default_Radix) -- Representation radix
		return  Number
		is ((X / Radix ** N) mod Radix);
	-- N'th digit of number refpresentation

	function Is_Palindrome (
		X     : Number;                  -- Number to check
		Radix : Number := Default_Radix) -- Representation radix
		return  Boolean;
	-- Check if a number representation is a palindrome

end VSSL.Integers.Generic_Radix;
