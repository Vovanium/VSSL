generic
	type Number_Type (<>) is private;
	with function "-" (
		A    : Number_Type)
		return Number_Type is <>;
	with function "+" (
		A,
		B    : Number_Type)
		return Number_Type is <>;
	with function "*" (
		A,
		B    : Number_Type)
		return Number_Type is <>;
	type Exponent_Type is range <>; -- A type to store exponent values
	with function To_Number (
		A    : Integer)
		return Number_Type is <>; -- A function to convert a digit to a given type
	with function Multiply_by_Power (
		A,
		R    : Number_Type;
		N    : Exponent_Type)
		return Number_Type is <>;
function VSSL.Reals.Generic_String_to_Number (
	S    : String)
	return Number_Type;

--
-- Implementation of numeric type is required to be able a hold a value of Base**Number_of_Digits - 1.
-- (More precisely, the largest number that resemble all the digits without decimal separator)
-- This generally fits well in floating point or integers but does not fit in fixed point.
