package body VSSL.Champernowne_Constant is

	function Champernowne_Constant_Digit (X : Positive) return Natural is
		Index : Integer := 1;
		Order : Integer := 1;
		Size : Integer := 9; -- number of numbers in order
		Number : Integer;
		DI : Integer;
		-- (1st order is 1...9 2nd order is 10...99 etc.)
	begin
		-- Finding order of digit
		while Index + Size * Order <= X loop
			Index := Index + Size * Order;
			Size := Size * 10;
			Order := Order + 1;
		end loop;
		Number := (X - Index) / Order;
		Index := Index + Number * Order;
		Number := Number + 10 ** (Order - 1);
		DI := Order - (X - Index) - 1;
		return (Number / 10 ** DI) mod 10;
	end Champernowne_Constant_Digit;

end VSSL.Champernowne_Constant;
