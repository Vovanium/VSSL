--
-- Implementation of quaternions with Float type
--
with VSSL.Floating_Point.Generic_Quaternions;

package VSSL.Floating_Point.Quaternions is new VSSL.Floating_Point.Generic_Quaternions (Real => Float);