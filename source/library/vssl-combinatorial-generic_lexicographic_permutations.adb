with VSSL.Combinatorial.Permutations;

package body VSSL.Combinatorial.Generic_Lexicographic_Permutations is

	function Descending_Tail (S : Set) return Index is
		L : Index := S'Last;
	begin
		while L > S'First and then S (L) < S (Index'Pred (L)) loop
			L := Index'Pred (L);
		end loop;
		return L;
	end Descending_Tail;

	procedure Swap is new Vssl.Combinatorial.Permutations.Generic_Swap (Element);

	procedure Invert is new Vssl.Combinatorial.Permutations.Generic_Invert (Element, Index, Set);

	function Closest (S : Set; T : Element) return Index is
	begin
		for I in S'Range loop
			if not (S (I) < T) then
				return I;
			end if;
		end loop;
		return S'Last;
	end Closest;

	procedure Next_Lexicographic_Permutation (
		P : in out Set)
	is
		L : Index := Descending_Tail (P);
		I : Index;
	begin
		Invert (P (L .. P'Last));
		if P'First < L then
			I := Closest (P (L .. P'Last), P (Index'Pred (L)));
			Swap (P (Index'Pred (L)), P (I));
		end if;
	end Next_Lexicographic_Permutation;

end VSSL.Combinatorial.Generic_Lexicographic_Permutations;