-- Package provides facilities for conversion of real numbers
-- (and integers as a subset of) to and from textual representation
-- stored in strings

with Ada.Characters.Latin_1;
use  Ada.Characters.Latin_1;

package VSSL.Reals.String_Conversions with Pure is

	subtype Radix_Type is Integer range 2 .. 36;
	-- A type with a range of allowed number bases

	subtype Digit_Value is Integer range 0 .. Radix_Type'Last - 1;
	-- A type to store a value of a single digit

	subtype Ignored_Character is Character
	with Static_Predicate => Ignored_Character in NUL .. ' ' | '_';
	-- A class of characters that are ignored by the parser

	subtype Digit_Character is Character
	with Static_Predicate => Digit_Character in '0' .. '9' | 'A' .. 'Z' | 'a' .. 'z';
	-- A class of characters used for digits

	function To_Digit_Value (C : Digit_Character) return Digit_Value
	is (case C is
		when '0' .. '9' => Character'Pos (C) - Character'Pos ('0'),
		when 'A' .. 'Z' => Character'Pos (C) - Character'Pos ('A') + 10,
		when 'a' .. 'z' => Character'Pos (C) - Character'Pos ('a') + 10);

	generic
		type Integer_Type is range <>;
	function Generic_Decimal_to_Integer (S : String) return Integer_Type;
	-- Generic function to convert a run of decimal digits
	-- to an arbitrary integer type

	function Decimal_to_Integer (S : String) return Integer;
	-- Converts run of decimal digits to an integer

	procedure Parse_Number_Format (
		S       : in     String;     -- A string to parse
		Radix   :    out Radix_Type; -- Radix of a mantissa
		Minus   :    out Boolean;    -- Minus sign of a mantissa
		M_First :    out Positive;   -- First digit index in the mantissa
		M_Last  :    out Natural;    -- Last digit index in the mantissa
		E_Base  :    out Radix_Type; -- Exponent base (equal to Radix or 2)
		E_Minus :    out Boolean;    -- Minus sign of the exponent
		E_First :    out Positive;   -- First digit index in the exponent
		E_Last  :    out Natural);   -- Last digit index in the exponent
	-- Parses a string for a valid real number
	-- detecting various formats and returns information
	-- for conversion: radix, sign, mantissa position,
	-- exponent base, sign and position (if exists).
	--
	-- If the exponent does not exist is returns empty range for
	-- E_First .. E_Last (thus E_First > E_Last).

	procedure Split_Mantissa (
		S           : in     String;
		Separator   :    out Positive; -- Separator position
		Int_Digits  :    out Natural;  -- Digit count in integral part
		Frac_Digits :    out Natural); -- Digit count in fractional part
	-- Splits mantissa into integer and fractional part,
	-- also counts digits in both parts
	-- and checks for character classes raising an Constraint_Error expression on fail

-- Supported formats:
--
-- * With integer and/or fractional part with or without decimal separator
--   (required if fractional part exists). Decimal separator can be dot or comma.
--         "1"                => 1.0
--         "1."               => 1.0
--         "1.0"              => 1.0
--         "0.4"              => 0.4
--         ".4"               => 0.4
--         "2,3"              => 2.3
-- * With or without a sign
--         "1"                => 1.0
--         "+1"               => 1.0
--         "-1"               => -1.0
-- * Digits can be separated with undercsores (_) or spaces ( ).
--   C0 Control characters (like NUL, TAB) are also ignored in most positions.
--        "31 968"            => 31.968
--        "3.14159_26"        => 3.1415926
-- * In scientific exponentional format. Common letters E or e and
--   also Fortran-ish D or d can be used to introduce exponent.
--         "12E7"             => 120000000.0
--         "+1.24d-03"        => 0.00123
-- * In Ada-style based format. Base range is enhanced to 2..36.
--   Both hash (#) and colon (:) are supported (colon was a valid replacement
--   for hash in Ada-83).
--   If there's no exponent, the second hash can be omitted.
--        "16#7FFD.8#"        => 32765.5
--        "8#3.77#E+2"        => 255.0
-- * With C-style prefixes for hexadecimal (0x) and binary (0b).
--        "0x7FFD.8"          => 32765.5
--        "0b1101"            => 13.0
-- * With C-stype binary exponent also
--        "0x1.8p1"           => 3.0
-- * In Fortran-style BOZ-format.
--        "B'1101'"           => 13.0
--        "O'377'"            => 255.0
--        "Z'7FFD'"           => 32765.0
-- * In Algol-style 'r' based format.
--        "16r7FFD.8"         => 32765.5
-- * Postfix H and B for hexadecimal and binary numbers.
--        "1101B"             => 13.0
--        "7FFD.8H"           => 32765.5
-- * Upper and lowercase letters allowed.
--        "16#7fFd.8#"        => 32765.5
--
-- NOT supported formats:
--
-- * Any kind of non-finite values, like NANs, Infinities.
-- * Imaginary, complex, dual, imprecise etc. multi-component numbers.
-- * Periodic fractions.
-- * C-style octals.
-- * Sexagesimal numbers like in HMS time or DMS angles.
-- * Roman numerals.
-- * Anything out of the ASCII character set.
--
-- It tries to analyse the whole string. If data is in wrong format
-- then exception may be raised or wrong result produced.
-- The behaviour if the result is out of range is on the implementation
-- of underlying arithmetic subprograms.

end VSSL.Reals.String_Conversions;
