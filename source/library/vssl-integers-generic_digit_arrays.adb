package body VSSL.Integers.Generic_Digit_Arrays is

	function First_Nonzero (X : Digit_Array) return Index'Base is
	begin
		for I in X'Range loop
			if X (I) /= 0 then
				return I;
			end if;
		end loop;
		return Index'Base'Last;
	end First_Nonzero;

	function Last_Nonzero (X : Digit_Array) return Index'Base is
	begin
		for I in reverse X'Range loop
			if X (I) /= 0 then
				return I;
			end if;
		end loop;
		return Index'Base'First;
	end Last_Nonzero;

	function "=" (X, Y : Digit_Array) return Boolean is
		F : Index'Base := Index'Max (X'First, Y'First);
		L : Index'Base := Index'Min (X'Last, Y'Last);
	begin
		return (if X'First < Y'First then
				Is_Zero (X (X'First .. Index'Min (X'Last, Index'Pred (Y'First))))
			elsif Y'First < X'First then
				Is_Zero (Y (Y'First .. Index'Min (Y'Last, Index'Pred (X'First))))
			else
				True)
		and then
			X (F .. L) = Y (F .. L)
		and then (if X'Last < Y'Last then
				Is_Zero (Y (Index'Max (Y'First, Index'Succ (X'Last)) .. Y'Last))
			elsif Y'Last < X'Last then
				Is_Zero (X (Index'Max (X'First, Index'Succ (Y'Last)) .. X'Last))
			else
				True);
	end "=";

	function "<" (X, Y : Digit_Array) return Boolean is
		pragma Assume (X'First in Index);
		pragma Assume (Y'First in Index);
		pragma Assume (X'Last in Index);
		pragma Assume (Y'Last in Index); -- These assumptions are made for Gnatprove
		XD, YD : Digit_Type;
	begin
		for I in reverse Index'Min (X'First, Y'First) .. Index'Max (X'Last, Y'Last) loop
			XD := Element (X, I);
			YD := Element (Y, I);
			if XD < YD then
				return True;
			elsif XD > YD then
				return False;
			end if;
		end loop;
		return False;
	end "<"; -- TODO: optimize

	procedure Set (
		X :    out Digit_Array;
		Y : in     Digit_Array)
	is
	begin
		if not Is_Fit (Y, X'First, X'Last) then
			raise Constraint_Error with "Value does not fit in target";
		end if;
		X := (others => 0);
		for J in Index'Max (X'First, Y'First) .. Index'Min (X'Last, Y'Last) loop
			X (J) := Y (J);
			pragma Loop_Invariant (for all K in X'First .. J => X (K) =
				(if K in Y'Range then Y (K) else 0));
		end loop;
	end Set;

end VSSL.Integers.Generic_Digit_Arrays;
