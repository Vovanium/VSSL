pragma SPARK_Mode;

with Ada.Iterator_Interfaces;

generic
	Limit : Positive;
package VSSL.Integers.Primes.Generic_Sieve is

	function Is_Prime (N : Positive) return Boolean;
	package Prime_Iterators is new Ada.Iterator_Interfaces (
		Cursor => Positive,
		Has_Element => Is_Prime);

	type Prime_Iterator is new Prime_Iterators.Forward_Iterator with private;

	overriding function First (
		Object : Prime_Iterator)
		return Positive;

	overriding function Next (
		Object : Prime_Iterator;
		Position : Positive)
		return Positive;

	function Iterate return Prime_Iterator;

private
	type Prime_Iterator is new Prime_Iterators.Forward_Iterator with null record;

end VSSL.Integers.Primes.Generic_Sieve;

