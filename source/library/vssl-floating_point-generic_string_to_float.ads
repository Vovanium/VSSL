generic
	type Number_Type is digits <>;
function VSSL.Floating_Point.Generic_String_to_Float (
	S    : String)
	return Number_Type;

-- Converts a textual representation of a number to a floating point type.
-- See Reals.String_Conversions.Parse_Number_Format for reference on
-- supported representations.
