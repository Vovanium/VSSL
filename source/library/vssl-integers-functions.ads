--
-- Elementary functions on ordinary integers
--
pragma SPARK_Mode;

with VSSL.Integers.Generic_Functions;

package VSSL.Integers.Functions is new VSSL.Integers.Generic_Functions (Integer);